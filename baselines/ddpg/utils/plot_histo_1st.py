import pickle
import numpy as np
import matplotlib.pyplot as plt

file_location1 = '/home/flowers/Desktop/Scratch/experiments/first_successes/CMC_successes_complex_policy'
file_location2 = '/home/flowers/Desktop/Scratch/experiments/first_successes/CMC_successes_simpleLinear_policy'
file_location3 = '/home/flowers/Desktop/Scratch/experiments/first_successes/CMC_successes_complex_policy400_300'

n_steps_success = np.loadtxt(file_location1)
n_steps_success2 = np.loadtxt(file_location2)
n_steps_success3 = np.loadtxt(file_location3)

n_runs = n_steps_success.shape[0]
print('mean number of steps before reaching a reward: ', n_steps_success.mean())
print('mean number of steps before reaching a reward: ', n_steps_success2.mean())
print('mean number of steps before reaching a reward: ', n_steps_success3.mean())

fig = plt.figure(figsize=(20,13), frameon=False)
plt.hist(n_steps_success,50, color=(0,0.447,0.7410, 0.5))
plt.hist(n_steps_success2,50, color=(0.85,0.325,0.098, 0.5))
plt.hist(n_steps_success3,50, color=(0.929,0.694,0.125, 0.5))
plt.legend(['GEP, complex policy (64,64)','GEP linear policy', 'GEP, complex policy (400,300)'])
plt.xlabel('number of steps before first reward')
plt.title('Histogram of the number of steps before reaching the first reward ('+str(n_runs)+' runs)')
plt.savefig('/home/flowers/Desktop/Scratch/experiments/first_successes/hist')


ind=np.argwhere(n_steps_success!=0)
n_steps_success[ind].mean()
