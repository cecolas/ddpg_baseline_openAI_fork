#!/bin/sh
#SBATCH -n1
#SBATCH -p long_sirocco
#SBATCH -t 20:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_test.sh.err
#SBATCH -o run_test.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Ddpg Withnoise: Trial 1, Seed 1, 2018-01-24 20:43:09.856897';
echo '=================> Ddpg Withnoise: Trial 1, Seed 1, 2018-01-24 20:43:09.856897' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 1 --study DDPG --seed 1 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/data_study_GEP/ 
