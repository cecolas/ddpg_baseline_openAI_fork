#!/bin/sh
#SBATCH -n1
#SBATCH -p long_sirocco
#SBATCH -t 60:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_GEP_DDPG_trials101_110.sh.err
#SBATCH -o run_GEP_DDPG_trials101_110.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withparamnoise: Trial 101, Buffer Size 500, Buffer Id 41, Seed 101, 2018-02-01 18:05:09.466476';
echo '=================> Gep-Ddpg Withparamnoise: Trial 101, Buffer Size 500, Buffer Id 41, Seed 101, 2018-02-01 18:05:09.466476' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 101 --seed 101  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_41 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 102, Buffer Size 500, Buffer Id 42, Seed 102, 2018-02-01 18:05:09.466529';
echo '=================> Gep-Ddpg Withparamnoise: Trial 102, Buffer Size 500, Buffer Id 42, Seed 102, 2018-02-01 18:05:09.466529' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 102 --seed 102  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_42 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 103, Buffer Size 500, Buffer Id 43, Seed 103, 2018-02-01 18:05:09.466552';
echo '=================> Gep-Ddpg Withparamnoise: Trial 103, Buffer Size 500, Buffer Id 43, Seed 103, 2018-02-01 18:05:09.466552' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 103 --seed 103  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_43 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 104, Buffer Size 500, Buffer Id 44, Seed 104, 2018-02-01 18:05:09.466573';
echo '=================> Gep-Ddpg Withparamnoise: Trial 104, Buffer Size 500, Buffer Id 44, Seed 104, 2018-02-01 18:05:09.466573' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 104 --seed 104  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_44 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 105, Buffer Size 500, Buffer Id 45, Seed 105, 2018-02-01 18:05:09.466595';
echo '=================> Gep-Ddpg Withparamnoise: Trial 105, Buffer Size 500, Buffer Id 45, Seed 105, 2018-02-01 18:05:09.466595' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 105 --seed 105  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_45 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 106, Buffer Size 500, Buffer Id 46, Seed 106, 2018-02-01 18:05:09.466623';
echo '=================> Gep-Ddpg Withparamnoise: Trial 106, Buffer Size 500, Buffer Id 46, Seed 106, 2018-02-01 18:05:09.466623' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 106 --seed 106  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_46 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 107, Buffer Size 500, Buffer Id 47, Seed 107, 2018-02-01 18:05:09.466641';
echo '=================> Gep-Ddpg Withparamnoise: Trial 107, Buffer Size 500, Buffer Id 47, Seed 107, 2018-02-01 18:05:09.466641' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 107 --seed 107  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_47 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 108, Buffer Size 500, Buffer Id 48, Seed 108, 2018-02-01 18:05:09.466659';
echo '=================> Gep-Ddpg Withparamnoise: Trial 108, Buffer Size 500, Buffer Id 48, Seed 108, 2018-02-01 18:05:09.466659' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 108 --seed 108  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_48 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 109, Buffer Size 500, Buffer Id 49, Seed 109, 2018-02-01 18:05:09.466677';
echo '=================> Gep-Ddpg Withparamnoise: Trial 109, Buffer Size 500, Buffer Id 49, Seed 109, 2018-02-01 18:05:09.466677' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 109 --seed 109  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_49 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 110, Buffer Size 500, Buffer Id 50, Seed 110, 2018-02-01 18:05:09.466696';
echo '=================> Gep-Ddpg Withparamnoise: Trial 110, Buffer Size 500, Buffer Id 50, Seed 110, 2018-02-01 18:05:09.466696' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 110 --seed 110  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_50 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait