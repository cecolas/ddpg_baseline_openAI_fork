#!/bin/sh
#SBATCH -n20
#SBATCH -p long_sirocco
#SBATCH -t 30:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_GEP_FDDPG_trials521_540.sh.err
#SBATCH -o run_GEP_FDDPG_trials521_540.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep_Fddpg Withnoise: Trial 521, Buffer Size 100, Buffer Id 1, Seed 521, 2018-02-14 12:02:38.503723';
echo '=================> Gep_Fddpg Withnoise: Trial 521, Buffer Size 100, Buffer Id 1, Seed 521, 2018-02-14 12:02:38.503723' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 521 --seed 521 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_100_1 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 522, Buffer Size 100, Buffer Id 2, Seed 522, 2018-02-14 12:02:38.503918';
echo '=================> Gep_Fddpg Withnoise: Trial 522, Buffer Size 100, Buffer Id 2, Seed 522, 2018-02-14 12:02:38.503918' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 522 --seed 522 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_100_2 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 523, Buffer Size 100, Buffer Id 3, Seed 523, 2018-02-14 12:02:38.503946';
echo '=================> Gep_Fddpg Withnoise: Trial 523, Buffer Size 100, Buffer Id 3, Seed 523, 2018-02-14 12:02:38.503946' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 523 --seed 523 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_100_3 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 524, Buffer Size 100, Buffer Id 4, Seed 524, 2018-02-14 12:02:38.503966';
echo '=================> Gep_Fddpg Withnoise: Trial 524, Buffer Size 100, Buffer Id 4, Seed 524, 2018-02-14 12:02:38.503966' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 524 --seed 524 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_100_4 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 525, Buffer Size 100, Buffer Id 5, Seed 525, 2018-02-14 12:02:38.503986';
echo '=================> Gep_Fddpg Withnoise: Trial 525, Buffer Size 100, Buffer Id 5, Seed 525, 2018-02-14 12:02:38.503986' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 525 --seed 525 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_100_5 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 526, Buffer Size 100, Buffer Id 6, Seed 526, 2018-02-14 12:02:38.504006';
echo '=================> Gep_Fddpg Withnoise: Trial 526, Buffer Size 100, Buffer Id 6, Seed 526, 2018-02-14 12:02:38.504006' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 526 --seed 526 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_100_6 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 527, Buffer Size 100, Buffer Id 7, Seed 527, 2018-02-14 12:02:38.504024';
echo '=================> Gep_Fddpg Withnoise: Trial 527, Buffer Size 100, Buffer Id 7, Seed 527, 2018-02-14 12:02:38.504024' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 527 --seed 527 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_100_7 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 528, Buffer Size 100, Buffer Id 8, Seed 528, 2018-02-14 12:02:38.504042';
echo '=================> Gep_Fddpg Withnoise: Trial 528, Buffer Size 100, Buffer Id 8, Seed 528, 2018-02-14 12:02:38.504042' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 528 --seed 528 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_100_8 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 529, Buffer Size 100, Buffer Id 9, Seed 529, 2018-02-14 12:02:38.504061';
echo '=================> Gep_Fddpg Withnoise: Trial 529, Buffer Size 100, Buffer Id 9, Seed 529, 2018-02-14 12:02:38.504061' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 529 --seed 529 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_100_9 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 530, Buffer Size 100, Buffer Id 10, Seed 530, 2018-02-14 12:02:38.504080';
echo '=================> Gep_Fddpg Withnoise: Trial 530, Buffer Size 100, Buffer Id 10, Seed 530, 2018-02-14 12:02:38.504080' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 530 --seed 530 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_100_10 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 531, Buffer Size 100, Buffer Id 11, Seed 531, 2018-02-14 12:02:38.504103';
echo '=================> Gep_Fddpg Withnoise: Trial 531, Buffer Size 100, Buffer Id 11, Seed 531, 2018-02-14 12:02:38.504103' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 531 --seed 531 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_100_11 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 532, Buffer Size 100, Buffer Id 12, Seed 532, 2018-02-14 12:02:38.504121';
echo '=================> Gep_Fddpg Withnoise: Trial 532, Buffer Size 100, Buffer Id 12, Seed 532, 2018-02-14 12:02:38.504121' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 532 --seed 532 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_100_12 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 533, Buffer Size 100, Buffer Id 13, Seed 533, 2018-02-14 12:02:38.504141';
echo '=================> Gep_Fddpg Withnoise: Trial 533, Buffer Size 100, Buffer Id 13, Seed 533, 2018-02-14 12:02:38.504141' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 533 --seed 533 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_100_13 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 534, Buffer Size 100, Buffer Id 14, Seed 534, 2018-02-14 12:02:38.504159';
echo '=================> Gep_Fddpg Withnoise: Trial 534, Buffer Size 100, Buffer Id 14, Seed 534, 2018-02-14 12:02:38.504159' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 534 --seed 534 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_100_14 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 535, Buffer Size 100, Buffer Id 15, Seed 535, 2018-02-14 12:02:38.504238';
echo '=================> Gep_Fddpg Withnoise: Trial 535, Buffer Size 100, Buffer Id 15, Seed 535, 2018-02-14 12:02:38.504238' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 535 --seed 535 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_100_15 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 536, Buffer Size 100, Buffer Id 16, Seed 536, 2018-02-14 12:02:38.504259';
echo '=================> Gep_Fddpg Withnoise: Trial 536, Buffer Size 100, Buffer Id 16, Seed 536, 2018-02-14 12:02:38.504259' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 536 --seed 536 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_100_16 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 537, Buffer Size 100, Buffer Id 17, Seed 537, 2018-02-14 12:02:38.504280';
echo '=================> Gep_Fddpg Withnoise: Trial 537, Buffer Size 100, Buffer Id 17, Seed 537, 2018-02-14 12:02:38.504280' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 537 --seed 537 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_100_17 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 538, Buffer Size 100, Buffer Id 18, Seed 538, 2018-02-14 12:02:38.504299';
echo '=================> Gep_Fddpg Withnoise: Trial 538, Buffer Size 100, Buffer Id 18, Seed 538, 2018-02-14 12:02:38.504299' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 538 --seed 538 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_100_18 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 539, Buffer Size 100, Buffer Id 19, Seed 539, 2018-02-14 12:02:38.504320';
echo '=================> Gep_Fddpg Withnoise: Trial 539, Buffer Size 100, Buffer Id 19, Seed 539, 2018-02-14 12:02:38.504320' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 539 --seed 539 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_100_19 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 540, Buffer Size 100, Buffer Id 20, Seed 540, 2018-02-14 12:02:38.504338';
echo '=================> Gep_Fddpg Withnoise: Trial 540, Buffer Size 100, Buffer Id 20, Seed 540, 2018-02-14 12:02:38.504338' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 540 --seed 540 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_100_20 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait