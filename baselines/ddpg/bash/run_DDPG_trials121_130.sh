#!/bin/sh
#SBATCH -n1
#SBATCH -p long_sirocco
#SBATCH -t 60:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_DDPG_trials121_130.sh.err
#SBATCH -o run_DDPG_trials121_130.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Ddpg Withdecreasingnoise: Trial 121, Seed 121, 2018-02-02 11:46:59.227584';
echo '=================> Ddpg Withdecreasingnoise: Trial 121, Seed 121, 2018-02-02 11:46:59.227584' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 121 --study DDPG --noise_type decreasing-ou_0.6 --seed 121 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 122, Seed 122, 2018-02-02 11:46:59.227619';
echo '=================> Ddpg Withdecreasingnoise: Trial 122, Seed 122, 2018-02-02 11:46:59.227619' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 122 --study DDPG --noise_type decreasing-ou_0.6 --seed 122 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 123, Seed 123, 2018-02-02 11:46:59.227635';
echo '=================> Ddpg Withdecreasingnoise: Trial 123, Seed 123, 2018-02-02 11:46:59.227635' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 123 --study DDPG --noise_type decreasing-ou_0.6 --seed 123 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 124, Seed 124, 2018-02-02 11:46:59.227651';
echo '=================> Ddpg Withdecreasingnoise: Trial 124, Seed 124, 2018-02-02 11:46:59.227651' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 124 --study DDPG --noise_type decreasing-ou_0.6 --seed 124 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 125, Seed 125, 2018-02-02 11:46:59.227664';
echo '=================> Ddpg Withdecreasingnoise: Trial 125, Seed 125, 2018-02-02 11:46:59.227664' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 125 --study DDPG --noise_type decreasing-ou_0.6 --seed 125 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 126, Seed 126, 2018-02-02 11:46:59.227678';
echo '=================> Ddpg Withdecreasingnoise: Trial 126, Seed 126, 2018-02-02 11:46:59.227678' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 126 --study DDPG --noise_type decreasing-ou_0.6 --seed 126 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 127, Seed 127, 2018-02-02 11:46:59.227691';
echo '=================> Ddpg Withdecreasingnoise: Trial 127, Seed 127, 2018-02-02 11:46:59.227691' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 127 --study DDPG --noise_type decreasing-ou_0.6 --seed 127 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 128, Seed 128, 2018-02-02 11:46:59.227703';
echo '=================> Ddpg Withdecreasingnoise: Trial 128, Seed 128, 2018-02-02 11:46:59.227703' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 128 --study DDPG --noise_type decreasing-ou_0.6 --seed 128 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 129, Seed 129, 2018-02-02 11:46:59.227716';
echo '=================> Ddpg Withdecreasingnoise: Trial 129, Seed 129, 2018-02-02 11:46:59.227716' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 129 --study DDPG --noise_type decreasing-ou_0.6 --seed 129 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 130, Seed 130, 2018-02-02 11:46:59.227728';
echo '=================> Ddpg Withdecreasingnoise: Trial 130, Seed 130, 2018-02-02 11:46:59.227728' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 130 --study DDPG --noise_type decreasing-ou_0.6 --seed 130 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait