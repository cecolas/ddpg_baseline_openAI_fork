#!/bin/sh
export EXP_INTERP='/usr/bin/python3' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withnoise: Trial 151, Buffer Size 500, Buffer Id 11, Seed 151, 2018-01-31 15:21:51.117647';
echo '=================> Gep-Ddpg Withnoise: Trial 151, Buffer Size 500, Buffer Id 11, Seed 151, 2018-01-31 15:21:51.117647' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 151 --seed 151 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_11_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withnoise: Trial 152, Buffer Size 500, Buffer Id 12, Seed 152, 2018-01-31 15:21:51.117688';
echo '=================> Gep-Ddpg Withnoise: Trial 152, Buffer Size 500, Buffer Id 12, Seed 152, 2018-01-31 15:21:51.117688' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 152 --seed 152 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_12_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withnoise: Trial 153, Buffer Size 500, Buffer Id 13, Seed 153, 2018-01-31 15:21:51.117708';
echo '=================> Gep-Ddpg Withnoise: Trial 153, Buffer Size 500, Buffer Id 13, Seed 153, 2018-01-31 15:21:51.117708' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 153 --seed 153 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_13_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withnoise: Trial 154, Buffer Size 500, Buffer Id 14, Seed 154, 2018-01-31 15:21:51.117727';
echo '=================> Gep-Ddpg Withnoise: Trial 154, Buffer Size 500, Buffer Id 14, Seed 154, 2018-01-31 15:21:51.117727' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 154 --seed 154 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_14_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withnoise: Trial 155, Buffer Size 500, Buffer Id 15, Seed 155, 2018-01-31 15:21:51.117744';
echo '=================> Gep-Ddpg Withnoise: Trial 155, Buffer Size 500, Buffer Id 15, Seed 155, 2018-01-31 15:21:51.117744' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 155 --seed 155 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_15_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
