#!/bin/sh
#SBATCH -n20
#SBATCH -p long_sirocco
#SBATCH -t 30:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_GEP_FDDPG_trials541_560.sh.err
#SBATCH -o run_GEP_FDDPG_trials541_560.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep_Fddpg Withnoise: Trial 541, Buffer Size 300, Buffer Id 1, Seed 541, 2018-02-14 13:26:22.993410';
echo '=================> Gep_Fddpg Withnoise: Trial 541, Buffer Size 300, Buffer Id 1, Seed 541, 2018-02-14 13:26:22.993410' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 541 --seed 541 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_300_1 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 542, Buffer Size 300, Buffer Id 2, Seed 542, 2018-02-14 13:26:22.993457';
echo '=================> Gep_Fddpg Withnoise: Trial 542, Buffer Size 300, Buffer Id 2, Seed 542, 2018-02-14 13:26:22.993457' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 542 --seed 542 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_300_2 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 543, Buffer Size 300, Buffer Id 3, Seed 543, 2018-02-14 13:26:22.993485';
echo '=================> Gep_Fddpg Withnoise: Trial 543, Buffer Size 300, Buffer Id 3, Seed 543, 2018-02-14 13:26:22.993485' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 543 --seed 543 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_300_3 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 544, Buffer Size 300, Buffer Id 4, Seed 544, 2018-02-14 13:26:22.993509';
echo '=================> Gep_Fddpg Withnoise: Trial 544, Buffer Size 300, Buffer Id 4, Seed 544, 2018-02-14 13:26:22.993509' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 544 --seed 544 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_300_4 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 545, Buffer Size 300, Buffer Id 5, Seed 545, 2018-02-14 13:26:22.993531';
echo '=================> Gep_Fddpg Withnoise: Trial 545, Buffer Size 300, Buffer Id 5, Seed 545, 2018-02-14 13:26:22.993531' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 545 --seed 545 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_300_5 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 546, Buffer Size 300, Buffer Id 6, Seed 546, 2018-02-14 13:26:22.993552';
echo '=================> Gep_Fddpg Withnoise: Trial 546, Buffer Size 300, Buffer Id 6, Seed 546, 2018-02-14 13:26:22.993552' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 546 --seed 546 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_300_6 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 547, Buffer Size 300, Buffer Id 7, Seed 547, 2018-02-14 13:26:22.993572';
echo '=================> Gep_Fddpg Withnoise: Trial 547, Buffer Size 300, Buffer Id 7, Seed 547, 2018-02-14 13:26:22.993572' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 547 --seed 547 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_300_7 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 548, Buffer Size 300, Buffer Id 8, Seed 548, 2018-02-14 13:26:22.993593';
echo '=================> Gep_Fddpg Withnoise: Trial 548, Buffer Size 300, Buffer Id 8, Seed 548, 2018-02-14 13:26:22.993593' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 548 --seed 548 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_300_8 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 549, Buffer Size 300, Buffer Id 9, Seed 549, 2018-02-14 13:26:22.993613';
echo '=================> Gep_Fddpg Withnoise: Trial 549, Buffer Size 300, Buffer Id 9, Seed 549, 2018-02-14 13:26:22.993613' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 549 --seed 549 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_300_9 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 550, Buffer Size 300, Buffer Id 10, Seed 550, 2018-02-14 13:26:22.993633';
echo '=================> Gep_Fddpg Withnoise: Trial 550, Buffer Size 300, Buffer Id 10, Seed 550, 2018-02-14 13:26:22.993633' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 550 --seed 550 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_300_10 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 551, Buffer Size 300, Buffer Id 11, Seed 551, 2018-02-14 13:26:22.993656';
echo '=================> Gep_Fddpg Withnoise: Trial 551, Buffer Size 300, Buffer Id 11, Seed 551, 2018-02-14 13:26:22.993656' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 551 --seed 551 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_300_11 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 552, Buffer Size 300, Buffer Id 12, Seed 552, 2018-02-14 13:26:22.993677';
echo '=================> Gep_Fddpg Withnoise: Trial 552, Buffer Size 300, Buffer Id 12, Seed 552, 2018-02-14 13:26:22.993677' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 552 --seed 552 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_300_12 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 553, Buffer Size 300, Buffer Id 13, Seed 553, 2018-02-14 13:26:22.993699';
echo '=================> Gep_Fddpg Withnoise: Trial 553, Buffer Size 300, Buffer Id 13, Seed 553, 2018-02-14 13:26:22.993699' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 553 --seed 553 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_300_13 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 554, Buffer Size 300, Buffer Id 14, Seed 554, 2018-02-14 13:26:22.993721';
echo '=================> Gep_Fddpg Withnoise: Trial 554, Buffer Size 300, Buffer Id 14, Seed 554, 2018-02-14 13:26:22.993721' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 554 --seed 554 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_300_14 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 555, Buffer Size 300, Buffer Id 15, Seed 555, 2018-02-14 13:26:22.993786';
echo '=================> Gep_Fddpg Withnoise: Trial 555, Buffer Size 300, Buffer Id 15, Seed 555, 2018-02-14 13:26:22.993786' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 555 --seed 555 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_300_15 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 556, Buffer Size 300, Buffer Id 16, Seed 556, 2018-02-14 13:26:22.993811';
echo '=================> Gep_Fddpg Withnoise: Trial 556, Buffer Size 300, Buffer Id 16, Seed 556, 2018-02-14 13:26:22.993811' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 556 --seed 556 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_300_16 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 557, Buffer Size 300, Buffer Id 17, Seed 557, 2018-02-14 13:26:22.993833';
echo '=================> Gep_Fddpg Withnoise: Trial 557, Buffer Size 300, Buffer Id 17, Seed 557, 2018-02-14 13:26:22.993833' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 557 --seed 557 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_300_17 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 558, Buffer Size 300, Buffer Id 18, Seed 558, 2018-02-14 13:26:22.993853';
echo '=================> Gep_Fddpg Withnoise: Trial 558, Buffer Size 300, Buffer Id 18, Seed 558, 2018-02-14 13:26:22.993853' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 558 --seed 558 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_300_18 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 559, Buffer Size 300, Buffer Id 19, Seed 559, 2018-02-14 13:26:22.993873';
echo '=================> Gep_Fddpg Withnoise: Trial 559, Buffer Size 300, Buffer Id 19, Seed 559, 2018-02-14 13:26:22.993873' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 559 --seed 559 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_300_19 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 560, Buffer Size 300, Buffer Id 20, Seed 560, 2018-02-14 13:26:22.993893';
echo '=================> Gep_Fddpg Withnoise: Trial 560, Buffer Size 300, Buffer Id 20, Seed 560, 2018-02-14 13:26:22.993893' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 560 --seed 560 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_300_20 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait