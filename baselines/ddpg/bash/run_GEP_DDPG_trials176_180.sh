#!/bin/sh
export EXP_INTERP='/usr/bin/python3' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withparamnoise: Trial 176, Buffer Size 500, Buffer Id 36, Seed 176, 2018-01-31 15:27:04.113947';
echo '=================> Gep-Ddpg Withparamnoise: Trial 176, Buffer Size 500, Buffer Id 36, Seed 176, 2018-01-31 15:27:04.113947' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 176 --seed 176  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_36_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withparamnoise: Trial 177, Buffer Size 500, Buffer Id 37, Seed 177, 2018-01-31 15:27:04.113979';
echo '=================> Gep-Ddpg Withparamnoise: Trial 177, Buffer Size 500, Buffer Id 37, Seed 177, 2018-01-31 15:27:04.113979' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 177 --seed 177  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_37_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withparamnoise: Trial 178, Buffer Size 500, Buffer Id 38, Seed 178, 2018-01-31 15:27:04.113994';
echo '=================> Gep-Ddpg Withparamnoise: Trial 178, Buffer Size 500, Buffer Id 38, Seed 178, 2018-01-31 15:27:04.113994' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 178 --seed 178  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_38_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withparamnoise: Trial 179, Buffer Size 500, Buffer Id 39, Seed 179, 2018-01-31 15:27:04.114006';
echo '=================> Gep-Ddpg Withparamnoise: Trial 179, Buffer Size 500, Buffer Id 39, Seed 179, 2018-01-31 15:27:04.114006' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 179 --seed 179  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_39_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withparamnoise: Trial 180, Buffer Size 500, Buffer Id 40, Seed 180, 2018-01-31 15:27:04.114017';
echo '=================> Gep-Ddpg Withparamnoise: Trial 180, Buffer Size 500, Buffer Id 40, Seed 180, 2018-01-31 15:27:04.114017' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 180 --seed 180  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_40_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
