#!/bin/sh
#SBATCH -n1
#SBATCH -p long_sirocco
#SBATCH -t 60:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_GEP_DDPG_trials191_200.sh.err
#SBATCH -o run_GEP_DDPG_trials191_200.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withparamnoise: Trial 191, Buffer Size 500, Buffer Id 71, Seed 191, 2018-02-02 13:56:26.448576';
echo '=================> Gep-Ddpg Withparamnoise: Trial 191, Buffer Size 500, Buffer Id 71, Seed 191, 2018-02-02 13:56:26.448576' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 191 --seed 191  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_71 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 192, Buffer Size 500, Buffer Id 72, Seed 192, 2018-02-02 13:56:26.448624';
echo '=================> Gep-Ddpg Withparamnoise: Trial 192, Buffer Size 500, Buffer Id 72, Seed 192, 2018-02-02 13:56:26.448624' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 192 --seed 192  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_72 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 193, Buffer Size 500, Buffer Id 73, Seed 193, 2018-02-02 13:56:26.448652';
echo '=================> Gep-Ddpg Withparamnoise: Trial 193, Buffer Size 500, Buffer Id 73, Seed 193, 2018-02-02 13:56:26.448652' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 193 --seed 193  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_73 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 194, Buffer Size 500, Buffer Id 74, Seed 194, 2018-02-02 13:56:26.448676';
echo '=================> Gep-Ddpg Withparamnoise: Trial 194, Buffer Size 500, Buffer Id 74, Seed 194, 2018-02-02 13:56:26.448676' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 194 --seed 194  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_74 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 195, Buffer Size 500, Buffer Id 75, Seed 195, 2018-02-02 13:56:26.448698';
echo '=================> Gep-Ddpg Withparamnoise: Trial 195, Buffer Size 500, Buffer Id 75, Seed 195, 2018-02-02 13:56:26.448698' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 195 --seed 195  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_75 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 196, Buffer Size 500, Buffer Id 76, Seed 196, 2018-02-02 13:56:26.448723';
echo '=================> Gep-Ddpg Withparamnoise: Trial 196, Buffer Size 500, Buffer Id 76, Seed 196, 2018-02-02 13:56:26.448723' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 196 --seed 196  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_76 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 197, Buffer Size 500, Buffer Id 77, Seed 197, 2018-02-02 13:56:26.448743';
echo '=================> Gep-Ddpg Withparamnoise: Trial 197, Buffer Size 500, Buffer Id 77, Seed 197, 2018-02-02 13:56:26.448743' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 197 --seed 197  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_77 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 198, Buffer Size 500, Buffer Id 78, Seed 198, 2018-02-02 13:56:26.448766';
echo '=================> Gep-Ddpg Withparamnoise: Trial 198, Buffer Size 500, Buffer Id 78, Seed 198, 2018-02-02 13:56:26.448766' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 198 --seed 198  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_78 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 199, Buffer Size 500, Buffer Id 79, Seed 199, 2018-02-02 13:56:26.448788';
echo '=================> Gep-Ddpg Withparamnoise: Trial 199, Buffer Size 500, Buffer Id 79, Seed 199, 2018-02-02 13:56:26.448788' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 199 --seed 199  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_79 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 200, Buffer Size 500, Buffer Id 80, Seed 200, 2018-02-02 13:56:26.448809';
echo '=================> Gep-Ddpg Withparamnoise: Trial 200, Buffer Size 500, Buffer Id 80, Seed 200, 2018-02-02 13:56:26.448809' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 200 --seed 200  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_80 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait