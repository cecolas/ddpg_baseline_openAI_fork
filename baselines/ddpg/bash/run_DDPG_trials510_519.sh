#!/bin/sh
#SBATCH -n10
#SBATCH -p long_sirocco
#SBATCH -t 25:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_DDPG_trials510_519.sh.err
#SBATCH -o run_DDPG_trials510_519.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Ddpg Withnoise: Trial 510, Seed 510, 2018-02-06 11:43:18.205986';
echo '=================> Ddpg Withnoise: Trial 510, Seed 510, 2018-02-06 11:43:18.205986' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc_histo.py --trial_id 510 --study DDPG --noise_type ou_0.3 --seed 510 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 511, Seed 511, 2018-02-06 11:43:18.206026';
echo '=================> Ddpg Withnoise: Trial 511, Seed 511, 2018-02-06 11:43:18.206026' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc_histo.py --trial_id 511 --study DDPG --noise_type ou_0.3 --seed 511 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 512, Seed 512, 2018-02-06 11:43:18.206041';
echo '=================> Ddpg Withnoise: Trial 512, Seed 512, 2018-02-06 11:43:18.206041' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc_histo.py --trial_id 512 --study DDPG --noise_type ou_0.3 --seed 512 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 513, Seed 513, 2018-02-06 11:43:18.206053';
echo '=================> Ddpg Withnoise: Trial 513, Seed 513, 2018-02-06 11:43:18.206053' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc_histo.py --trial_id 513 --study DDPG --noise_type ou_0.3 --seed 513 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 514, Seed 514, 2018-02-06 11:43:18.206064';
echo '=================> Ddpg Withnoise: Trial 514, Seed 514, 2018-02-06 11:43:18.206064' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc_histo.py --trial_id 514 --study DDPG --noise_type ou_0.3 --seed 514 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 515, Seed 515, 2018-02-06 11:43:18.206076';
echo '=================> Ddpg Withnoise: Trial 515, Seed 515, 2018-02-06 11:43:18.206076' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc_histo.py --trial_id 515 --study DDPG --noise_type ou_0.3 --seed 515 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 516, Seed 516, 2018-02-06 11:43:18.206087';
echo '=================> Ddpg Withnoise: Trial 516, Seed 516, 2018-02-06 11:43:18.206087' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc_histo.py --trial_id 516 --study DDPG --noise_type ou_0.3 --seed 516 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 517, Seed 517, 2018-02-06 11:43:18.206098';
echo '=================> Ddpg Withnoise: Trial 517, Seed 517, 2018-02-06 11:43:18.206098' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc_histo.py --trial_id 517 --study DDPG --noise_type ou_0.3 --seed 517 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 518, Seed 518, 2018-02-06 11:43:18.206109';
echo '=================> Ddpg Withnoise: Trial 518, Seed 518, 2018-02-06 11:43:18.206109' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc_histo.py --trial_id 518 --study DDPG --noise_type ou_0.3 --seed 518 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 519, Seed 519, 2018-02-06 11:43:18.206120';
echo '=================> Ddpg Withnoise: Trial 519, Seed 519, 2018-02-06 11:43:18.206120' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc_histo.py --trial_id 519 --study DDPG --noise_type ou_0.3 --seed 519 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait