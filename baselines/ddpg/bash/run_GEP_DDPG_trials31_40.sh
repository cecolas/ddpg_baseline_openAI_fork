#!/bin/sh
#SBATCH -n1
#SBATCH -p long_sirocco
#SBATCH -t 30:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_GEP_DDPG_trials31_40.sh.err
#SBATCH -o run_GEP_DDPG_trials31_40.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withnoise: Trial 31, Buffer Size 500, Buffer Id 11, Seed 31, 2018-01-27 11:23:00.619075';
echo '=================> Gep-Ddpg Withnoise: Trial 31, Buffer Size 500, Buffer Id 11, Seed 31, 2018-01-27 11:23:00.619075' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 31 --seed 31 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_11_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 32, Buffer Size 500, Buffer Id 12, Seed 32, 2018-01-27 11:23:00.619116';
echo '=================> Gep-Ddpg Withnoise: Trial 32, Buffer Size 500, Buffer Id 12, Seed 32, 2018-01-27 11:23:00.619116' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 32 --seed 32 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_12_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 33, Buffer Size 500, Buffer Id 13, Seed 33, 2018-01-27 11:23:00.619136';
echo '=================> Gep-Ddpg Withnoise: Trial 33, Buffer Size 500, Buffer Id 13, Seed 33, 2018-01-27 11:23:00.619136' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 33 --seed 33 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_13_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 34, Buffer Size 500, Buffer Id 14, Seed 34, 2018-01-27 11:23:00.619154';
echo '=================> Gep-Ddpg Withnoise: Trial 34, Buffer Size 500, Buffer Id 14, Seed 34, 2018-01-27 11:23:00.619154' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 34 --seed 34 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_14_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 35, Buffer Size 500, Buffer Id 15, Seed 35, 2018-01-27 11:23:00.619170';
echo '=================> Gep-Ddpg Withnoise: Trial 35, Buffer Size 500, Buffer Id 15, Seed 35, 2018-01-27 11:23:00.619170' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 35 --seed 35 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_15_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 36, Buffer Size 500, Buffer Id 16, Seed 36, 2018-01-27 11:23:00.619189';
echo '=================> Gep-Ddpg Withnoise: Trial 36, Buffer Size 500, Buffer Id 16, Seed 36, 2018-01-27 11:23:00.619189' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 36 --seed 36 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_16_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 37, Buffer Size 500, Buffer Id 17, Seed 37, 2018-01-27 11:23:00.619207';
echo '=================> Gep-Ddpg Withnoise: Trial 37, Buffer Size 500, Buffer Id 17, Seed 37, 2018-01-27 11:23:00.619207' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 37 --seed 37 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_17_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 38, Buffer Size 500, Buffer Id 18, Seed 38, 2018-01-27 11:23:00.619226';
echo '=================> Gep-Ddpg Withnoise: Trial 38, Buffer Size 500, Buffer Id 18, Seed 38, 2018-01-27 11:23:00.619226' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 38 --seed 38 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_18_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 39, Buffer Size 500, Buffer Id 19, Seed 39, 2018-01-27 11:23:00.619240';
echo '=================> Gep-Ddpg Withnoise: Trial 39, Buffer Size 500, Buffer Id 19, Seed 39, 2018-01-27 11:23:00.619240' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 39 --seed 39 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_19_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 40, Buffer Size 500, Buffer Id 20, Seed 40, 2018-01-27 11:23:00.619250';
echo '=================> Gep-Ddpg Withnoise: Trial 40, Buffer Size 500, Buffer Id 20, Seed 40, 2018-01-27 11:23:00.619250' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 40 --seed 40 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_20_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait