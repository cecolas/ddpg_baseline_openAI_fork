#!/usr/bin/python
# -*- coding: utf-8 -*-

from itertools import *
import datetime

# PATH_TO_RESULTS = '/mnt/shared/cedric/DDPG/'
# PATH_TO_INTERPRETER = "/usr/bin/python3"
PATH_TO_RESULTS = '/mnt/shared/cedric/DDPG/results/'

PATH_TO_INTERPRETER = "/usr/bin/python3"

trial_id = list(range(176,181))
frozen = False
study = 'GEP_DDPG'
ddpg_noise = 'withParamNoise'
size_buff = 500
buffer_folder = '/mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/'

filename = 'run_GEP_DDPG_trials' + str(trial_id[0])+'_'+str(trial_id[-1])+'.sh'
with open(filename, 'w') as f:
    f.write('#!/bin/sh\n')
    f.write("export EXP_INTERP='%s' ;\n" % PATH_TO_INTERPRETER)
    f.write('ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"\n')
    f.write('agpu=0\n')
    for i,buff_id in enumerate(list(range(36,41))):
        buffer_name = 'Cheetah_model_%s_%s_buffer' %(str(size_buff), str(buff_id))
        print('buffer_name :',buffer_name)
        t_id = trial_id[i]
        name = ("GEP-DDPG %s: trial %s, buffer size %s, buffer id %s, seed %s, %s" % (ddpg_noise,str(t_id),str(size_buff),str(buff_id), str(t_id), str(datetime.datetime.now()))).title()
        f.write("echo '=================> %s';\n" % name)
        f.write("echo '=================> %s' >> log.txt;\n" % name)
        f.write("export CUDA_VISIBLE_DEVICES=$agpu\n")
        f.write("agpu=$(((agpu+1)%ngpu))\n")
        if ddpg_noise == 'withNoise':
            f.write("$EXP_INTERP main_fl_gpu.py --trial_id %i --seed %i --noise_type %s --study %s --buffer_location %s --data_path %s \n"
                    % (t_id, t_id, 'ou_0.3', study,  buffer_folder+buffer_name, PATH_TO_RESULTS))
        elif ddpg_noise == 'withoutNoise':
            f.write("$EXP_INTERP main_fl_gpu.py --trial_id %i --seed %i  --noise_type %s --study %s --buffer_location %s --data_path %s \n"
                % (t_id, t_id, 'none', study, buffer_folder + buffer_name, PATH_TO_RESULTS))
        elif ddpg_noise == 'withParamNoise':
            f.write("$EXP_INTERP main_fl_gpu.py --trial_id %i --seed %i  --noise_type %s --study %s --buffer_location %s --data_path %s \n"
                % (t_id, t_id, 'adaptive-param_0.2', study, buffer_folder + buffer_name, PATH_TO_RESULTS))
        else:
            raise NotImplementedError
