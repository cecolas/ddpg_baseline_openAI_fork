#!/bin/sh
#SBATCH -n10
#SBATCH -p long_sirocco
#SBATCH -t 25:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_GEP_DDPG_trials211_220.sh.err
#SBATCH -o run_GEP_DDPG_trials211_220.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withparamnoise: Trial 217, Buffer Size 50, Buffer Id 37, Seed 217, 2018-02-06 13:25:23.498310';
echo '=================> Gep-Ddpg Withparamnoise: Trial 217, Buffer Size 50, Buffer Id 37, Seed 217, 2018-02-06 13:25:23.498310' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 217 --seed 217  --noise_type adaptive-param_0.2 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_37 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 218, Buffer Size 50, Buffer Id 38, Seed 218, 2018-02-06 13:25:23.498330';
echo '=================> Gep-Ddpg Withparamnoise: Trial 218, Buffer Size 50, Buffer Id 38, Seed 218, 2018-02-06 13:25:23.498330' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 218 --seed 218  --noise_type adaptive-param_0.2 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_38 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 219, Buffer Size 50, Buffer Id 39, Seed 219, 2018-02-06 13:25:23.498350';
echo '=================> Gep-Ddpg Withparamnoise: Trial 219, Buffer Size 50, Buffer Id 39, Seed 219, 2018-02-06 13:25:23.498350' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 219 --seed 219  --noise_type adaptive-param_0.2 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_39 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 220, Buffer Size 50, Buffer Id 40, Seed 220, 2018-02-06 13:25:23.498368';
echo '=================> Gep-Ddpg Withparamnoise: Trial 220, Buffer Size 50, Buffer Id 40, Seed 220, 2018-02-06 13:25:23.498368' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 220 --seed 220  --noise_type adaptive-param_0.2 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_40 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait