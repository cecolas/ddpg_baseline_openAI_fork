#!/bin/sh
#SBATCH -n1
#SBATCH -p long_sirocco
#SBATCH -t 30:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_GEP_DDPG_trials21_30.sh.err
#SBATCH -o run_GEP_DDPG_trials21_30.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withnoise: Trial 21, Buffer Size 500, Buffer Id 1, Seed 21, 2018-01-27 11:22:31.003467';
echo '=================> Gep-Ddpg Withnoise: Trial 21, Buffer Size 500, Buffer Id 1, Seed 21, 2018-01-27 11:22:31.003467' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 21 --seed 21 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_1_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 22, Buffer Size 500, Buffer Id 2, Seed 22, 2018-01-27 11:22:31.003509';
echo '=================> Gep-Ddpg Withnoise: Trial 22, Buffer Size 500, Buffer Id 2, Seed 22, 2018-01-27 11:22:31.003509' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 22 --seed 22 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_2_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 23, Buffer Size 500, Buffer Id 3, Seed 23, 2018-01-27 11:22:31.003529';
echo '=================> Gep-Ddpg Withnoise: Trial 23, Buffer Size 500, Buffer Id 3, Seed 23, 2018-01-27 11:22:31.003529' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 23 --seed 23 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_3_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 24, Buffer Size 500, Buffer Id 4, Seed 24, 2018-01-27 11:22:31.003546';
echo '=================> Gep-Ddpg Withnoise: Trial 24, Buffer Size 500, Buffer Id 4, Seed 24, 2018-01-27 11:22:31.003546' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 24 --seed 24 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_4_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 25, Buffer Size 500, Buffer Id 5, Seed 25, 2018-01-27 11:22:31.003560';
echo '=================> Gep-Ddpg Withnoise: Trial 25, Buffer Size 500, Buffer Id 5, Seed 25, 2018-01-27 11:22:31.003560' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 25 --seed 25 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_5_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 26, Buffer Size 500, Buffer Id 6, Seed 26, 2018-01-27 11:22:31.003576';
echo '=================> Gep-Ddpg Withnoise: Trial 26, Buffer Size 500, Buffer Id 6, Seed 26, 2018-01-27 11:22:31.003576' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 26 --seed 26 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_6_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 27, Buffer Size 500, Buffer Id 7, Seed 27, 2018-01-27 11:22:31.003590';
echo '=================> Gep-Ddpg Withnoise: Trial 27, Buffer Size 500, Buffer Id 7, Seed 27, 2018-01-27 11:22:31.003590' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 27 --seed 27 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_7_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 28, Buffer Size 500, Buffer Id 8, Seed 28, 2018-01-27 11:22:31.003607';
echo '=================> Gep-Ddpg Withnoise: Trial 28, Buffer Size 500, Buffer Id 8, Seed 28, 2018-01-27 11:22:31.003607' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 28 --seed 28 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_8_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 29, Buffer Size 500, Buffer Id 9, Seed 29, 2018-01-27 11:22:31.003623';
echo '=================> Gep-Ddpg Withnoise: Trial 29, Buffer Size 500, Buffer Id 9, Seed 29, 2018-01-27 11:22:31.003623' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 29 --seed 29 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_9_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 30, Buffer Size 500, Buffer Id 10, Seed 30, 2018-01-27 11:22:31.003638';
echo '=================> Gep-Ddpg Withnoise: Trial 30, Buffer Size 500, Buffer Id 10, Seed 30, 2018-01-27 11:22:31.003638' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 30 --seed 30 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_10_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait