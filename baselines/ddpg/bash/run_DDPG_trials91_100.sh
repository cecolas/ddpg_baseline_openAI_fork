#!/bin/sh
#SBATCH -n1
#SBATCH -p long_sirocco
#SBATCH -t 60:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_DDPG_trials91_100.sh.err
#SBATCH -o run_DDPG_trials91_100.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Ddpg Withparamnoise: Trial 91, Seed 91, 2018-01-30 16:15:35.066600';
echo '=================> Ddpg Withparamnoise: Trial 91, Seed 91, 2018-01-30 16:15:35.066600' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 91 --study DDPG --noise_type adaptive-param_0.2 --seed 91 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 92, Seed 92, 2018-01-30 16:15:35.066635';
echo '=================> Ddpg Withparamnoise: Trial 92, Seed 92, 2018-01-30 16:15:35.066635' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 92 --study DDPG --noise_type adaptive-param_0.2 --seed 92 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 93, Seed 93, 2018-01-30 16:15:35.066651';
echo '=================> Ddpg Withparamnoise: Trial 93, Seed 93, 2018-01-30 16:15:35.066651' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 93 --study DDPG --noise_type adaptive-param_0.2 --seed 93 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 94, Seed 94, 2018-01-30 16:15:35.066664';
echo '=================> Ddpg Withparamnoise: Trial 94, Seed 94, 2018-01-30 16:15:35.066664' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 94 --study DDPG --noise_type adaptive-param_0.2 --seed 94 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 95, Seed 95, 2018-01-30 16:15:35.066676';
echo '=================> Ddpg Withparamnoise: Trial 95, Seed 95, 2018-01-30 16:15:35.066676' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 95 --study DDPG --noise_type adaptive-param_0.2 --seed 95 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 96, Seed 96, 2018-01-30 16:15:35.066688';
echo '=================> Ddpg Withparamnoise: Trial 96, Seed 96, 2018-01-30 16:15:35.066688' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 96 --study DDPG --noise_type adaptive-param_0.2 --seed 96 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 97, Seed 97, 2018-01-30 16:15:35.066700';
echo '=================> Ddpg Withparamnoise: Trial 97, Seed 97, 2018-01-30 16:15:35.066700' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 97 --study DDPG --noise_type adaptive-param_0.2 --seed 97 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 98, Seed 98, 2018-01-30 16:15:35.066711';
echo '=================> Ddpg Withparamnoise: Trial 98, Seed 98, 2018-01-30 16:15:35.066711' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 98 --study DDPG --noise_type adaptive-param_0.2 --seed 98 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 99, Seed 99, 2018-01-30 16:15:35.066723';
echo '=================> Ddpg Withparamnoise: Trial 99, Seed 99, 2018-01-30 16:15:35.066723' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 99 --study DDPG --noise_type adaptive-param_0.2 --seed 99 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 100, Seed 100, 2018-01-30 16:15:35.066734';
echo '=================> Ddpg Withparamnoise: Trial 100, Seed 100, 2018-01-30 16:15:35.066734' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 100 --study DDPG --noise_type adaptive-param_0.2 --seed 100 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait