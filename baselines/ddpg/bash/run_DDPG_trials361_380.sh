#!/bin/sh
#SBATCH -n20
#SBATCH -p long_sirocco
#SBATCH -t 30:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_DDPG_trials361_380.sh.err
#SBATCH -o run_DDPG_trials361_380.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Ddpg Withnoise: Trial 361, Seed 361, 2018-02-07 13:49:59.298008';
echo '=================> Ddpg Withnoise: Trial 361, Seed 361, 2018-02-07 13:49:59.298008' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 361 --study DDPG --noise_type ou_0.0 --nb-epochs 250 --seed 361 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 362, Seed 362, 2018-02-07 13:49:59.298046';
echo '=================> Ddpg Withnoise: Trial 362, Seed 362, 2018-02-07 13:49:59.298046' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 362 --study DDPG --noise_type ou_0.0 --nb-epochs 250 --seed 362 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 363, Seed 363, 2018-02-07 13:49:59.298059';
echo '=================> Ddpg Withnoise: Trial 363, Seed 363, 2018-02-07 13:49:59.298059' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 363 --study DDPG --noise_type ou_0.0 --nb-epochs 250 --seed 363 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 364, Seed 364, 2018-02-07 13:49:59.298070';
echo '=================> Ddpg Withnoise: Trial 364, Seed 364, 2018-02-07 13:49:59.298070' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 364 --study DDPG --noise_type ou_0.0 --nb-epochs 250 --seed 364 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 365, Seed 365, 2018-02-07 13:49:59.298080';
echo '=================> Ddpg Withnoise: Trial 365, Seed 365, 2018-02-07 13:49:59.298080' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 365 --study DDPG --noise_type ou_0.0 --nb-epochs 250 --seed 365 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 366, Seed 366, 2018-02-07 13:49:59.298091';
echo '=================> Ddpg Withnoise: Trial 366, Seed 366, 2018-02-07 13:49:59.298091' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 366 --study DDPG --noise_type ou_0.0 --nb-epochs 250 --seed 366 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 367, Seed 367, 2018-02-07 13:49:59.298100';
echo '=================> Ddpg Withnoise: Trial 367, Seed 367, 2018-02-07 13:49:59.298100' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 367 --study DDPG --noise_type ou_0.0 --nb-epochs 250 --seed 367 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 368, Seed 368, 2018-02-07 13:49:59.298110';
echo '=================> Ddpg Withnoise: Trial 368, Seed 368, 2018-02-07 13:49:59.298110' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 368 --study DDPG --noise_type ou_0.0 --nb-epochs 250 --seed 368 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 369, Seed 369, 2018-02-07 13:49:59.298120';
echo '=================> Ddpg Withnoise: Trial 369, Seed 369, 2018-02-07 13:49:59.298120' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 369 --study DDPG --noise_type ou_0.0 --nb-epochs 250 --seed 369 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 370, Seed 370, 2018-02-07 13:49:59.298130';
echo '=================> Ddpg Withnoise: Trial 370, Seed 370, 2018-02-07 13:49:59.298130' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 370 --study DDPG --noise_type ou_0.0 --nb-epochs 250 --seed 370 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 371, Seed 371, 2018-02-07 13:49:59.298140';
echo '=================> Ddpg Withnoise: Trial 371, Seed 371, 2018-02-07 13:49:59.298140' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 371 --study DDPG --noise_type ou_0.0 --nb-epochs 250 --seed 371 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 372, Seed 372, 2018-02-07 13:49:59.298150';
echo '=================> Ddpg Withnoise: Trial 372, Seed 372, 2018-02-07 13:49:59.298150' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 372 --study DDPG --noise_type ou_0.0 --nb-epochs 250 --seed 372 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 373, Seed 373, 2018-02-07 13:49:59.298160';
echo '=================> Ddpg Withnoise: Trial 373, Seed 373, 2018-02-07 13:49:59.298160' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 373 --study DDPG --noise_type ou_0.0 --nb-epochs 250 --seed 373 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 374, Seed 374, 2018-02-07 13:49:59.298170';
echo '=================> Ddpg Withnoise: Trial 374, Seed 374, 2018-02-07 13:49:59.298170' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 374 --study DDPG --noise_type ou_0.0 --nb-epochs 250 --seed 374 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 375, Seed 375, 2018-02-07 13:49:59.298180';
echo '=================> Ddpg Withnoise: Trial 375, Seed 375, 2018-02-07 13:49:59.298180' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 375 --study DDPG --noise_type ou_0.0 --nb-epochs 250 --seed 375 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 376, Seed 376, 2018-02-07 13:49:59.298190';
echo '=================> Ddpg Withnoise: Trial 376, Seed 376, 2018-02-07 13:49:59.298190' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 376 --study DDPG --noise_type ou_0.0 --nb-epochs 250 --seed 376 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 377, Seed 377, 2018-02-07 13:49:59.298200';
echo '=================> Ddpg Withnoise: Trial 377, Seed 377, 2018-02-07 13:49:59.298200' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 377 --study DDPG --noise_type ou_0.0 --nb-epochs 250 --seed 377 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 378, Seed 378, 2018-02-07 13:49:59.298210';
echo '=================> Ddpg Withnoise: Trial 378, Seed 378, 2018-02-07 13:49:59.298210' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 378 --study DDPG --noise_type ou_0.0 --nb-epochs 250 --seed 378 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 379, Seed 379, 2018-02-07 13:49:59.298222';
echo '=================> Ddpg Withnoise: Trial 379, Seed 379, 2018-02-07 13:49:59.298222' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 379 --study DDPG --noise_type ou_0.0 --nb-epochs 250 --seed 379 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 380, Seed 380, 2018-02-07 13:49:59.298270';
echo '=================> Ddpg Withnoise: Trial 380, Seed 380, 2018-02-07 13:49:59.298270' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 380 --study DDPG --noise_type ou_0.0 --nb-epochs 250 --seed 380 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait