#!/bin/sh
#SBATCH -n1
#SBATCH -p long_sirocco
#SBATCH -t 30:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_DDPG_trials41_60.sh.err
#SBATCH -o run_DDPG_trials41_60.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Ddpg Withnoise: Trial 41, Seed 41, 2018-01-26 17:19:07.724380';
echo '=================> Ddpg Withnoise: Trial 41, Seed 41, 2018-01-26 17:19:07.724380' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 41 --study DDPG --noise_type ou_0.3 --seed 41 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 42, Seed 42, 2018-01-26 17:19:07.724401';
echo '=================> Ddpg Withnoise: Trial 42, Seed 42, 2018-01-26 17:19:07.724401' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 42 --study DDPG --noise_type ou_0.3 --seed 42 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 43, Seed 43, 2018-01-26 17:19:07.724409';
echo '=================> Ddpg Withnoise: Trial 43, Seed 43, 2018-01-26 17:19:07.724409' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 43 --study DDPG --noise_type ou_0.3 --seed 43 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 44, Seed 44, 2018-01-26 17:19:07.724415';
echo '=================> Ddpg Withnoise: Trial 44, Seed 44, 2018-01-26 17:19:07.724415' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 44 --study DDPG --noise_type ou_0.3 --seed 44 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 45, Seed 45, 2018-01-26 17:19:07.724420';
echo '=================> Ddpg Withnoise: Trial 45, Seed 45, 2018-01-26 17:19:07.724420' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 45 --study DDPG --noise_type ou_0.3 --seed 45 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 46, Seed 46, 2018-01-26 17:19:07.724425';
echo '=================> Ddpg Withnoise: Trial 46, Seed 46, 2018-01-26 17:19:07.724425' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 46 --study DDPG --noise_type ou_0.3 --seed 46 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 47, Seed 47, 2018-01-26 17:19:07.724430';
echo '=================> Ddpg Withnoise: Trial 47, Seed 47, 2018-01-26 17:19:07.724430' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 47 --study DDPG --noise_type ou_0.3 --seed 47 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 48, Seed 48, 2018-01-26 17:19:07.724438';
echo '=================> Ddpg Withnoise: Trial 48, Seed 48, 2018-01-26 17:19:07.724438' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 48 --study DDPG --noise_type ou_0.3 --seed 48 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 49, Seed 49, 2018-01-26 17:19:07.724443';
echo '=================> Ddpg Withnoise: Trial 49, Seed 49, 2018-01-26 17:19:07.724443' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 49 --study DDPG --noise_type ou_0.3 --seed 49 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 50, Seed 50, 2018-01-26 17:19:07.724450';
echo '=================> Ddpg Withnoise: Trial 50, Seed 50, 2018-01-26 17:19:07.724450' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 50 --study DDPG --noise_type ou_0.3 --seed 50 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 51, Seed 51, 2018-01-26 17:19:07.724457';
echo '=================> Ddpg Withnoise: Trial 51, Seed 51, 2018-01-26 17:19:07.724457' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 51 --study DDPG --noise_type ou_0.3 --seed 51 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 52, Seed 52, 2018-01-26 17:19:07.724462';
echo '=================> Ddpg Withnoise: Trial 52, Seed 52, 2018-01-26 17:19:07.724462' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 52 --study DDPG --noise_type ou_0.3 --seed 52 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 53, Seed 53, 2018-01-26 17:19:07.724467';
echo '=================> Ddpg Withnoise: Trial 53, Seed 53, 2018-01-26 17:19:07.724467' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 53 --study DDPG --noise_type ou_0.3 --seed 53 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 54, Seed 54, 2018-01-26 17:19:07.724472';
echo '=================> Ddpg Withnoise: Trial 54, Seed 54, 2018-01-26 17:19:07.724472' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 54 --study DDPG --noise_type ou_0.3 --seed 54 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 55, Seed 55, 2018-01-26 17:19:07.724480';
echo '=================> Ddpg Withnoise: Trial 55, Seed 55, 2018-01-26 17:19:07.724480' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 55 --study DDPG --noise_type ou_0.3 --seed 55 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 56, Seed 56, 2018-01-26 17:19:07.724496';
echo '=================> Ddpg Withnoise: Trial 56, Seed 56, 2018-01-26 17:19:07.724496' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 56 --study DDPG --noise_type ou_0.3 --seed 56 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 57, Seed 57, 2018-01-26 17:19:07.724504';
echo '=================> Ddpg Withnoise: Trial 57, Seed 57, 2018-01-26 17:19:07.724504' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 57 --study DDPG --noise_type ou_0.3 --seed 57 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 58, Seed 58, 2018-01-26 17:19:07.724515';
echo '=================> Ddpg Withnoise: Trial 58, Seed 58, 2018-01-26 17:19:07.724515' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 58 --study DDPG --noise_type ou_0.3 --seed 58 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 59, Seed 59, 2018-01-26 17:19:07.724526';
echo '=================> Ddpg Withnoise: Trial 59, Seed 59, 2018-01-26 17:19:07.724526' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 59 --study DDPG --noise_type ou_0.3 --seed 59 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 60, Seed 60, 2018-01-26 17:19:07.724536';
echo '=================> Ddpg Withnoise: Trial 60, Seed 60, 2018-01-26 17:19:07.724536' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 60 --study DDPG --noise_type ou_0.3 --seed 60 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait