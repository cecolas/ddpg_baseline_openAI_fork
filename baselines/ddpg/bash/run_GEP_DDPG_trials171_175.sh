#!/bin/sh
export EXP_INTERP='/usr/bin/python3' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withparamnoise: Trial 171, Buffer Size 500, Buffer Id 31, Seed 171, 2018-01-31 15:26:54.770441';
echo '=================> Gep-Ddpg Withparamnoise: Trial 171, Buffer Size 500, Buffer Id 31, Seed 171, 2018-01-31 15:26:54.770441' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 171 --seed 171  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_31_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withparamnoise: Trial 172, Buffer Size 500, Buffer Id 32, Seed 172, 2018-01-31 15:26:54.770488';
echo '=================> Gep-Ddpg Withparamnoise: Trial 172, Buffer Size 500, Buffer Id 32, Seed 172, 2018-01-31 15:26:54.770488' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 172 --seed 172  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_32_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withparamnoise: Trial 173, Buffer Size 500, Buffer Id 33, Seed 173, 2018-01-31 15:26:54.770510';
echo '=================> Gep-Ddpg Withparamnoise: Trial 173, Buffer Size 500, Buffer Id 33, Seed 173, 2018-01-31 15:26:54.770510' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 173 --seed 173  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_33_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withparamnoise: Trial 174, Buffer Size 500, Buffer Id 34, Seed 174, 2018-01-31 15:26:54.770529';
echo '=================> Gep-Ddpg Withparamnoise: Trial 174, Buffer Size 500, Buffer Id 34, Seed 174, 2018-01-31 15:26:54.770529' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 174 --seed 174  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_34_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withparamnoise: Trial 175, Buffer Size 500, Buffer Id 35, Seed 175, 2018-01-31 15:26:54.770547';
echo '=================> Gep-Ddpg Withparamnoise: Trial 175, Buffer Size 500, Buffer Id 35, Seed 175, 2018-01-31 15:26:54.770547' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 175 --seed 175  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_35_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
