#!/bin/sh
export EXP_INTERP='/usr/bin/python3' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withnoise: Trial 156, Buffer Size 500, Buffer Id 16, Seed 156, 2018-01-31 15:22:00.919008';
echo '=================> Gep-Ddpg Withnoise: Trial 156, Buffer Size 500, Buffer Id 16, Seed 156, 2018-01-31 15:22:00.919008' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 156 --seed 156 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_16_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withnoise: Trial 157, Buffer Size 500, Buffer Id 17, Seed 157, 2018-01-31 15:22:00.919059';
echo '=================> Gep-Ddpg Withnoise: Trial 157, Buffer Size 500, Buffer Id 17, Seed 157, 2018-01-31 15:22:00.919059' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 157 --seed 157 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_17_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withnoise: Trial 158, Buffer Size 500, Buffer Id 18, Seed 158, 2018-01-31 15:22:00.919085';
echo '=================> Gep-Ddpg Withnoise: Trial 158, Buffer Size 500, Buffer Id 18, Seed 158, 2018-01-31 15:22:00.919085' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 158 --seed 158 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_18_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withnoise: Trial 159, Buffer Size 500, Buffer Id 19, Seed 159, 2018-01-31 15:22:00.919106';
echo '=================> Gep-Ddpg Withnoise: Trial 159, Buffer Size 500, Buffer Id 19, Seed 159, 2018-01-31 15:22:00.919106' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 159 --seed 159 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_19_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withnoise: Trial 160, Buffer Size 500, Buffer Id 20, Seed 160, 2018-01-31 15:22:00.919126';
echo '=================> Gep-Ddpg Withnoise: Trial 160, Buffer Size 500, Buffer Id 20, Seed 160, 2018-01-31 15:22:00.919126' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 160 --seed 160 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_20_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
