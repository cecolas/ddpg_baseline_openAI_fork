#!/bin/sh
#SBATCH -n1
#SBATCH -p long_sirocco
#SBATCH -t 60:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_DDPG_trials131_140.sh.err
#SBATCH -o run_DDPG_trials131_140.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Ddpg Withdecreasingnoise: Trial 131, Seed 131, 2018-02-02 11:47:07.390671';
echo '=================> Ddpg Withdecreasingnoise: Trial 131, Seed 131, 2018-02-02 11:47:07.390671' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 131 --study DDPG --noise_type decreasing-ou_0.6 --seed 131 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 132, Seed 132, 2018-02-02 11:47:07.390710';
echo '=================> Ddpg Withdecreasingnoise: Trial 132, Seed 132, 2018-02-02 11:47:07.390710' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 132 --study DDPG --noise_type decreasing-ou_0.6 --seed 132 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 133, Seed 133, 2018-02-02 11:47:07.390727';
echo '=================> Ddpg Withdecreasingnoise: Trial 133, Seed 133, 2018-02-02 11:47:07.390727' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 133 --study DDPG --noise_type decreasing-ou_0.6 --seed 133 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 134, Seed 134, 2018-02-02 11:47:07.390739';
echo '=================> Ddpg Withdecreasingnoise: Trial 134, Seed 134, 2018-02-02 11:47:07.390739' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 134 --study DDPG --noise_type decreasing-ou_0.6 --seed 134 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 135, Seed 135, 2018-02-02 11:47:07.390752';
echo '=================> Ddpg Withdecreasingnoise: Trial 135, Seed 135, 2018-02-02 11:47:07.390752' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 135 --study DDPG --noise_type decreasing-ou_0.6 --seed 135 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 136, Seed 136, 2018-02-02 11:47:07.390769';
echo '=================> Ddpg Withdecreasingnoise: Trial 136, Seed 136, 2018-02-02 11:47:07.390769' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 136 --study DDPG --noise_type decreasing-ou_0.6 --seed 136 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 137, Seed 137, 2018-02-02 11:47:07.390781';
echo '=================> Ddpg Withdecreasingnoise: Trial 137, Seed 137, 2018-02-02 11:47:07.390781' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 137 --study DDPG --noise_type decreasing-ou_0.6 --seed 137 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 138, Seed 138, 2018-02-02 11:47:07.390793';
echo '=================> Ddpg Withdecreasingnoise: Trial 138, Seed 138, 2018-02-02 11:47:07.390793' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 138 --study DDPG --noise_type decreasing-ou_0.6 --seed 138 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 139, Seed 139, 2018-02-02 11:47:07.390805';
echo '=================> Ddpg Withdecreasingnoise: Trial 139, Seed 139, 2018-02-02 11:47:07.390805' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 139 --study DDPG --noise_type decreasing-ou_0.6 --seed 139 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 140, Seed 140, 2018-02-02 11:47:07.390817';
echo '=================> Ddpg Withdecreasingnoise: Trial 140, Seed 140, 2018-02-02 11:47:07.390817' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 140 --study DDPG --noise_type decreasing-ou_0.6 --seed 140 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait