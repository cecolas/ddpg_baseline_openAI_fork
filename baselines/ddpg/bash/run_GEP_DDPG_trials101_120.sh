#!/bin/sh
#SBATCH -n1
#SBATCH -p long_sirocco
#SBATCH -t 60:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_GEP_DDPG_trials101_120.sh.err
#SBATCH -o run_GEP_DDPG_trials101_120.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withparamnoise: Trial 101, Buffer Size 500, Buffer Id 1, Seed 101, 2018-01-29 20:34:00.292271';
echo '=================> Gep-Ddpg Withparamnoise: Trial 101, Buffer Size 500, Buffer Id 1, Seed 101, 2018-01-29 20:34:00.292271' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 101 --seed 101  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_1_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 102, Buffer Size 500, Buffer Id 2, Seed 102, 2018-01-29 20:34:00.292321';
echo '=================> Gep-Ddpg Withparamnoise: Trial 102, Buffer Size 500, Buffer Id 2, Seed 102, 2018-01-29 20:34:00.292321' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 102 --seed 102  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_2_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 103, Buffer Size 500, Buffer Id 3, Seed 103, 2018-01-29 20:34:00.292342';
echo '=================> Gep-Ddpg Withparamnoise: Trial 103, Buffer Size 500, Buffer Id 3, Seed 103, 2018-01-29 20:34:00.292342' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 103 --seed 103  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_3_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 104, Buffer Size 500, Buffer Id 4, Seed 104, 2018-01-29 20:34:00.292360';
echo '=================> Gep-Ddpg Withparamnoise: Trial 104, Buffer Size 500, Buffer Id 4, Seed 104, 2018-01-29 20:34:00.292360' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 104 --seed 104  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_4_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 105, Buffer Size 500, Buffer Id 5, Seed 105, 2018-01-29 20:34:00.292382';
echo '=================> Gep-Ddpg Withparamnoise: Trial 105, Buffer Size 500, Buffer Id 5, Seed 105, 2018-01-29 20:34:00.292382' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 105 --seed 105  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_5_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 106, Buffer Size 500, Buffer Id 6, Seed 106, 2018-01-29 20:34:00.292404';
echo '=================> Gep-Ddpg Withparamnoise: Trial 106, Buffer Size 500, Buffer Id 6, Seed 106, 2018-01-29 20:34:00.292404' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 106 --seed 106  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_6_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 107, Buffer Size 500, Buffer Id 7, Seed 107, 2018-01-29 20:34:00.292422';
echo '=================> Gep-Ddpg Withparamnoise: Trial 107, Buffer Size 500, Buffer Id 7, Seed 107, 2018-01-29 20:34:00.292422' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 107 --seed 107  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_7_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 108, Buffer Size 500, Buffer Id 8, Seed 108, 2018-01-29 20:34:00.292441';
echo '=================> Gep-Ddpg Withparamnoise: Trial 108, Buffer Size 500, Buffer Id 8, Seed 108, 2018-01-29 20:34:00.292441' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 108 --seed 108  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_8_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 109, Buffer Size 500, Buffer Id 9, Seed 109, 2018-01-29 20:34:00.292465';
echo '=================> Gep-Ddpg Withparamnoise: Trial 109, Buffer Size 500, Buffer Id 9, Seed 109, 2018-01-29 20:34:00.292465' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 109 --seed 109  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_9_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 110, Buffer Size 500, Buffer Id 10, Seed 110, 2018-01-29 20:34:00.292483';
echo '=================> Gep-Ddpg Withparamnoise: Trial 110, Buffer Size 500, Buffer Id 10, Seed 110, 2018-01-29 20:34:00.292483' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 110 --seed 110  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_10_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 111, Buffer Size 500, Buffer Id 11, Seed 111, 2018-01-29 20:34:00.292503';
echo '=================> Gep-Ddpg Withparamnoise: Trial 111, Buffer Size 500, Buffer Id 11, Seed 111, 2018-01-29 20:34:00.292503' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 111 --seed 111  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_11_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 112, Buffer Size 500, Buffer Id 12, Seed 112, 2018-01-29 20:34:00.292530';
echo '=================> Gep-Ddpg Withparamnoise: Trial 112, Buffer Size 500, Buffer Id 12, Seed 112, 2018-01-29 20:34:00.292530' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 112 --seed 112  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_12_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 113, Buffer Size 500, Buffer Id 13, Seed 113, 2018-01-29 20:34:00.292549';
echo '=================> Gep-Ddpg Withparamnoise: Trial 113, Buffer Size 500, Buffer Id 13, Seed 113, 2018-01-29 20:34:00.292549' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 113 --seed 113  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_13_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 114, Buffer Size 500, Buffer Id 14, Seed 114, 2018-01-29 20:34:00.292568';
echo '=================> Gep-Ddpg Withparamnoise: Trial 114, Buffer Size 500, Buffer Id 14, Seed 114, 2018-01-29 20:34:00.292568' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 114 --seed 114  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_14_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 115, Buffer Size 500, Buffer Id 15, Seed 115, 2018-01-29 20:34:00.292629';
echo '=================> Gep-Ddpg Withparamnoise: Trial 115, Buffer Size 500, Buffer Id 15, Seed 115, 2018-01-29 20:34:00.292629' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 115 --seed 115  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_15_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 116, Buffer Size 500, Buffer Id 16, Seed 116, 2018-01-29 20:34:00.292651';
echo '=================> Gep-Ddpg Withparamnoise: Trial 116, Buffer Size 500, Buffer Id 16, Seed 116, 2018-01-29 20:34:00.292651' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 116 --seed 116  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_16_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 117, Buffer Size 500, Buffer Id 17, Seed 117, 2018-01-29 20:34:00.292674';
echo '=================> Gep-Ddpg Withparamnoise: Trial 117, Buffer Size 500, Buffer Id 17, Seed 117, 2018-01-29 20:34:00.292674' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 117 --seed 117  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_17_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 118, Buffer Size 500, Buffer Id 18, Seed 118, 2018-01-29 20:34:00.292692';
echo '=================> Gep-Ddpg Withparamnoise: Trial 118, Buffer Size 500, Buffer Id 18, Seed 118, 2018-01-29 20:34:00.292692' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 118 --seed 118  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_18_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 119, Buffer Size 500, Buffer Id 19, Seed 119, 2018-01-29 20:34:00.292709';
echo '=================> Gep-Ddpg Withparamnoise: Trial 119, Buffer Size 500, Buffer Id 19, Seed 119, 2018-01-29 20:34:00.292709' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 119 --seed 119  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_19_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 120, Buffer Size 500, Buffer Id 20, Seed 120, 2018-01-29 20:34:00.292729';
echo '=================> Gep-Ddpg Withparamnoise: Trial 120, Buffer Size 500, Buffer Id 20, Seed 120, 2018-01-29 20:34:00.292729' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 120 --seed 120  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_20_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait