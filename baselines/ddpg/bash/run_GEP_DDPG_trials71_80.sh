#!/bin/sh
#SBATCH -n1
#SBATCH -p long_sirocco
#SBATCH -t 60:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_GEP_DDPG_trials71_80.sh.err
#SBATCH -o run_GEP_DDPG_trials71_80.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withnoise: Trial 71, Buffer Size 500, Buffer Id 11, Seed 71, 2018-01-30 10:08:19.307899';
echo '=================> Gep-Ddpg Withnoise: Trial 71, Buffer Size 500, Buffer Id 11, Seed 71, 2018-01-30 10:08:19.307899' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 71 --seed 71 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_11_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 72, Buffer Size 500, Buffer Id 12, Seed 72, 2018-01-30 10:08:19.307938';
echo '=================> Gep-Ddpg Withnoise: Trial 72, Buffer Size 500, Buffer Id 12, Seed 72, 2018-01-30 10:08:19.307938' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 72 --seed 72 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_12_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 73, Buffer Size 500, Buffer Id 13, Seed 73, 2018-01-30 10:08:19.307956';
echo '=================> Gep-Ddpg Withnoise: Trial 73, Buffer Size 500, Buffer Id 13, Seed 73, 2018-01-30 10:08:19.307956' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 73 --seed 73 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_13_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 74, Buffer Size 500, Buffer Id 14, Seed 74, 2018-01-30 10:08:19.307971';
echo '=================> Gep-Ddpg Withnoise: Trial 74, Buffer Size 500, Buffer Id 14, Seed 74, 2018-01-30 10:08:19.307971' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 74 --seed 74 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_14_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 75, Buffer Size 500, Buffer Id 15, Seed 75, 2018-01-30 10:08:19.307986';
echo '=================> Gep-Ddpg Withnoise: Trial 75, Buffer Size 500, Buffer Id 15, Seed 75, 2018-01-30 10:08:19.307986' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 75 --seed 75 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_15_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 76, Buffer Size 500, Buffer Id 16, Seed 76, 2018-01-30 10:08:19.308000';
echo '=================> Gep-Ddpg Withnoise: Trial 76, Buffer Size 500, Buffer Id 16, Seed 76, 2018-01-30 10:08:19.308000' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 76 --seed 76 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_16_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 77, Buffer Size 500, Buffer Id 17, Seed 77, 2018-01-30 10:08:19.308015';
echo '=================> Gep-Ddpg Withnoise: Trial 77, Buffer Size 500, Buffer Id 17, Seed 77, 2018-01-30 10:08:19.308015' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 77 --seed 77 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_17_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 78, Buffer Size 500, Buffer Id 18, Seed 78, 2018-01-30 10:08:19.308031';
echo '=================> Gep-Ddpg Withnoise: Trial 78, Buffer Size 500, Buffer Id 18, Seed 78, 2018-01-30 10:08:19.308031' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 78 --seed 78 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_18_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 79, Buffer Size 500, Buffer Id 19, Seed 79, 2018-01-30 10:08:19.308046';
echo '=================> Gep-Ddpg Withnoise: Trial 79, Buffer Size 500, Buffer Id 19, Seed 79, 2018-01-30 10:08:19.308046' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 79 --seed 79 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_19_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 80, Buffer Size 500, Buffer Id 20, Seed 80, 2018-01-30 10:08:19.308062';
echo '=================> Gep-Ddpg Withnoise: Trial 80, Buffer Size 500, Buffer Id 20, Seed 80, 2018-01-30 10:08:19.308062' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 80 --seed 80 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_20_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait