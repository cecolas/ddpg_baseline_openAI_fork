#!/usr/bin/python
# -*- coding: utf-8 -*-

from itertools import *
import datetime

# PATH_TO_RESULTS = '/mnt/shared/cedric/DDPG/'
# PATH_TO_INTERPRETER = "/usr/bin/python3"
PATH_TO_RESULTS = '/projets/flowers/cedric/ddpg_baseline_openAI_fork/results/'
PATH_TO_INTERPRETER = "/home/ccolas/virtual_envs/py3.5/bin/python"

trial_id = list(range(201,211))
frozen = False
study = 'DDPG'
ddpg_noise = 'withParamNoise'

filename = 'run_DDPG_trials' + str(trial_id[0])+'_'+str(trial_id[-1])+'.sh'
with open(filename, 'w') as f:
    f.write('#!/bin/sh\n')
    f.write('#SBATCH -n1\n')
    f.write('#SBATCH -p long_sirocco\n')
    f.write('#SBATCH -t 60:00:00\n')
    f.write('#SBATCH --gres=gpu:1\n')
    f.write('#SBATCH -e ' + filename + '.err\n')
    f.write('#SBATCH -o ' + filename + '.out\n')
    f.write('rm log.txt; \n')
    f.write("export EXP_INTERP='%s' ;\n" % PATH_TO_INTERPRETER)
    f.write('ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"\n')
    f.write('agpu=0\n')

    for seed in range(len(trial_id)):
        t_id = trial_id[seed]
        name = ("DDPG %s: trial %s, seed %s, %s" % (ddpg_noise,str(t_id),str(t_id), str(datetime.datetime.now()))).title()
        f.write("echo '=================> %s';\n" % name)
        f.write("echo '=================> %s' >> log.txt;\n" % name)
        f.write("export CUDA_VISIBLE_DEVICES=$agpu\n")
        f.write("agpu=$(((agpu+1)%ngpu))\n")
        if ddpg_noise == 'withNoise':
            f.write("$EXP_INTERP main_cmc.py --trial_id %i --study %s --noise_type %s --seed %i --data_path %s & \n"
                    % (t_id, study, 'ou_0.3',t_id, PATH_TO_RESULTS))
        elif ddpg_noise == 'withParamNoise':
            f.write("$EXP_INTERP main_cmc.py --trial_id %i --study %s --noise_type %s --seed %i --data_path %s & \n"
                    % (t_id, study, 'adaptive-param_0.2', t_id, PATH_TO_RESULTS))
        elif ddpg_noise == 'withDecreasingNoise':
            f.write("$EXP_INTERP main_cmc.py --trial_id %i --study %s --noise_type %s --seed %i --data_path %s & \n"
                    % (t_id, study, 'decreasing-ou_0.6', t_id, PATH_TO_RESULTS))
        else:
            raise NotImplementedError
    f.write('wait')
