#!/usr/bin/python
# -*- coding: utf-8 -*-

from itertools import *
import datetime

# PATH_TO_RESULTS = '/mnt/shared/cedric/DDPG/'
# PATH_TO_INTERPRETER = "/usr/bin/python3"
PATH_TO_RESULTS = '/projets/flowers/cedric/ddpg_baseline_openAI_fork/results/'
PATH_TO_INTERPRETER = "/home/ccolas/virtual_envs/py3.5/bin/python"

trial_id = list(range(161,181))
study = 'GEP_FDDPG'
size_buff = 750
buffer_folder = '/projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/'

filename = 'run_GEP_FDDPG_trials' + str(trial_id[0])+'_'+str(trial_id[-1])+'.sh'
with open(filename, 'w') as f:
    f.write('#!/bin/sh\n')
    f.write('#SBATCH -n1\n')
    f.write('#SBATCH -p long_sirocco\n')
    f.write('#SBATCH -t 30:00:00\n')
    f.write('#SBATCH --gres=gpu:1\n')
    f.write('#SBATCH -e ' + filename + '.err\n')
    f.write('#SBATCH -o ' + filename + '.out\n')
    f.write('rm log.txt; \n')
    f.write("export EXP_INTERP='%s' ;\n" % PATH_TO_INTERPRETER)
    f.write('ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"\n')
    f.write('agpu=0\n')
    for i,buff_id in enumerate(list(range(1,21))):
        buffer_name = 'buffer_%s_%sk_%s' %(str(int(size_buff/10)), str(size_buff), str(buff_id))
        print('buffer_name :',buffer_name)
        t_id = trial_id[i]
        name = ("GEP-FDDPG: trial %s, buffer size %s, buffer id %s, seed %s, %s" % (str(t_id),str(size_buff),str(buff_id), str(t_id), str(datetime.datetime.now()))).title()
        f.write("echo '=================> %s';\n" % name)
        f.write("echo '=================> %s' >> log.txt;\n" % name)
        f.write("export CUDA_VISIBLE_DEVICES=$agpu\n")
        f.write("agpu=$(((agpu+1)%ngpu))\n")
        f.write("$EXP_INTERP main.py --trial_id %i --seed %i --study %s --buffer_location %s --data_path %s & \n"
                % (t_id, t_id, study,  buffer_folder+buffer_name, PATH_TO_RESULTS))
    f.write('wait')
