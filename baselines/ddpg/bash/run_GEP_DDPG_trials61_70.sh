#!/bin/sh
#SBATCH -n1
#SBATCH -p long_sirocco
#SBATCH -t 60:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_GEP_DDPG_trials61_70.sh.err
#SBATCH -o run_GEP_DDPG_trials61_70.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withnoise: Trial 61, Buffer Size 500, Buffer Id 1, Seed 61, 2018-01-30 10:08:07.735233';
echo '=================> Gep-Ddpg Withnoise: Trial 61, Buffer Size 500, Buffer Id 1, Seed 61, 2018-01-30 10:08:07.735233' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 61 --seed 61 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_1_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 62, Buffer Size 500, Buffer Id 2, Seed 62, 2018-01-30 10:08:07.735273';
echo '=================> Gep-Ddpg Withnoise: Trial 62, Buffer Size 500, Buffer Id 2, Seed 62, 2018-01-30 10:08:07.735273' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 62 --seed 62 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_2_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 63, Buffer Size 500, Buffer Id 3, Seed 63, 2018-01-30 10:08:07.735285';
echo '=================> Gep-Ddpg Withnoise: Trial 63, Buffer Size 500, Buffer Id 3, Seed 63, 2018-01-30 10:08:07.735285' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 63 --seed 63 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_3_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 64, Buffer Size 500, Buffer Id 4, Seed 64, 2018-01-30 10:08:07.735295';
echo '=================> Gep-Ddpg Withnoise: Trial 64, Buffer Size 500, Buffer Id 4, Seed 64, 2018-01-30 10:08:07.735295' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 64 --seed 64 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_4_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 65, Buffer Size 500, Buffer Id 5, Seed 65, 2018-01-30 10:08:07.735304';
echo '=================> Gep-Ddpg Withnoise: Trial 65, Buffer Size 500, Buffer Id 5, Seed 65, 2018-01-30 10:08:07.735304' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 65 --seed 65 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_5_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 66, Buffer Size 500, Buffer Id 6, Seed 66, 2018-01-30 10:08:07.735313';
echo '=================> Gep-Ddpg Withnoise: Trial 66, Buffer Size 500, Buffer Id 6, Seed 66, 2018-01-30 10:08:07.735313' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 66 --seed 66 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_6_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 67, Buffer Size 500, Buffer Id 7, Seed 67, 2018-01-30 10:08:07.735322';
echo '=================> Gep-Ddpg Withnoise: Trial 67, Buffer Size 500, Buffer Id 7, Seed 67, 2018-01-30 10:08:07.735322' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 67 --seed 67 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_7_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 68, Buffer Size 500, Buffer Id 8, Seed 68, 2018-01-30 10:08:07.735334';
echo '=================> Gep-Ddpg Withnoise: Trial 68, Buffer Size 500, Buffer Id 8, Seed 68, 2018-01-30 10:08:07.735334' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 68 --seed 68 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_8_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 69, Buffer Size 500, Buffer Id 9, Seed 69, 2018-01-30 10:08:07.735343';
echo '=================> Gep-Ddpg Withnoise: Trial 69, Buffer Size 500, Buffer Id 9, Seed 69, 2018-01-30 10:08:07.735343' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 69 --seed 69 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_9_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 70, Buffer Size 500, Buffer Id 10, Seed 70, 2018-01-30 10:08:07.735352';
echo '=================> Gep-Ddpg Withnoise: Trial 70, Buffer Size 500, Buffer Id 10, Seed 70, 2018-01-30 10:08:07.735352' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 70 --seed 70 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_10_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait