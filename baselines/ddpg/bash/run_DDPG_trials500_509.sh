#!/bin/sh
#SBATCH -n10
#SBATCH -p long_sirocco
#SBATCH -t 25:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_DDPG_trials500_509.sh.err
#SBATCH -o run_DDPG_trials500_509.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Ddpg Withnoise: Trial 500, Seed 500, 2018-02-06 11:43:07.525111';
echo '=================> Ddpg Withnoise: Trial 500, Seed 500, 2018-02-06 11:43:07.525111' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc_histo.py --trial_id 500 --study DDPG --noise_type ou_0.3 --seed 500 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 501, Seed 501, 2018-02-06 11:43:07.525524';
echo '=================> Ddpg Withnoise: Trial 501, Seed 501, 2018-02-06 11:43:07.525524' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc_histo.py --trial_id 501 --study DDPG --noise_type ou_0.3 --seed 501 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 502, Seed 502, 2018-02-06 11:43:07.525906';
echo '=================> Ddpg Withnoise: Trial 502, Seed 502, 2018-02-06 11:43:07.525906' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc_histo.py --trial_id 502 --study DDPG --noise_type ou_0.3 --seed 502 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 503, Seed 503, 2018-02-06 11:43:07.526259';
echo '=================> Ddpg Withnoise: Trial 503, Seed 503, 2018-02-06 11:43:07.526259' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc_histo.py --trial_id 503 --study DDPG --noise_type ou_0.3 --seed 503 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 504, Seed 504, 2018-02-06 11:43:07.526449';
echo '=================> Ddpg Withnoise: Trial 504, Seed 504, 2018-02-06 11:43:07.526449' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc_histo.py --trial_id 504 --study DDPG --noise_type ou_0.3 --seed 504 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 505, Seed 505, 2018-02-06 11:43:07.526747';
echo '=================> Ddpg Withnoise: Trial 505, Seed 505, 2018-02-06 11:43:07.526747' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc_histo.py --trial_id 505 --study DDPG --noise_type ou_0.3 --seed 505 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 506, Seed 506, 2018-02-06 11:43:07.526954';
echo '=================> Ddpg Withnoise: Trial 506, Seed 506, 2018-02-06 11:43:07.526954' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc_histo.py --trial_id 506 --study DDPG --noise_type ou_0.3 --seed 506 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 507, Seed 507, 2018-02-06 11:43:07.527506';
echo '=================> Ddpg Withnoise: Trial 507, Seed 507, 2018-02-06 11:43:07.527506' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc_histo.py --trial_id 507 --study DDPG --noise_type ou_0.3 --seed 507 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 508, Seed 508, 2018-02-06 11:43:07.536052';
echo '=================> Ddpg Withnoise: Trial 508, Seed 508, 2018-02-06 11:43:07.536052' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc_histo.py --trial_id 508 --study DDPG --noise_type ou_0.3 --seed 508 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 509, Seed 509, 2018-02-06 11:43:07.536092';
echo '=================> Ddpg Withnoise: Trial 509, Seed 509, 2018-02-06 11:43:07.536092' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc_histo.py --trial_id 509 --study DDPG --noise_type ou_0.3 --seed 509 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait