#!/bin/sh
#SBATCH -n1
#SBATCH -p longq
#SBATCH -t 30:00:00
#SBATCH -e run_DDPG_trials1_20.sh.err
#SBATCH -o run_DDPG_trials1_20.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
#ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
#agpu=0
echo '=================> Ddpg Withnoise: Trial 1, Seed 1, 2018-01-25 16:49:16.692449';
echo '=================> Ddpg Withnoise: Trial 1, Seed 1, 2018-01-25 16:49:16.692449' >> log.txt;
#export CUDA_VISIBLE_DEVICES=$agpu
#agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 1 --study DDPG --noise_type ou_0.3 --seed 1 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 2, Seed 2, 2018-01-25 16:49:16.692544';
echo '=================> Ddpg Withnoise: Trial 2, Seed 2, 2018-01-25 16:49:16.692544' >> log.txt;
#export CUDA_VISIBLE_DEVICES=$agpu
#agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 2 --study DDPG --noise_type ou_0.3 --seed 2 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 3, Seed 3, 2018-01-25 16:49:16.692590';
echo '=================> Ddpg Withnoise: Trial 3, Seed 3, 2018-01-25 16:49:16.692590' >> log.txt;
#export CUDA_VISIBLE_DEVICES=$agpu
#agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 3 --study DDPG --noise_type ou_0.3 --seed 3 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 4, Seed 4, 2018-01-25 16:49:16.692633';
echo '=================> Ddpg Withnoise: Trial 4, Seed 4, 2018-01-25 16:49:16.692633' >> log.txt;
#export CUDA_VISIBLE_DEVICES=$agpu
#agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 4 --study DDPG --noise_type ou_0.3 --seed 4 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 5, Seed 5, 2018-01-25 16:49:16.692673';
echo '=================> Ddpg Withnoise: Trial 5, Seed 5, 2018-01-25 16:49:16.692673' >> log.txt;
#export CUDA_VISIBLE_DEVICES=$agpu
#agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 5 --study DDPG --noise_type ou_0.3 --seed 5 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 6, Seed 6, 2018-01-25 16:49:16.692714';
echo '=================> Ddpg Withnoise: Trial 6, Seed 6, 2018-01-25 16:49:16.692714' >> log.txt;
#export CUDA_VISIBLE_DEVICES=$agpu
#agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 6 --study DDPG --noise_type ou_0.3 --seed 6 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 7, Seed 7, 2018-01-25 16:49:16.692753';
echo '=================> Ddpg Withnoise: Trial 7, Seed 7, 2018-01-25 16:49:16.692753' >> log.txt;
#export CUDA_VISIBLE_DEVICES=$agpu
#agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 7 --study DDPG --noise_type ou_0.3 --seed 7 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 8, Seed 8, 2018-01-25 16:49:16.692794';
echo '=================> Ddpg Withnoise: Trial 8, Seed 8, 2018-01-25 16:49:16.692794' >> log.txt;
#export CUDA_VISIBLE_DEVICES=$agpu
#agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 8 --study DDPG --noise_type ou_0.3 --seed 8 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 9, Seed 9, 2018-01-25 16:49:16.692838';
echo '=================> Ddpg Withnoise: Trial 9, Seed 9, 2018-01-25 16:49:16.692838' >> log.txt;
#export CUDA_VISIBLE_DEVICES=$agpu
#agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 9 --study DDPG --noise_type ou_0.3 --seed 9 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 10, Seed 10, 2018-01-25 16:49:16.692879';
echo '=================> Ddpg Withnoise: Trial 10, Seed 10, 2018-01-25 16:49:16.692879' >> log.txt;
#export CUDA_VISIBLE_DEVICES=$agpu
#agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 10 --study DDPG --noise_type ou_0.3 --seed 10 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 11, Seed 11, 2018-01-25 16:49:16.692923';
echo '=================> Ddpg Withnoise: Trial 11, Seed 11, 2018-01-25 16:49:16.692923' >> log.txt;
#export CUDA_VISIBLE_DEVICES=$agpu
#agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 11 --study DDPG --noise_type ou_0.3 --seed 11 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 12, Seed 12, 2018-01-25 16:49:16.692964';
echo '=================> Ddpg Withnoise: Trial 12, Seed 12, 2018-01-25 16:49:16.692964' >> log.txt;
#export CUDA_VISIBLE_DEVICES=$agpu
#agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 12 --study DDPG --noise_type ou_0.3 --seed 12 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 13, Seed 13, 2018-01-25 16:49:16.693002';
echo '=================> Ddpg Withnoise: Trial 13, Seed 13, 2018-01-25 16:49:16.693002' >> log.txt;
#export CUDA_VISIBLE_DEVICES=$agpu
#agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 13 --study DDPG --noise_type ou_0.3 --seed 13 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 14, Seed 14, 2018-01-25 16:49:16.693043';
echo '=================> Ddpg Withnoise: Trial 14, Seed 14, 2018-01-25 16:49:16.693043' >> log.txt;
#export CUDA_VISIBLE_DEVICES=$agpu
#agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 14 --study DDPG --noise_type ou_0.3 --seed 14 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 15, Seed 15, 2018-01-25 16:49:16.693080';
echo '=================> Ddpg Withnoise: Trial 15, Seed 15, 2018-01-25 16:49:16.693080' >> log.txt;
#export CUDA_VISIBLE_DEVICES=$agpu
#agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 15 --study DDPG --noise_type ou_0.3 --seed 15 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 16, Seed 16, 2018-01-25 16:49:16.693119';
echo '=================> Ddpg Withnoise: Trial 16, Seed 16, 2018-01-25 16:49:16.693119' >> log.txt;
#export CUDA_VISIBLE_DEVICES=$agpu
#agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 16 --study DDPG --noise_type ou_0.3 --seed 16 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 17, Seed 17, 2018-01-25 16:49:16.693159';
echo '=================> Ddpg Withnoise: Trial 17, Seed 17, 2018-01-25 16:49:16.693159' >> log.txt;
#export CUDA_VISIBLE_DEVICES=$agpu
#agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 17 --study DDPG --noise_type ou_0.3 --seed 17 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 18, Seed 18, 2018-01-25 16:49:16.693201';
echo '=================> Ddpg Withnoise: Trial 18, Seed 18, 2018-01-25 16:49:16.693201' >> log.txt;
#export CUDA_VISIBLE_DEVICES=$agpu
#agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 18 --study DDPG --noise_type ou_0.3 --seed 18 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 19, Seed 19, 2018-01-25 16:49:16.693242';
echo '=================> Ddpg Withnoise: Trial 19, Seed 19, 2018-01-25 16:49:16.693242' >> log.txt;
#export CUDA_VISIBLE_DEVICES=$agpu
#agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 19 --study DDPG --noise_type ou_0.3 --seed 19 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 20, Seed 20, 2018-01-25 16:49:16.693282';
echo '=================> Ddpg Withnoise: Trial 20, Seed 20, 2018-01-25 16:49:16.693282' >> log.txt;
#export CUDA_VISIBLE_DEVICES=$agpu
#agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 20 --study DDPG --noise_type ou_0.3 --seed 20 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait