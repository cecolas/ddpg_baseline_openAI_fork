#!/bin/sh
#SBATCH -n20
#SBATCH -p long_sirocco
#SBATCH -t 30:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_DDPG_trials341_360.sh.err
#SBATCH -o run_DDPG_trials341_360.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Ddpg Withdecreasingnoise: Trial 341, Seed 341, 2018-02-07 10:56:40.876161';
echo '=================> Ddpg Withdecreasingnoise: Trial 341, Seed 341, 2018-02-07 10:56:40.876161' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 341 --study DDPG --noise_type decreasing-ou_0.6 --nb-epochs 250 --seed 341 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 342, Seed 342, 2018-02-07 10:56:40.876205';
echo '=================> Ddpg Withdecreasingnoise: Trial 342, Seed 342, 2018-02-07 10:56:40.876205' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 342 --study DDPG --noise_type decreasing-ou_0.6 --nb-epochs 250 --seed 342 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 343, Seed 343, 2018-02-07 10:56:40.876221';
echo '=================> Ddpg Withdecreasingnoise: Trial 343, Seed 343, 2018-02-07 10:56:40.876221' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 343 --study DDPG --noise_type decreasing-ou_0.6 --nb-epochs 250 --seed 343 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 344, Seed 344, 2018-02-07 10:56:40.876238';
echo '=================> Ddpg Withdecreasingnoise: Trial 344, Seed 344, 2018-02-07 10:56:40.876238' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 344 --study DDPG --noise_type decreasing-ou_0.6 --nb-epochs 250 --seed 344 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 345, Seed 345, 2018-02-07 10:56:40.876250';
echo '=================> Ddpg Withdecreasingnoise: Trial 345, Seed 345, 2018-02-07 10:56:40.876250' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 345 --study DDPG --noise_type decreasing-ou_0.6 --nb-epochs 250 --seed 345 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 346, Seed 346, 2018-02-07 10:56:40.876263';
echo '=================> Ddpg Withdecreasingnoise: Trial 346, Seed 346, 2018-02-07 10:56:40.876263' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 346 --study DDPG --noise_type decreasing-ou_0.6 --nb-epochs 250 --seed 346 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 347, Seed 347, 2018-02-07 10:56:40.876275';
echo '=================> Ddpg Withdecreasingnoise: Trial 347, Seed 347, 2018-02-07 10:56:40.876275' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 347 --study DDPG --noise_type decreasing-ou_0.6 --nb-epochs 250 --seed 347 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 348, Seed 348, 2018-02-07 10:56:40.876287';
echo '=================> Ddpg Withdecreasingnoise: Trial 348, Seed 348, 2018-02-07 10:56:40.876287' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 348 --study DDPG --noise_type decreasing-ou_0.6 --nb-epochs 250 --seed 348 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 349, Seed 349, 2018-02-07 10:56:40.876300';
echo '=================> Ddpg Withdecreasingnoise: Trial 349, Seed 349, 2018-02-07 10:56:40.876300' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 349 --study DDPG --noise_type decreasing-ou_0.6 --nb-epochs 250 --seed 349 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 350, Seed 350, 2018-02-07 10:56:40.876311';
echo '=================> Ddpg Withdecreasingnoise: Trial 350, Seed 350, 2018-02-07 10:56:40.876311' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 350 --study DDPG --noise_type decreasing-ou_0.6 --nb-epochs 250 --seed 350 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 351, Seed 351, 2018-02-07 10:56:40.876324';
echo '=================> Ddpg Withdecreasingnoise: Trial 351, Seed 351, 2018-02-07 10:56:40.876324' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 351 --study DDPG --noise_type decreasing-ou_0.6 --nb-epochs 250 --seed 351 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 352, Seed 352, 2018-02-07 10:56:40.876339';
echo '=================> Ddpg Withdecreasingnoise: Trial 352, Seed 352, 2018-02-07 10:56:40.876339' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 352 --study DDPG --noise_type decreasing-ou_0.6 --nb-epochs 250 --seed 352 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 353, Seed 353, 2018-02-07 10:56:40.876351';
echo '=================> Ddpg Withdecreasingnoise: Trial 353, Seed 353, 2018-02-07 10:56:40.876351' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 353 --study DDPG --noise_type decreasing-ou_0.6 --nb-epochs 250 --seed 353 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 354, Seed 354, 2018-02-07 10:56:40.876362';
echo '=================> Ddpg Withdecreasingnoise: Trial 354, Seed 354, 2018-02-07 10:56:40.876362' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 354 --study DDPG --noise_type decreasing-ou_0.6 --nb-epochs 250 --seed 354 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 355, Seed 355, 2018-02-07 10:56:40.876373';
echo '=================> Ddpg Withdecreasingnoise: Trial 355, Seed 355, 2018-02-07 10:56:40.876373' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 355 --study DDPG --noise_type decreasing-ou_0.6 --nb-epochs 250 --seed 355 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 356, Seed 356, 2018-02-07 10:56:40.876383';
echo '=================> Ddpg Withdecreasingnoise: Trial 356, Seed 356, 2018-02-07 10:56:40.876383' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 356 --study DDPG --noise_type decreasing-ou_0.6 --nb-epochs 250 --seed 356 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 357, Seed 357, 2018-02-07 10:56:40.876394';
echo '=================> Ddpg Withdecreasingnoise: Trial 357, Seed 357, 2018-02-07 10:56:40.876394' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 357 --study DDPG --noise_type decreasing-ou_0.6 --nb-epochs 250 --seed 357 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 358, Seed 358, 2018-02-07 10:56:40.876405';
echo '=================> Ddpg Withdecreasingnoise: Trial 358, Seed 358, 2018-02-07 10:56:40.876405' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 358 --study DDPG --noise_type decreasing-ou_0.6 --nb-epochs 250 --seed 358 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 359, Seed 359, 2018-02-07 10:56:40.876466';
echo '=================> Ddpg Withdecreasingnoise: Trial 359, Seed 359, 2018-02-07 10:56:40.876466' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 359 --study DDPG --noise_type decreasing-ou_0.6 --nb-epochs 250 --seed 359 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withdecreasingnoise: Trial 360, Seed 360, 2018-02-07 10:56:40.876481';
echo '=================> Ddpg Withdecreasingnoise: Trial 360, Seed 360, 2018-02-07 10:56:40.876481' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 360 --study DDPG --noise_type decreasing-ou_0.6 --nb-epochs 250 --seed 360 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait