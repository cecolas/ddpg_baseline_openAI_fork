#!/bin/sh
#SBATCH -n20
#SBATCH -p long_sirocco
#SBATCH -t 30:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_DDPG_trials321_340.sh.err
#SBATCH -o run_DDPG_trials321_340.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Ddpg Withnoise: Trial 321, Seed 321, 2018-02-07 10:56:23.297124';
echo '=================> Ddpg Withnoise: Trial 321, Seed 321, 2018-02-07 10:56:23.297124' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 321 --study DDPG --noise_type ou_0.3 --nb-epochs 250 --seed 321 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 322, Seed 322, 2018-02-07 10:56:23.297171';
echo '=================> Ddpg Withnoise: Trial 322, Seed 322, 2018-02-07 10:56:23.297171' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 322 --study DDPG --noise_type ou_0.3 --nb-epochs 250 --seed 322 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 323, Seed 323, 2018-02-07 10:56:23.297187';
echo '=================> Ddpg Withnoise: Trial 323, Seed 323, 2018-02-07 10:56:23.297187' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 323 --study DDPG --noise_type ou_0.3 --nb-epochs 250 --seed 323 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 324, Seed 324, 2018-02-07 10:56:23.297200';
echo '=================> Ddpg Withnoise: Trial 324, Seed 324, 2018-02-07 10:56:23.297200' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 324 --study DDPG --noise_type ou_0.3 --nb-epochs 250 --seed 324 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 325, Seed 325, 2018-02-07 10:56:23.297211';
echo '=================> Ddpg Withnoise: Trial 325, Seed 325, 2018-02-07 10:56:23.297211' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 325 --study DDPG --noise_type ou_0.3 --nb-epochs 250 --seed 325 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 326, Seed 326, 2018-02-07 10:56:23.297222';
echo '=================> Ddpg Withnoise: Trial 326, Seed 326, 2018-02-07 10:56:23.297222' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 326 --study DDPG --noise_type ou_0.3 --nb-epochs 250 --seed 326 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 327, Seed 327, 2018-02-07 10:56:23.297232';
echo '=================> Ddpg Withnoise: Trial 327, Seed 327, 2018-02-07 10:56:23.297232' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 327 --study DDPG --noise_type ou_0.3 --nb-epochs 250 --seed 327 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 328, Seed 328, 2018-02-07 10:56:23.297244';
echo '=================> Ddpg Withnoise: Trial 328, Seed 328, 2018-02-07 10:56:23.297244' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 328 --study DDPG --noise_type ou_0.3 --nb-epochs 250 --seed 328 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 329, Seed 329, 2018-02-07 10:56:23.297255';
echo '=================> Ddpg Withnoise: Trial 329, Seed 329, 2018-02-07 10:56:23.297255' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 329 --study DDPG --noise_type ou_0.3 --nb-epochs 250 --seed 329 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 330, Seed 330, 2018-02-07 10:56:23.297379';
echo '=================> Ddpg Withnoise: Trial 330, Seed 330, 2018-02-07 10:56:23.297379' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 330 --study DDPG --noise_type ou_0.3 --nb-epochs 250 --seed 330 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 331, Seed 331, 2018-02-07 10:56:23.297401';
echo '=================> Ddpg Withnoise: Trial 331, Seed 331, 2018-02-07 10:56:23.297401' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 331 --study DDPG --noise_type ou_0.3 --nb-epochs 250 --seed 331 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 332, Seed 332, 2018-02-07 10:56:23.297413';
echo '=================> Ddpg Withnoise: Trial 332, Seed 332, 2018-02-07 10:56:23.297413' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 332 --study DDPG --noise_type ou_0.3 --nb-epochs 250 --seed 332 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 333, Seed 333, 2018-02-07 10:56:23.297425';
echo '=================> Ddpg Withnoise: Trial 333, Seed 333, 2018-02-07 10:56:23.297425' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 333 --study DDPG --noise_type ou_0.3 --nb-epochs 250 --seed 333 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 334, Seed 334, 2018-02-07 10:56:23.297527';
echo '=================> Ddpg Withnoise: Trial 334, Seed 334, 2018-02-07 10:56:23.297527' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 334 --study DDPG --noise_type ou_0.3 --nb-epochs 250 --seed 334 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 335, Seed 335, 2018-02-07 10:56:23.297543';
echo '=================> Ddpg Withnoise: Trial 335, Seed 335, 2018-02-07 10:56:23.297543' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 335 --study DDPG --noise_type ou_0.3 --nb-epochs 250 --seed 335 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 336, Seed 336, 2018-02-07 10:56:23.297556';
echo '=================> Ddpg Withnoise: Trial 336, Seed 336, 2018-02-07 10:56:23.297556' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 336 --study DDPG --noise_type ou_0.3 --nb-epochs 250 --seed 336 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 337, Seed 337, 2018-02-07 10:56:23.297569';
echo '=================> Ddpg Withnoise: Trial 337, Seed 337, 2018-02-07 10:56:23.297569' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 337 --study DDPG --noise_type ou_0.3 --nb-epochs 250 --seed 337 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 338, Seed 338, 2018-02-07 10:56:23.297581';
echo '=================> Ddpg Withnoise: Trial 338, Seed 338, 2018-02-07 10:56:23.297581' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 338 --study DDPG --noise_type ou_0.3 --nb-epochs 250 --seed 338 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 339, Seed 339, 2018-02-07 10:56:23.297595';
echo '=================> Ddpg Withnoise: Trial 339, Seed 339, 2018-02-07 10:56:23.297595' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 339 --study DDPG --noise_type ou_0.3 --nb-epochs 250 --seed 339 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 340, Seed 340, 2018-02-07 10:56:23.297661';
echo '=================> Ddpg Withnoise: Trial 340, Seed 340, 2018-02-07 10:56:23.297661' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 340 --study DDPG --noise_type ou_0.3 --nb-epochs 250 --seed 340 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait