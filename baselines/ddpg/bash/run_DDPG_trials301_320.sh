#!/bin/sh
#SBATCH -n20
#SBATCH -p long_sirocco
#SBATCH -t 30:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_DDPG_trials301_320.sh.err
#SBATCH -o run_DDPG_trials301_320.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Ddpg Withparamnoise: Trial 301, Seed 301, 2018-02-07 10:56:12.189651';
echo '=================> Ddpg Withparamnoise: Trial 301, Seed 301, 2018-02-07 10:56:12.189651' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 301 --study DDPG --noise_type adaptive-param_0.2 --nb-epochs 250 --seed 301 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 302, Seed 302, 2018-02-07 10:56:12.189833';
echo '=================> Ddpg Withparamnoise: Trial 302, Seed 302, 2018-02-07 10:56:12.189833' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 302 --study DDPG --noise_type adaptive-param_0.2 --nb-epochs 250 --seed 302 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 303, Seed 303, 2018-02-07 10:56:12.189859';
echo '=================> Ddpg Withparamnoise: Trial 303, Seed 303, 2018-02-07 10:56:12.189859' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 303 --study DDPG --noise_type adaptive-param_0.2 --nb-epochs 250 --seed 303 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 304, Seed 304, 2018-02-07 10:56:12.189877';
echo '=================> Ddpg Withparamnoise: Trial 304, Seed 304, 2018-02-07 10:56:12.189877' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 304 --study DDPG --noise_type adaptive-param_0.2 --nb-epochs 250 --seed 304 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 305, Seed 305, 2018-02-07 10:56:12.189890';
echo '=================> Ddpg Withparamnoise: Trial 305, Seed 305, 2018-02-07 10:56:12.189890' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 305 --study DDPG --noise_type adaptive-param_0.2 --nb-epochs 250 --seed 305 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 306, Seed 306, 2018-02-07 10:56:12.189902';
echo '=================> Ddpg Withparamnoise: Trial 306, Seed 306, 2018-02-07 10:56:12.189902' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 306 --study DDPG --noise_type adaptive-param_0.2 --nb-epochs 250 --seed 306 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 307, Seed 307, 2018-02-07 10:56:12.189914';
echo '=================> Ddpg Withparamnoise: Trial 307, Seed 307, 2018-02-07 10:56:12.189914' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 307 --study DDPG --noise_type adaptive-param_0.2 --nb-epochs 250 --seed 307 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 308, Seed 308, 2018-02-07 10:56:12.189926';
echo '=================> Ddpg Withparamnoise: Trial 308, Seed 308, 2018-02-07 10:56:12.189926' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 308 --study DDPG --noise_type adaptive-param_0.2 --nb-epochs 250 --seed 308 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 309, Seed 309, 2018-02-07 10:56:12.189937';
echo '=================> Ddpg Withparamnoise: Trial 309, Seed 309, 2018-02-07 10:56:12.189937' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 309 --study DDPG --noise_type adaptive-param_0.2 --nb-epochs 250 --seed 309 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 310, Seed 310, 2018-02-07 10:56:12.189949';
echo '=================> Ddpg Withparamnoise: Trial 310, Seed 310, 2018-02-07 10:56:12.189949' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 310 --study DDPG --noise_type adaptive-param_0.2 --nb-epochs 250 --seed 310 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 311, Seed 311, 2018-02-07 10:56:12.189962';
echo '=================> Ddpg Withparamnoise: Trial 311, Seed 311, 2018-02-07 10:56:12.189962' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 311 --study DDPG --noise_type adaptive-param_0.2 --nb-epochs 250 --seed 311 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 312, Seed 312, 2018-02-07 10:56:12.189974';
echo '=================> Ddpg Withparamnoise: Trial 312, Seed 312, 2018-02-07 10:56:12.189974' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 312 --study DDPG --noise_type adaptive-param_0.2 --nb-epochs 250 --seed 312 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 313, Seed 313, 2018-02-07 10:56:12.189986';
echo '=================> Ddpg Withparamnoise: Trial 313, Seed 313, 2018-02-07 10:56:12.189986' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 313 --study DDPG --noise_type adaptive-param_0.2 --nb-epochs 250 --seed 313 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 314, Seed 314, 2018-02-07 10:56:12.190000';
echo '=================> Ddpg Withparamnoise: Trial 314, Seed 314, 2018-02-07 10:56:12.190000' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 314 --study DDPG --noise_type adaptive-param_0.2 --nb-epochs 250 --seed 314 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 315, Seed 315, 2018-02-07 10:56:12.190012';
echo '=================> Ddpg Withparamnoise: Trial 315, Seed 315, 2018-02-07 10:56:12.190012' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 315 --study DDPG --noise_type adaptive-param_0.2 --nb-epochs 250 --seed 315 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 316, Seed 316, 2018-02-07 10:56:12.190024';
echo '=================> Ddpg Withparamnoise: Trial 316, Seed 316, 2018-02-07 10:56:12.190024' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 316 --study DDPG --noise_type adaptive-param_0.2 --nb-epochs 250 --seed 316 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 317, Seed 317, 2018-02-07 10:56:12.190035';
echo '=================> Ddpg Withparamnoise: Trial 317, Seed 317, 2018-02-07 10:56:12.190035' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 317 --study DDPG --noise_type adaptive-param_0.2 --nb-epochs 250 --seed 317 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 318, Seed 318, 2018-02-07 10:56:12.190047';
echo '=================> Ddpg Withparamnoise: Trial 318, Seed 318, 2018-02-07 10:56:12.190047' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 318 --study DDPG --noise_type adaptive-param_0.2 --nb-epochs 250 --seed 318 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 319, Seed 319, 2018-02-07 10:56:12.190120';
echo '=================> Ddpg Withparamnoise: Trial 319, Seed 319, 2018-02-07 10:56:12.190120' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 319 --study DDPG --noise_type adaptive-param_0.2 --nb-epochs 250 --seed 319 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 320, Seed 320, 2018-02-07 10:56:12.190137';
echo '=================> Ddpg Withparamnoise: Trial 320, Seed 320, 2018-02-07 10:56:12.190137' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 320 --study DDPG --noise_type adaptive-param_0.2 --nb-epochs 250 --seed 320 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait