#!/bin/sh
#SBATCH -n20
#SBATCH -p long_sirocco
#SBATCH -t 30:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_GEP_FDDPG_trials641_641.sh.err
#SBATCH -o run_GEP_FDDPG_trials641_641.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep_Fddpg Withnoise: Trial 641, Buffer Size 900, Buffer Id 21, Seed 641, 2018-02-23 11:12:35.703007';
echo '=================> Gep_Fddpg Withnoise: Trial 641, Buffer Size 900, Buffer Id 21, Seed 641, 2018-02-23 11:12:35.703007' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 641 --seed 641 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_900_21 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait