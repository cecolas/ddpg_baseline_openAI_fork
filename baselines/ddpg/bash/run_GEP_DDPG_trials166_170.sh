#!/bin/sh
export EXP_INTERP='/usr/bin/python3' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withparamnoise: Trial 166, Buffer Size 500, Buffer Id 26, Seed 166, 2018-01-31 15:26:43.158350';
echo '=================> Gep-Ddpg Withparamnoise: Trial 166, Buffer Size 500, Buffer Id 26, Seed 166, 2018-01-31 15:26:43.158350' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 166 --seed 166  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_26_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withparamnoise: Trial 167, Buffer Size 500, Buffer Id 27, Seed 167, 2018-01-31 15:26:43.158404';
echo '=================> Gep-Ddpg Withparamnoise: Trial 167, Buffer Size 500, Buffer Id 27, Seed 167, 2018-01-31 15:26:43.158404' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 167 --seed 167  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_27_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withparamnoise: Trial 168, Buffer Size 500, Buffer Id 28, Seed 168, 2018-01-31 15:26:43.158431';
echo '=================> Gep-Ddpg Withparamnoise: Trial 168, Buffer Size 500, Buffer Id 28, Seed 168, 2018-01-31 15:26:43.158431' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 168 --seed 168  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_28_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withparamnoise: Trial 169, Buffer Size 500, Buffer Id 29, Seed 169, 2018-01-31 15:26:43.158455';
echo '=================> Gep-Ddpg Withparamnoise: Trial 169, Buffer Size 500, Buffer Id 29, Seed 169, 2018-01-31 15:26:43.158455' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 169 --seed 169  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_29_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withparamnoise: Trial 170, Buffer Size 500, Buffer Id 30, Seed 170, 2018-01-31 15:26:43.158478';
echo '=================> Gep-Ddpg Withparamnoise: Trial 170, Buffer Size 500, Buffer Id 30, Seed 170, 2018-01-31 15:26:43.158478' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 170 --seed 170  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_30_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
