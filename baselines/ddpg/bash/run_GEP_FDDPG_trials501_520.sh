#!/bin/sh
#SBATCH -n20
#SBATCH -p long_sirocco
#SBATCH -t 30:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_GEP_FDDPG_trials501_520.sh.err
#SBATCH -o run_GEP_FDDPG_trials501_520.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep_Fddpg Withnoise: Trial 501, Buffer Size 500, Buffer Id 41, Seed 501, 2018-02-13 11:44:44.417774';
echo '=================> Gep_Fddpg Withnoise: Trial 501, Buffer Size 500, Buffer Id 41, Seed 501, 2018-02-13 11:44:44.417774' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 501 --seed 501 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_41 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 502, Buffer Size 500, Buffer Id 42, Seed 502, 2018-02-13 11:44:44.417802';
echo '=================> Gep_Fddpg Withnoise: Trial 502, Buffer Size 500, Buffer Id 42, Seed 502, 2018-02-13 11:44:44.417802' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 502 --seed 502 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_42 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 503, Buffer Size 500, Buffer Id 43, Seed 503, 2018-02-13 11:44:44.417814';
echo '=================> Gep_Fddpg Withnoise: Trial 503, Buffer Size 500, Buffer Id 43, Seed 503, 2018-02-13 11:44:44.417814' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 503 --seed 503 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_43 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 504, Buffer Size 500, Buffer Id 44, Seed 504, 2018-02-13 11:44:44.417824';
echo '=================> Gep_Fddpg Withnoise: Trial 504, Buffer Size 500, Buffer Id 44, Seed 504, 2018-02-13 11:44:44.417824' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 504 --seed 504 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_44 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 505, Buffer Size 500, Buffer Id 45, Seed 505, 2018-02-13 11:44:44.417833';
echo '=================> Gep_Fddpg Withnoise: Trial 505, Buffer Size 500, Buffer Id 45, Seed 505, 2018-02-13 11:44:44.417833' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 505 --seed 505 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_45 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 506, Buffer Size 500, Buffer Id 46, Seed 506, 2018-02-13 11:44:44.417842';
echo '=================> Gep_Fddpg Withnoise: Trial 506, Buffer Size 500, Buffer Id 46, Seed 506, 2018-02-13 11:44:44.417842' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 506 --seed 506 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_46 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 507, Buffer Size 500, Buffer Id 47, Seed 507, 2018-02-13 11:44:44.417851';
echo '=================> Gep_Fddpg Withnoise: Trial 507, Buffer Size 500, Buffer Id 47, Seed 507, 2018-02-13 11:44:44.417851' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 507 --seed 507 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_47 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 508, Buffer Size 500, Buffer Id 48, Seed 508, 2018-02-13 11:44:44.417861';
echo '=================> Gep_Fddpg Withnoise: Trial 508, Buffer Size 500, Buffer Id 48, Seed 508, 2018-02-13 11:44:44.417861' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 508 --seed 508 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_48 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 509, Buffer Size 500, Buffer Id 49, Seed 509, 2018-02-13 11:44:44.417869';
echo '=================> Gep_Fddpg Withnoise: Trial 509, Buffer Size 500, Buffer Id 49, Seed 509, 2018-02-13 11:44:44.417869' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 509 --seed 509 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_49 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 510, Buffer Size 500, Buffer Id 50, Seed 510, 2018-02-13 11:44:44.417878';
echo '=================> Gep_Fddpg Withnoise: Trial 510, Buffer Size 500, Buffer Id 50, Seed 510, 2018-02-13 11:44:44.417878' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 510 --seed 510 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_50 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 511, Buffer Size 500, Buffer Id 51, Seed 511, 2018-02-13 11:44:44.417888';
echo '=================> Gep_Fddpg Withnoise: Trial 511, Buffer Size 500, Buffer Id 51, Seed 511, 2018-02-13 11:44:44.417888' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 511 --seed 511 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_51 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 512, Buffer Size 500, Buffer Id 52, Seed 512, 2018-02-13 11:44:44.417897';
echo '=================> Gep_Fddpg Withnoise: Trial 512, Buffer Size 500, Buffer Id 52, Seed 512, 2018-02-13 11:44:44.417897' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 512 --seed 512 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_52 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 513, Buffer Size 500, Buffer Id 53, Seed 513, 2018-02-13 11:44:44.417906';
echo '=================> Gep_Fddpg Withnoise: Trial 513, Buffer Size 500, Buffer Id 53, Seed 513, 2018-02-13 11:44:44.417906' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 513 --seed 513 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_53 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 514, Buffer Size 500, Buffer Id 54, Seed 514, 2018-02-13 11:44:44.417915';
echo '=================> Gep_Fddpg Withnoise: Trial 514, Buffer Size 500, Buffer Id 54, Seed 514, 2018-02-13 11:44:44.417915' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 514 --seed 514 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_54 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 515, Buffer Size 500, Buffer Id 55, Seed 515, 2018-02-13 11:44:44.417953';
echo '=================> Gep_Fddpg Withnoise: Trial 515, Buffer Size 500, Buffer Id 55, Seed 515, 2018-02-13 11:44:44.417953' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 515 --seed 515 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_55 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 516, Buffer Size 500, Buffer Id 56, Seed 516, 2018-02-13 11:44:44.417964';
echo '=================> Gep_Fddpg Withnoise: Trial 516, Buffer Size 500, Buffer Id 56, Seed 516, 2018-02-13 11:44:44.417964' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 516 --seed 516 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_56 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 517, Buffer Size 500, Buffer Id 57, Seed 517, 2018-02-13 11:44:44.417973';
echo '=================> Gep_Fddpg Withnoise: Trial 517, Buffer Size 500, Buffer Id 57, Seed 517, 2018-02-13 11:44:44.417973' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 517 --seed 517 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_57 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 518, Buffer Size 500, Buffer Id 58, Seed 518, 2018-02-13 11:44:44.417982';
echo '=================> Gep_Fddpg Withnoise: Trial 518, Buffer Size 500, Buffer Id 58, Seed 518, 2018-02-13 11:44:44.417982' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 518 --seed 518 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_58 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 519, Buffer Size 500, Buffer Id 59, Seed 519, 2018-02-13 11:44:44.417991';
echo '=================> Gep_Fddpg Withnoise: Trial 519, Buffer Size 500, Buffer Id 59, Seed 519, 2018-02-13 11:44:44.417991' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 519 --seed 519 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_59 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 520, Buffer Size 500, Buffer Id 60, Seed 520, 2018-02-13 11:44:44.418000';
echo '=================> Gep_Fddpg Withnoise: Trial 520, Buffer Size 500, Buffer Id 60, Seed 520, 2018-02-13 11:44:44.418000' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 520 --seed 520 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_60 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait