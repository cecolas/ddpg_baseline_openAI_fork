#!/bin/sh
#SBATCH -n1
#SBATCH -p long_sirocco
#SBATCH -t 30:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_GEP_DDPG_trials61_80.sh.err
#SBATCH -o run_GEP_DDPG_trials61_80.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withnoise: Trial 61, Buffer Size 500, Buffer Id 1, Seed 61, 2018-01-29 15:29:55.832452';
echo '=================> Gep-Ddpg Withnoise: Trial 61, Buffer Size 500, Buffer Id 1, Seed 61, 2018-01-29 15:29:55.832452' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 61 --seed 61 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_1_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 62, Buffer Size 500, Buffer Id 2, Seed 62, 2018-01-29 15:29:55.832479';
echo '=================> Gep-Ddpg Withnoise: Trial 62, Buffer Size 500, Buffer Id 2, Seed 62, 2018-01-29 15:29:55.832479' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 62 --seed 62 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_2_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 63, Buffer Size 500, Buffer Id 3, Seed 63, 2018-01-29 15:29:55.832492';
echo '=================> Gep-Ddpg Withnoise: Trial 63, Buffer Size 500, Buffer Id 3, Seed 63, 2018-01-29 15:29:55.832492' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 63 --seed 63 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_3_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 64, Buffer Size 500, Buffer Id 4, Seed 64, 2018-01-29 15:29:55.832503';
echo '=================> Gep-Ddpg Withnoise: Trial 64, Buffer Size 500, Buffer Id 4, Seed 64, 2018-01-29 15:29:55.832503' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 64 --seed 64 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_4_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 65, Buffer Size 500, Buffer Id 5, Seed 65, 2018-01-29 15:29:55.832512';
echo '=================> Gep-Ddpg Withnoise: Trial 65, Buffer Size 500, Buffer Id 5, Seed 65, 2018-01-29 15:29:55.832512' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 65 --seed 65 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_5_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 66, Buffer Size 500, Buffer Id 6, Seed 66, 2018-01-29 15:29:55.832522';
echo '=================> Gep-Ddpg Withnoise: Trial 66, Buffer Size 500, Buffer Id 6, Seed 66, 2018-01-29 15:29:55.832522' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 66 --seed 66 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_6_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 67, Buffer Size 500, Buffer Id 7, Seed 67, 2018-01-29 15:29:55.832531';
echo '=================> Gep-Ddpg Withnoise: Trial 67, Buffer Size 500, Buffer Id 7, Seed 67, 2018-01-29 15:29:55.832531' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 67 --seed 67 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_7_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 68, Buffer Size 500, Buffer Id 8, Seed 68, 2018-01-29 15:29:55.832542';
echo '=================> Gep-Ddpg Withnoise: Trial 68, Buffer Size 500, Buffer Id 8, Seed 68, 2018-01-29 15:29:55.832542' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 68 --seed 68 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_8_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 69, Buffer Size 500, Buffer Id 9, Seed 69, 2018-01-29 15:29:55.832552';
echo '=================> Gep-Ddpg Withnoise: Trial 69, Buffer Size 500, Buffer Id 9, Seed 69, 2018-01-29 15:29:55.832552' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 69 --seed 69 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_9_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 70, Buffer Size 500, Buffer Id 10, Seed 70, 2018-01-29 15:29:55.832561';
echo '=================> Gep-Ddpg Withnoise: Trial 70, Buffer Size 500, Buffer Id 10, Seed 70, 2018-01-29 15:29:55.832561' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 70 --seed 70 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_10_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 71, Buffer Size 500, Buffer Id 11, Seed 71, 2018-01-29 15:29:55.832571';
echo '=================> Gep-Ddpg Withnoise: Trial 71, Buffer Size 500, Buffer Id 11, Seed 71, 2018-01-29 15:29:55.832571' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 71 --seed 71 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_11_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 72, Buffer Size 500, Buffer Id 12, Seed 72, 2018-01-29 15:29:55.832580';
echo '=================> Gep-Ddpg Withnoise: Trial 72, Buffer Size 500, Buffer Id 12, Seed 72, 2018-01-29 15:29:55.832580' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 72 --seed 72 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_12_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 73, Buffer Size 500, Buffer Id 13, Seed 73, 2018-01-29 15:29:55.832590';
echo '=================> Gep-Ddpg Withnoise: Trial 73, Buffer Size 500, Buffer Id 13, Seed 73, 2018-01-29 15:29:55.832590' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 73 --seed 73 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_13_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 74, Buffer Size 500, Buffer Id 14, Seed 74, 2018-01-29 15:29:55.832600';
echo '=================> Gep-Ddpg Withnoise: Trial 74, Buffer Size 500, Buffer Id 14, Seed 74, 2018-01-29 15:29:55.832600' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 74 --seed 74 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_14_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 75, Buffer Size 500, Buffer Id 15, Seed 75, 2018-01-29 15:29:55.832634';
echo '=================> Gep-Ddpg Withnoise: Trial 75, Buffer Size 500, Buffer Id 15, Seed 75, 2018-01-29 15:29:55.832634' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 75 --seed 75 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_15_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 76, Buffer Size 500, Buffer Id 16, Seed 76, 2018-01-29 15:29:55.832645';
echo '=================> Gep-Ddpg Withnoise: Trial 76, Buffer Size 500, Buffer Id 16, Seed 76, 2018-01-29 15:29:55.832645' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 76 --seed 76 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_16_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 77, Buffer Size 500, Buffer Id 17, Seed 77, 2018-01-29 15:29:55.832655';
echo '=================> Gep-Ddpg Withnoise: Trial 77, Buffer Size 500, Buffer Id 17, Seed 77, 2018-01-29 15:29:55.832655' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 77 --seed 77 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_17_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 78, Buffer Size 500, Buffer Id 18, Seed 78, 2018-01-29 15:29:55.832664';
echo '=================> Gep-Ddpg Withnoise: Trial 78, Buffer Size 500, Buffer Id 18, Seed 78, 2018-01-29 15:29:55.832664' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 78 --seed 78 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_18_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 79, Buffer Size 500, Buffer Id 19, Seed 79, 2018-01-29 15:29:55.832674';
echo '=================> Gep-Ddpg Withnoise: Trial 79, Buffer Size 500, Buffer Id 19, Seed 79, 2018-01-29 15:29:55.832674' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 79 --seed 79 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_19_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 80, Buffer Size 500, Buffer Id 20, Seed 80, 2018-01-29 15:29:55.832683';
echo '=================> Gep-Ddpg Withnoise: Trial 80, Buffer Size 500, Buffer Id 20, Seed 80, 2018-01-29 15:29:55.832683' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 80 --seed 80 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_20_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait