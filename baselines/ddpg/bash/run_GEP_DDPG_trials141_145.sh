#!/bin/sh
export EXP_INTERP='/usr/bin/python3' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withnoise: Trial 141, Buffer Size 500, Buffer Id 1, Seed 141, 2018-01-31 15:21:24.355698';
echo '=================> Gep-Ddpg Withnoise: Trial 141, Buffer Size 500, Buffer Id 1, Seed 141, 2018-01-31 15:21:24.355698' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 141 --seed 141 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_1_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withnoise: Trial 142, Buffer Size 500, Buffer Id 2, Seed 142, 2018-01-31 15:21:24.355749';
echo '=================> Gep-Ddpg Withnoise: Trial 142, Buffer Size 500, Buffer Id 2, Seed 142, 2018-01-31 15:21:24.355749' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 142 --seed 142 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_2_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withnoise: Trial 143, Buffer Size 500, Buffer Id 3, Seed 143, 2018-01-31 15:21:24.355793';
echo '=================> Gep-Ddpg Withnoise: Trial 143, Buffer Size 500, Buffer Id 3, Seed 143, 2018-01-31 15:21:24.355793' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 143 --seed 143 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_3_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withnoise: Trial 144, Buffer Size 500, Buffer Id 4, Seed 144, 2018-01-31 15:21:24.355813';
echo '=================> Gep-Ddpg Withnoise: Trial 144, Buffer Size 500, Buffer Id 4, Seed 144, 2018-01-31 15:21:24.355813' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 144 --seed 144 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_4_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withnoise: Trial 145, Buffer Size 500, Buffer Id 5, Seed 145, 2018-01-31 15:21:24.355831';
echo '=================> Gep-Ddpg Withnoise: Trial 145, Buffer Size 500, Buffer Id 5, Seed 145, 2018-01-31 15:21:24.355831' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 145 --seed 145 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_5_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
