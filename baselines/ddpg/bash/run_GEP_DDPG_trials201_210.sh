#!/bin/sh
#SBATCH -n10
#SBATCH -p long_sirocco
#SBATCH -t 25:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_GEP_DDPG_trials201_210.sh.err
#SBATCH -o run_GEP_DDPG_trials201_210.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withparamnoise: Trial 201, Buffer Size 50, Buffer Id 21, Seed 201, 2018-02-06 13:25:34.712412';
echo '=================> Gep-Ddpg Withparamnoise: Trial 201, Buffer Size 50, Buffer Id 21, Seed 201, 2018-02-06 13:25:34.712412' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 201 --seed 201  --noise_type adaptive-param_0.2 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_21 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 202, Buffer Size 50, Buffer Id 22, Seed 202, 2018-02-06 13:25:34.712460';
echo '=================> Gep-Ddpg Withparamnoise: Trial 202, Buffer Size 50, Buffer Id 22, Seed 202, 2018-02-06 13:25:34.712460' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 202 --seed 202  --noise_type adaptive-param_0.2 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_22 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 203, Buffer Size 50, Buffer Id 23, Seed 203, 2018-02-06 13:25:34.712488';
echo '=================> Gep-Ddpg Withparamnoise: Trial 203, Buffer Size 50, Buffer Id 23, Seed 203, 2018-02-06 13:25:34.712488' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 203 --seed 203  --noise_type adaptive-param_0.2 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_23 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 204, Buffer Size 50, Buffer Id 24, Seed 204, 2018-02-06 13:25:34.712513';
echo '=================> Gep-Ddpg Withparamnoise: Trial 204, Buffer Size 50, Buffer Id 24, Seed 204, 2018-02-06 13:25:34.712513' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 204 --seed 204  --noise_type adaptive-param_0.2 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_24 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 205, Buffer Size 50, Buffer Id 25, Seed 205, 2018-02-06 13:25:34.712536';
echo '=================> Gep-Ddpg Withparamnoise: Trial 205, Buffer Size 50, Buffer Id 25, Seed 205, 2018-02-06 13:25:34.712536' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 205 --seed 205  --noise_type adaptive-param_0.2 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_25 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 206, Buffer Size 50, Buffer Id 26, Seed 206, 2018-02-06 13:25:34.712560';
echo '=================> Gep-Ddpg Withparamnoise: Trial 206, Buffer Size 50, Buffer Id 26, Seed 206, 2018-02-06 13:25:34.712560' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 206 --seed 206  --noise_type adaptive-param_0.2 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_26 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 207, Buffer Size 50, Buffer Id 27, Seed 207, 2018-02-06 13:25:34.712581';
echo '=================> Gep-Ddpg Withparamnoise: Trial 207, Buffer Size 50, Buffer Id 27, Seed 207, 2018-02-06 13:25:34.712581' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 207 --seed 207  --noise_type adaptive-param_0.2 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_27 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 208, Buffer Size 50, Buffer Id 28, Seed 208, 2018-02-06 13:25:34.712603';
echo '=================> Gep-Ddpg Withparamnoise: Trial 208, Buffer Size 50, Buffer Id 28, Seed 208, 2018-02-06 13:25:34.712603' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 208 --seed 208  --noise_type adaptive-param_0.2 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_28 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 209, Buffer Size 50, Buffer Id 29, Seed 209, 2018-02-06 13:25:34.712625';
echo '=================> Gep-Ddpg Withparamnoise: Trial 209, Buffer Size 50, Buffer Id 29, Seed 209, 2018-02-06 13:25:34.712625' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 209 --seed 209  --noise_type adaptive-param_0.2 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_29 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 210, Buffer Size 50, Buffer Id 30, Seed 210, 2018-02-06 13:25:34.712646';
echo '=================> Gep-Ddpg Withparamnoise: Trial 210, Buffer Size 50, Buffer Id 30, Seed 210, 2018-02-06 13:25:34.712646' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 210 --seed 210  --noise_type adaptive-param_0.2 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_30 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait