#!/bin/sh
export EXP_INTERP='/usr/bin/python3' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withparamnoise: Trial 161, Buffer Size 500, Buffer Id 21, Seed 161, 2018-01-31 15:26:23.909272';
echo '=================> Gep-Ddpg Withparamnoise: Trial 161, Buffer Size 500, Buffer Id 21, Seed 161, 2018-01-31 15:26:23.909272' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 161 --seed 161  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_21_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withparamnoise: Trial 162, Buffer Size 500, Buffer Id 22, Seed 162, 2018-01-31 15:26:23.909307';
echo '=================> Gep-Ddpg Withparamnoise: Trial 162, Buffer Size 500, Buffer Id 22, Seed 162, 2018-01-31 15:26:23.909307' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 162 --seed 162  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_22_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withparamnoise: Trial 163, Buffer Size 500, Buffer Id 23, Seed 163, 2018-01-31 15:26:23.909322';
echo '=================> Gep-Ddpg Withparamnoise: Trial 163, Buffer Size 500, Buffer Id 23, Seed 163, 2018-01-31 15:26:23.909322' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 163 --seed 163  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_23_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withparamnoise: Trial 164, Buffer Size 500, Buffer Id 24, Seed 164, 2018-01-31 15:26:23.909335';
echo '=================> Gep-Ddpg Withparamnoise: Trial 164, Buffer Size 500, Buffer Id 24, Seed 164, 2018-01-31 15:26:23.909335' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 164 --seed 164  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_24_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withparamnoise: Trial 165, Buffer Size 500, Buffer Id 25, Seed 165, 2018-01-31 15:26:23.909345';
echo '=================> Gep-Ddpg Withparamnoise: Trial 165, Buffer Size 500, Buffer Id 25, Seed 165, 2018-01-31 15:26:23.909345' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 165 --seed 165  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_25_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
