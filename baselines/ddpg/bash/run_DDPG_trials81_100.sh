#!/bin/sh
#SBATCH -n1
#SBATCH -p long_sirocco
#SBATCH -t 60:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_DDPG_trials81_100.sh.err
#SBATCH -o run_DDPG_trials81_100.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Ddpg Withparamnoise: Trial 81, Seed 81, 2018-01-29 20:33:11.491910';
echo '=================> Ddpg Withparamnoise: Trial 81, Seed 81, 2018-01-29 20:33:11.491910' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 81 --study DDPG --noise_type adaptive-param_0.2 --seed 81 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 82, Seed 82, 2018-01-29 20:33:11.491955';
echo '=================> Ddpg Withparamnoise: Trial 82, Seed 82, 2018-01-29 20:33:11.491955' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 82 --study DDPG --noise_type adaptive-param_0.2 --seed 82 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 83, Seed 83, 2018-01-29 20:33:11.491976';
echo '=================> Ddpg Withparamnoise: Trial 83, Seed 83, 2018-01-29 20:33:11.491976' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 83 --study DDPG --noise_type adaptive-param_0.2 --seed 83 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 84, Seed 84, 2018-01-29 20:33:11.491987';
echo '=================> Ddpg Withparamnoise: Trial 84, Seed 84, 2018-01-29 20:33:11.491987' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 84 --study DDPG --noise_type adaptive-param_0.2 --seed 84 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 85, Seed 85, 2018-01-29 20:33:11.491997';
echo '=================> Ddpg Withparamnoise: Trial 85, Seed 85, 2018-01-29 20:33:11.491997' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 85 --study DDPG --noise_type adaptive-param_0.2 --seed 85 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 86, Seed 86, 2018-01-29 20:33:11.492006';
echo '=================> Ddpg Withparamnoise: Trial 86, Seed 86, 2018-01-29 20:33:11.492006' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 86 --study DDPG --noise_type adaptive-param_0.2 --seed 86 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 87, Seed 87, 2018-01-29 20:33:11.492015';
echo '=================> Ddpg Withparamnoise: Trial 87, Seed 87, 2018-01-29 20:33:11.492015' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 87 --study DDPG --noise_type adaptive-param_0.2 --seed 87 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 88, Seed 88, 2018-01-29 20:33:11.492025';
echo '=================> Ddpg Withparamnoise: Trial 88, Seed 88, 2018-01-29 20:33:11.492025' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 88 --study DDPG --noise_type adaptive-param_0.2 --seed 88 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 89, Seed 89, 2018-01-29 20:33:11.492034';
echo '=================> Ddpg Withparamnoise: Trial 89, Seed 89, 2018-01-29 20:33:11.492034' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 89 --study DDPG --noise_type adaptive-param_0.2 --seed 89 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 90, Seed 90, 2018-01-29 20:33:11.492044';
echo '=================> Ddpg Withparamnoise: Trial 90, Seed 90, 2018-01-29 20:33:11.492044' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 90 --study DDPG --noise_type adaptive-param_0.2 --seed 90 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 91, Seed 91, 2018-01-29 20:33:11.492053';
echo '=================> Ddpg Withparamnoise: Trial 91, Seed 91, 2018-01-29 20:33:11.492053' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 91 --study DDPG --noise_type adaptive-param_0.2 --seed 91 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 92, Seed 92, 2018-01-29 20:33:11.492063';
echo '=================> Ddpg Withparamnoise: Trial 92, Seed 92, 2018-01-29 20:33:11.492063' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 92 --study DDPG --noise_type adaptive-param_0.2 --seed 92 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 93, Seed 93, 2018-01-29 20:33:11.492072';
echo '=================> Ddpg Withparamnoise: Trial 93, Seed 93, 2018-01-29 20:33:11.492072' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 93 --study DDPG --noise_type adaptive-param_0.2 --seed 93 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 94, Seed 94, 2018-01-29 20:33:11.492081';
echo '=================> Ddpg Withparamnoise: Trial 94, Seed 94, 2018-01-29 20:33:11.492081' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 94 --study DDPG --noise_type adaptive-param_0.2 --seed 94 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 95, Seed 95, 2018-01-29 20:33:11.492091';
echo '=================> Ddpg Withparamnoise: Trial 95, Seed 95, 2018-01-29 20:33:11.492091' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 95 --study DDPG --noise_type adaptive-param_0.2 --seed 95 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 96, Seed 96, 2018-01-29 20:33:11.492100';
echo '=================> Ddpg Withparamnoise: Trial 96, Seed 96, 2018-01-29 20:33:11.492100' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 96 --study DDPG --noise_type adaptive-param_0.2 --seed 96 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 97, Seed 97, 2018-01-29 20:33:11.492111';
echo '=================> Ddpg Withparamnoise: Trial 97, Seed 97, 2018-01-29 20:33:11.492111' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 97 --study DDPG --noise_type adaptive-param_0.2 --seed 97 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 98, Seed 98, 2018-01-29 20:33:11.492122';
echo '=================> Ddpg Withparamnoise: Trial 98, Seed 98, 2018-01-29 20:33:11.492122' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 98 --study DDPG --noise_type adaptive-param_0.2 --seed 98 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 99, Seed 99, 2018-01-29 20:33:11.492132';
echo '=================> Ddpg Withparamnoise: Trial 99, Seed 99, 2018-01-29 20:33:11.492132' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 99 --study DDPG --noise_type adaptive-param_0.2 --seed 99 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 100, Seed 100, 2018-01-29 20:33:11.492192';
echo '=================> Ddpg Withparamnoise: Trial 100, Seed 100, 2018-01-29 20:33:11.492192' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 100 --study DDPG --noise_type adaptive-param_0.2 --seed 100 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait