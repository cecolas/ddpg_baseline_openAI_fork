#!/bin/sh
#SBATCH -n20
#SBATCH -p long_sirocco
#SBATCH -t 30:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_GEP_FDDPG_trials621_639.sh.err
#SBATCH -o run_GEP_FDDPG_trials621_639.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep_Fddpg Withnoise: Trial 621, Buffer Size 900, Buffer Id 1, Seed 621, 2018-02-22 14:53:11.315111';
echo '=================> Gep_Fddpg Withnoise: Trial 621, Buffer Size 900, Buffer Id 1, Seed 621, 2018-02-22 14:53:11.315111' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 621 --seed 621 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_900_1 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 622, Buffer Size 900, Buffer Id 2, Seed 622, 2018-02-22 14:53:11.315160';
echo '=================> Gep_Fddpg Withnoise: Trial 622, Buffer Size 900, Buffer Id 2, Seed 622, 2018-02-22 14:53:11.315160' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 622 --seed 622 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_900_2 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 623, Buffer Size 900, Buffer Id 3, Seed 623, 2018-02-22 14:53:11.315187';
echo '=================> Gep_Fddpg Withnoise: Trial 623, Buffer Size 900, Buffer Id 3, Seed 623, 2018-02-22 14:53:11.315187' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 623 --seed 623 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_900_3 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 624, Buffer Size 900, Buffer Id 4, Seed 624, 2018-02-22 14:53:11.315207';
echo '=================> Gep_Fddpg Withnoise: Trial 624, Buffer Size 900, Buffer Id 4, Seed 624, 2018-02-22 14:53:11.315207' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 624 --seed 624 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_900_4 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 625, Buffer Size 900, Buffer Id 5, Seed 625, 2018-02-22 14:53:11.315224';
echo '=================> Gep_Fddpg Withnoise: Trial 625, Buffer Size 900, Buffer Id 5, Seed 625, 2018-02-22 14:53:11.315224' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 625 --seed 625 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_900_5 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 626, Buffer Size 900, Buffer Id 6, Seed 626, 2018-02-22 14:53:11.315242';
echo '=================> Gep_Fddpg Withnoise: Trial 626, Buffer Size 900, Buffer Id 6, Seed 626, 2018-02-22 14:53:11.315242' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 626 --seed 626 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_900_6 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 627, Buffer Size 900, Buffer Id 7, Seed 627, 2018-02-22 14:53:11.315260';
echo '=================> Gep_Fddpg Withnoise: Trial 627, Buffer Size 900, Buffer Id 7, Seed 627, 2018-02-22 14:53:11.315260' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 627 --seed 627 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_900_7 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 628, Buffer Size 900, Buffer Id 8, Seed 628, 2018-02-22 14:53:11.315276';
echo '=================> Gep_Fddpg Withnoise: Trial 628, Buffer Size 900, Buffer Id 8, Seed 628, 2018-02-22 14:53:11.315276' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 628 --seed 628 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_900_8 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 629, Buffer Size 900, Buffer Id 9, Seed 629, 2018-02-22 14:53:11.315294';
echo '=================> Gep_Fddpg Withnoise: Trial 629, Buffer Size 900, Buffer Id 9, Seed 629, 2018-02-22 14:53:11.315294' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 629 --seed 629 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_900_9 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 630, Buffer Size 900, Buffer Id 10, Seed 630, 2018-02-22 14:53:11.315311';
echo '=================> Gep_Fddpg Withnoise: Trial 630, Buffer Size 900, Buffer Id 10, Seed 630, 2018-02-22 14:53:11.315311' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 630 --seed 630 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_900_10 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 631, Buffer Size 900, Buffer Id 11, Seed 631, 2018-02-22 14:53:11.315329';
echo '=================> Gep_Fddpg Withnoise: Trial 631, Buffer Size 900, Buffer Id 11, Seed 631, 2018-02-22 14:53:11.315329' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 631 --seed 631 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_900_11 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 632, Buffer Size 900, Buffer Id 12, Seed 632, 2018-02-22 14:53:11.315344';
echo '=================> Gep_Fddpg Withnoise: Trial 632, Buffer Size 900, Buffer Id 12, Seed 632, 2018-02-22 14:53:11.315344' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 632 --seed 632 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_900_12 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 633, Buffer Size 900, Buffer Id 13, Seed 633, 2018-02-22 14:53:11.315361';
echo '=================> Gep_Fddpg Withnoise: Trial 633, Buffer Size 900, Buffer Id 13, Seed 633, 2018-02-22 14:53:11.315361' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 633 --seed 633 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_900_13 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 634, Buffer Size 900, Buffer Id 14, Seed 634, 2018-02-22 14:53:11.315378';
echo '=================> Gep_Fddpg Withnoise: Trial 634, Buffer Size 900, Buffer Id 14, Seed 634, 2018-02-22 14:53:11.315378' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 634 --seed 634 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_900_14 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 635, Buffer Size 900, Buffer Id 15, Seed 635, 2018-02-22 14:53:11.315436';
echo '=================> Gep_Fddpg Withnoise: Trial 635, Buffer Size 900, Buffer Id 15, Seed 635, 2018-02-22 14:53:11.315436' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 635 --seed 635 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_900_15 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 636, Buffer Size 900, Buffer Id 16, Seed 636, 2018-02-22 14:53:11.315458';
echo '=================> Gep_Fddpg Withnoise: Trial 636, Buffer Size 900, Buffer Id 16, Seed 636, 2018-02-22 14:53:11.315458' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 636 --seed 636 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_900_16 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 637, Buffer Size 900, Buffer Id 17, Seed 637, 2018-02-22 14:53:11.315476';
echo '=================> Gep_Fddpg Withnoise: Trial 637, Buffer Size 900, Buffer Id 17, Seed 637, 2018-02-22 14:53:11.315476' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 637 --seed 637 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_900_17 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 638, Buffer Size 900, Buffer Id 18, Seed 638, 2018-02-22 14:53:11.315493';
echo '=================> Gep_Fddpg Withnoise: Trial 638, Buffer Size 900, Buffer Id 18, Seed 638, 2018-02-22 14:53:11.315493' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 638 --seed 638 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_900_18 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 639, Buffer Size 900, Buffer Id 19, Seed 639, 2018-02-22 14:53:11.315511';
echo '=================> Gep_Fddpg Withnoise: Trial 639, Buffer Size 900, Buffer Id 19, Seed 639, 2018-02-22 14:53:11.315511' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 639 --seed 639 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_900_19 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait