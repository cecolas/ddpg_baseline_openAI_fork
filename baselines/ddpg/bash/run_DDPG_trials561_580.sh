#!/bin/sh
#SBATCH -n1
#SBATCH -p long_sirocco
#SBATCH -t 60:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_DDPG_trials561_580.sh.err
#SBATCH -o run_DDPG_trials561_580.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Ddpg Withnoise: Trial 561, Seed 561, 2018-02-14 18:00:40.953601';
echo '=================> Ddpg Withnoise: Trial 561, Seed 561, 2018-02-14 18:00:40.953601' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 561 --study DDPG --noise_type ou_0.0 --seed 561 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 562, Seed 562, 2018-02-14 18:00:40.953634';
echo '=================> Ddpg Withnoise: Trial 562, Seed 562, 2018-02-14 18:00:40.953634' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 562 --study DDPG --noise_type ou_0.0 --seed 562 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 563, Seed 563, 2018-02-14 18:00:40.953641';
echo '=================> Ddpg Withnoise: Trial 563, Seed 563, 2018-02-14 18:00:40.953641' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 563 --study DDPG --noise_type ou_0.0 --seed 563 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 564, Seed 564, 2018-02-14 18:00:40.953648';
echo '=================> Ddpg Withnoise: Trial 564, Seed 564, 2018-02-14 18:00:40.953648' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 564 --study DDPG --noise_type ou_0.0 --seed 564 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 565, Seed 565, 2018-02-14 18:00:40.953653';
echo '=================> Ddpg Withnoise: Trial 565, Seed 565, 2018-02-14 18:00:40.953653' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 565 --study DDPG --noise_type ou_0.0 --seed 565 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 566, Seed 566, 2018-02-14 18:00:40.953659';
echo '=================> Ddpg Withnoise: Trial 566, Seed 566, 2018-02-14 18:00:40.953659' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 566 --study DDPG --noise_type ou_0.0 --seed 566 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 567, Seed 567, 2018-02-14 18:00:40.953664';
echo '=================> Ddpg Withnoise: Trial 567, Seed 567, 2018-02-14 18:00:40.953664' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 567 --study DDPG --noise_type ou_0.0 --seed 567 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 568, Seed 568, 2018-02-14 18:00:40.953670';
echo '=================> Ddpg Withnoise: Trial 568, Seed 568, 2018-02-14 18:00:40.953670' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 568 --study DDPG --noise_type ou_0.0 --seed 568 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 569, Seed 569, 2018-02-14 18:00:40.953675';
echo '=================> Ddpg Withnoise: Trial 569, Seed 569, 2018-02-14 18:00:40.953675' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 569 --study DDPG --noise_type ou_0.0 --seed 569 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 570, Seed 570, 2018-02-14 18:00:40.953680';
echo '=================> Ddpg Withnoise: Trial 570, Seed 570, 2018-02-14 18:00:40.953680' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 570 --study DDPG --noise_type ou_0.0 --seed 570 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 571, Seed 571, 2018-02-14 18:00:40.953685';
echo '=================> Ddpg Withnoise: Trial 571, Seed 571, 2018-02-14 18:00:40.953685' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 571 --study DDPG --noise_type ou_0.0 --seed 571 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 572, Seed 572, 2018-02-14 18:00:40.953691';
echo '=================> Ddpg Withnoise: Trial 572, Seed 572, 2018-02-14 18:00:40.953691' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 572 --study DDPG --noise_type ou_0.0 --seed 572 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 573, Seed 573, 2018-02-14 18:00:40.953696';
echo '=================> Ddpg Withnoise: Trial 573, Seed 573, 2018-02-14 18:00:40.953696' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 573 --study DDPG --noise_type ou_0.0 --seed 573 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 574, Seed 574, 2018-02-14 18:00:40.953702';
echo '=================> Ddpg Withnoise: Trial 574, Seed 574, 2018-02-14 18:00:40.953702' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 574 --study DDPG --noise_type ou_0.0 --seed 574 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 575, Seed 575, 2018-02-14 18:00:40.953707';
echo '=================> Ddpg Withnoise: Trial 575, Seed 575, 2018-02-14 18:00:40.953707' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 575 --study DDPG --noise_type ou_0.0 --seed 575 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 576, Seed 576, 2018-02-14 18:00:40.953712';
echo '=================> Ddpg Withnoise: Trial 576, Seed 576, 2018-02-14 18:00:40.953712' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 576 --study DDPG --noise_type ou_0.0 --seed 576 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 577, Seed 577, 2018-02-14 18:00:40.953717';
echo '=================> Ddpg Withnoise: Trial 577, Seed 577, 2018-02-14 18:00:40.953717' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 577 --study DDPG --noise_type ou_0.0 --seed 577 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 578, Seed 578, 2018-02-14 18:00:40.953723';
echo '=================> Ddpg Withnoise: Trial 578, Seed 578, 2018-02-14 18:00:40.953723' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 578 --study DDPG --noise_type ou_0.0 --seed 578 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 579, Seed 579, 2018-02-14 18:00:40.953728';
echo '=================> Ddpg Withnoise: Trial 579, Seed 579, 2018-02-14 18:00:40.953728' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 579 --study DDPG --noise_type ou_0.0 --seed 579 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 580, Seed 580, 2018-02-14 18:00:40.953733';
echo '=================> Ddpg Withnoise: Trial 580, Seed 580, 2018-02-14 18:00:40.953733' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 580 --study DDPG --noise_type ou_0.0 --seed 580 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait