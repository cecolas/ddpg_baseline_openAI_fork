#!/bin/sh
#SBATCH -n20
#SBATCH -p long_sirocco
#SBATCH -t 30:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_GEP_DDPG_trials411_420.sh.err
#SBATCH -o run_GEP_DDPG_trials411_420.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withnoise: Trial 411, Buffer Size 500, Buffer Id 71, Seed 411, 2018-02-07 13:52:28.564923';
echo '=================> Gep-Ddpg Withnoise: Trial 411, Buffer Size 500, Buffer Id 71, Seed 411, 2018-02-07 13:52:28.564923' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 411 --seed 411 --noise_type ou_0.0 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_71 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 412, Buffer Size 500, Buffer Id 72, Seed 412, 2018-02-07 13:52:28.564972';
echo '=================> Gep-Ddpg Withnoise: Trial 412, Buffer Size 500, Buffer Id 72, Seed 412, 2018-02-07 13:52:28.564972' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 412 --seed 412 --noise_type ou_0.0 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_72 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 413, Buffer Size 500, Buffer Id 73, Seed 413, 2018-02-07 13:52:28.564997';
echo '=================> Gep-Ddpg Withnoise: Trial 413, Buffer Size 500, Buffer Id 73, Seed 413, 2018-02-07 13:52:28.564997' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 413 --seed 413 --noise_type ou_0.0 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_73 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 414, Buffer Size 500, Buffer Id 74, Seed 414, 2018-02-07 13:52:28.565018';
echo '=================> Gep-Ddpg Withnoise: Trial 414, Buffer Size 500, Buffer Id 74, Seed 414, 2018-02-07 13:52:28.565018' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 414 --seed 414 --noise_type ou_0.0 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_74 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 415, Buffer Size 500, Buffer Id 75, Seed 415, 2018-02-07 13:52:28.565039';
echo '=================> Gep-Ddpg Withnoise: Trial 415, Buffer Size 500, Buffer Id 75, Seed 415, 2018-02-07 13:52:28.565039' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 415 --seed 415 --noise_type ou_0.0 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_75 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 416, Buffer Size 500, Buffer Id 76, Seed 416, 2018-02-07 13:52:28.565059';
echo '=================> Gep-Ddpg Withnoise: Trial 416, Buffer Size 500, Buffer Id 76, Seed 416, 2018-02-07 13:52:28.565059' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 416 --seed 416 --noise_type ou_0.0 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_76 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 417, Buffer Size 500, Buffer Id 77, Seed 417, 2018-02-07 13:52:28.565078';
echo '=================> Gep-Ddpg Withnoise: Trial 417, Buffer Size 500, Buffer Id 77, Seed 417, 2018-02-07 13:52:28.565078' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 417 --seed 417 --noise_type ou_0.0 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_77 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 418, Buffer Size 500, Buffer Id 78, Seed 418, 2018-02-07 13:52:28.565097';
echo '=================> Gep-Ddpg Withnoise: Trial 418, Buffer Size 500, Buffer Id 78, Seed 418, 2018-02-07 13:52:28.565097' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 418 --seed 418 --noise_type ou_0.0 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_78 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 419, Buffer Size 500, Buffer Id 79, Seed 419, 2018-02-07 13:52:28.565115';
echo '=================> Gep-Ddpg Withnoise: Trial 419, Buffer Size 500, Buffer Id 79, Seed 419, 2018-02-07 13:52:28.565115' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 419 --seed 419 --noise_type ou_0.0 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_79 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 420, Buffer Size 500, Buffer Id 80, Seed 420, 2018-02-07 13:52:28.565132';
echo '=================> Gep-Ddpg Withnoise: Trial 420, Buffer Size 500, Buffer Id 80, Seed 420, 2018-02-07 13:52:28.565132' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 420 --seed 420 --noise_type ou_0.0 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_80 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait