#!/bin/sh
#SBATCH -n1
#SBATCH -p long_sirocco
#SBATCH -t 60:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_GEP_DDPG_trials181_190.sh.err
#SBATCH -o run_GEP_DDPG_trials181_190.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withparamnoise: Trial 181, Buffer Size 500, Buffer Id 61, Seed 181, 2018-02-02 13:56:16.433458';
echo '=================> Gep-Ddpg Withparamnoise: Trial 181, Buffer Size 500, Buffer Id 61, Seed 181, 2018-02-02 13:56:16.433458' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 181 --seed 181  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_61 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 182, Buffer Size 500, Buffer Id 62, Seed 182, 2018-02-02 13:56:16.433506';
echo '=================> Gep-Ddpg Withparamnoise: Trial 182, Buffer Size 500, Buffer Id 62, Seed 182, 2018-02-02 13:56:16.433506' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 182 --seed 182  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_62 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 183, Buffer Size 500, Buffer Id 63, Seed 183, 2018-02-02 13:56:16.433536';
echo '=================> Gep-Ddpg Withparamnoise: Trial 183, Buffer Size 500, Buffer Id 63, Seed 183, 2018-02-02 13:56:16.433536' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 183 --seed 183  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_63 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 184, Buffer Size 500, Buffer Id 64, Seed 184, 2018-02-02 13:56:16.433561';
echo '=================> Gep-Ddpg Withparamnoise: Trial 184, Buffer Size 500, Buffer Id 64, Seed 184, 2018-02-02 13:56:16.433561' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 184 --seed 184  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_64 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 185, Buffer Size 500, Buffer Id 65, Seed 185, 2018-02-02 13:56:16.433584';
echo '=================> Gep-Ddpg Withparamnoise: Trial 185, Buffer Size 500, Buffer Id 65, Seed 185, 2018-02-02 13:56:16.433584' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 185 --seed 185  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_65 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 186, Buffer Size 500, Buffer Id 66, Seed 186, 2018-02-02 13:56:16.433612';
echo '=================> Gep-Ddpg Withparamnoise: Trial 186, Buffer Size 500, Buffer Id 66, Seed 186, 2018-02-02 13:56:16.433612' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 186 --seed 186  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_66 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 187, Buffer Size 500, Buffer Id 67, Seed 187, 2018-02-02 13:56:16.433637';
echo '=================> Gep-Ddpg Withparamnoise: Trial 187, Buffer Size 500, Buffer Id 67, Seed 187, 2018-02-02 13:56:16.433637' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 187 --seed 187  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_67 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 188, Buffer Size 500, Buffer Id 68, Seed 188, 2018-02-02 13:56:16.433660';
echo '=================> Gep-Ddpg Withparamnoise: Trial 188, Buffer Size 500, Buffer Id 68, Seed 188, 2018-02-02 13:56:16.433660' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 188 --seed 188  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_68 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 189, Buffer Size 500, Buffer Id 69, Seed 189, 2018-02-02 13:56:16.433684';
echo '=================> Gep-Ddpg Withparamnoise: Trial 189, Buffer Size 500, Buffer Id 69, Seed 189, 2018-02-02 13:56:16.433684' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 189 --seed 189  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_69 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 190, Buffer Size 500, Buffer Id 70, Seed 190, 2018-02-02 13:56:16.433708';
echo '=================> Gep-Ddpg Withparamnoise: Trial 190, Buffer Size 500, Buffer Id 70, Seed 190, 2018-02-02 13:56:16.433708' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 190 --seed 190  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_70 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait