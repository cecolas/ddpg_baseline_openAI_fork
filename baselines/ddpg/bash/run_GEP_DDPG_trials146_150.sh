#!/bin/sh
export EXP_INTERP='/usr/bin/python3' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withnoise: Trial 146, Buffer Size 500, Buffer Id 6, Seed 146, 2018-01-31 15:21:40.433803';
echo '=================> Gep-Ddpg Withnoise: Trial 146, Buffer Size 500, Buffer Id 6, Seed 146, 2018-01-31 15:21:40.433803' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 146 --seed 146 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_6_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withnoise: Trial 147, Buffer Size 500, Buffer Id 7, Seed 147, 2018-01-31 15:21:40.433849';
echo '=================> Gep-Ddpg Withnoise: Trial 147, Buffer Size 500, Buffer Id 7, Seed 147, 2018-01-31 15:21:40.433849' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 147 --seed 147 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_7_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withnoise: Trial 148, Buffer Size 500, Buffer Id 8, Seed 148, 2018-01-31 15:21:40.433875';
echo '=================> Gep-Ddpg Withnoise: Trial 148, Buffer Size 500, Buffer Id 8, Seed 148, 2018-01-31 15:21:40.433875' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 148 --seed 148 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_8_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withnoise: Trial 149, Buffer Size 500, Buffer Id 9, Seed 149, 2018-01-31 15:21:40.433896';
echo '=================> Gep-Ddpg Withnoise: Trial 149, Buffer Size 500, Buffer Id 9, Seed 149, 2018-01-31 15:21:40.433896' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 149 --seed 149 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_9_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
echo '=================> Gep-Ddpg Withnoise: Trial 150, Buffer Size 500, Buffer Id 10, Seed 150, 2018-01-31 15:21:40.433914';
echo '=================> Gep-Ddpg Withnoise: Trial 150, Buffer Size 500, Buffer Id 10, Seed 150, 2018-01-31 15:21:40.433914' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_fl_gpu.py --trial_id 150 --seed 150 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /mnt/shared/cedric/data_GEP/GEP_NNController_Cheetah/Cheetah_model_500_10_buffer --data_path /mnt/shared/cedric/DDPG/results/ 
