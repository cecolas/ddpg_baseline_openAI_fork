#!/bin/sh
#SBATCH -n10
#SBATCH -p long_sirocco
#SBATCH -t 25:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_GEP_DDPG_trials221_230.sh.err
#SBATCH -o run_GEP_DDPG_trials221_230.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withnoise: Trial 221, Buffer Size 50, Buffer Id 41, Seed 221, 2018-02-06 13:25:08.156924';
echo '=================> Gep-Ddpg Withnoise: Trial 221, Buffer Size 50, Buffer Id 41, Seed 221, 2018-02-06 13:25:08.156924' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 221 --seed 221 --noise_type ou_0.3 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_41 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 222, Buffer Size 50, Buffer Id 42, Seed 222, 2018-02-06 13:25:08.156977';
echo '=================> Gep-Ddpg Withnoise: Trial 222, Buffer Size 50, Buffer Id 42, Seed 222, 2018-02-06 13:25:08.156977' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 222 --seed 222 --noise_type ou_0.3 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_42 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 223, Buffer Size 50, Buffer Id 43, Seed 223, 2018-02-06 13:25:08.157006';
echo '=================> Gep-Ddpg Withnoise: Trial 223, Buffer Size 50, Buffer Id 43, Seed 223, 2018-02-06 13:25:08.157006' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 223 --seed 223 --noise_type ou_0.3 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_43 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 224, Buffer Size 50, Buffer Id 44, Seed 224, 2018-02-06 13:25:08.157031';
echo '=================> Gep-Ddpg Withnoise: Trial 224, Buffer Size 50, Buffer Id 44, Seed 224, 2018-02-06 13:25:08.157031' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 224 --seed 224 --noise_type ou_0.3 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_44 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 225, Buffer Size 50, Buffer Id 45, Seed 225, 2018-02-06 13:25:08.157053';
echo '=================> Gep-Ddpg Withnoise: Trial 225, Buffer Size 50, Buffer Id 45, Seed 225, 2018-02-06 13:25:08.157053' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 225 --seed 225 --noise_type ou_0.3 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_45 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 226, Buffer Size 50, Buffer Id 46, Seed 226, 2018-02-06 13:25:08.157074';
echo '=================> Gep-Ddpg Withnoise: Trial 226, Buffer Size 50, Buffer Id 46, Seed 226, 2018-02-06 13:25:08.157074' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 226 --seed 226 --noise_type ou_0.3 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_46 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 227, Buffer Size 50, Buffer Id 47, Seed 227, 2018-02-06 13:25:08.157096';
echo '=================> Gep-Ddpg Withnoise: Trial 227, Buffer Size 50, Buffer Id 47, Seed 227, 2018-02-06 13:25:08.157096' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 227 --seed 227 --noise_type ou_0.3 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_47 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 228, Buffer Size 50, Buffer Id 48, Seed 228, 2018-02-06 13:25:08.158142';
echo '=================> Gep-Ddpg Withnoise: Trial 228, Buffer Size 50, Buffer Id 48, Seed 228, 2018-02-06 13:25:08.158142' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 228 --seed 228 --noise_type ou_0.3 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_48 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 229, Buffer Size 50, Buffer Id 49, Seed 229, 2018-02-06 13:25:08.158179';
echo '=================> Gep-Ddpg Withnoise: Trial 229, Buffer Size 50, Buffer Id 49, Seed 229, 2018-02-06 13:25:08.158179' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 229 --seed 229 --noise_type ou_0.3 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_49 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 230, Buffer Size 50, Buffer Id 50, Seed 230, 2018-02-06 13:25:08.158203';
echo '=================> Gep-Ddpg Withnoise: Trial 230, Buffer Size 50, Buffer Id 50, Seed 230, 2018-02-06 13:25:08.158203' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 230 --seed 230 --noise_type ou_0.3 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_50 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait