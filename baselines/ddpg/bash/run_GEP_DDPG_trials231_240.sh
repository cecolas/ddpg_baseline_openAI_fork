#!/bin/sh
#SBATCH -n10
#SBATCH -p long_sirocco
#SBATCH -t 25:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_GEP_DDPG_trials231_240.sh.err
#SBATCH -o run_GEP_DDPG_trials231_240.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withnoise: Trial 231, Buffer Size 50, Buffer Id 51, Seed 231, 2018-02-06 13:24:51.409464';
echo '=================> Gep-Ddpg Withnoise: Trial 231, Buffer Size 50, Buffer Id 51, Seed 231, 2018-02-06 13:24:51.409464' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 231 --seed 231 --noise_type ou_0.3 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_51 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 232, Buffer Size 50, Buffer Id 52, Seed 232, 2018-02-06 13:24:51.409513';
echo '=================> Gep-Ddpg Withnoise: Trial 232, Buffer Size 50, Buffer Id 52, Seed 232, 2018-02-06 13:24:51.409513' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 232 --seed 232 --noise_type ou_0.3 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_52 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 233, Buffer Size 50, Buffer Id 53, Seed 233, 2018-02-06 13:24:51.409542';
echo '=================> Gep-Ddpg Withnoise: Trial 233, Buffer Size 50, Buffer Id 53, Seed 233, 2018-02-06 13:24:51.409542' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 233 --seed 233 --noise_type ou_0.3 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_53 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 234, Buffer Size 50, Buffer Id 54, Seed 234, 2018-02-06 13:24:51.409566';
echo '=================> Gep-Ddpg Withnoise: Trial 234, Buffer Size 50, Buffer Id 54, Seed 234, 2018-02-06 13:24:51.409566' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 234 --seed 234 --noise_type ou_0.3 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_54 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 235, Buffer Size 50, Buffer Id 55, Seed 235, 2018-02-06 13:24:51.409588';
echo '=================> Gep-Ddpg Withnoise: Trial 235, Buffer Size 50, Buffer Id 55, Seed 235, 2018-02-06 13:24:51.409588' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 235 --seed 235 --noise_type ou_0.3 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_55 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 236, Buffer Size 50, Buffer Id 56, Seed 236, 2018-02-06 13:24:51.409611';
echo '=================> Gep-Ddpg Withnoise: Trial 236, Buffer Size 50, Buffer Id 56, Seed 236, 2018-02-06 13:24:51.409611' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 236 --seed 236 --noise_type ou_0.3 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_56 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 237, Buffer Size 50, Buffer Id 57, Seed 237, 2018-02-06 13:24:51.409633';
echo '=================> Gep-Ddpg Withnoise: Trial 237, Buffer Size 50, Buffer Id 57, Seed 237, 2018-02-06 13:24:51.409633' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 237 --seed 237 --noise_type ou_0.3 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_57 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 238, Buffer Size 50, Buffer Id 58, Seed 238, 2018-02-06 13:24:51.409653';
echo '=================> Gep-Ddpg Withnoise: Trial 238, Buffer Size 50, Buffer Id 58, Seed 238, 2018-02-06 13:24:51.409653' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 238 --seed 238 --noise_type ou_0.3 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_58 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 239, Buffer Size 50, Buffer Id 59, Seed 239, 2018-02-06 13:24:51.409674';
echo '=================> Gep-Ddpg Withnoise: Trial 239, Buffer Size 50, Buffer Id 59, Seed 239, 2018-02-06 13:24:51.409674' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 239 --seed 239 --noise_type ou_0.3 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_59 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 240, Buffer Size 50, Buffer Id 60, Seed 240, 2018-02-06 13:24:51.409695';
echo '=================> Gep-Ddpg Withnoise: Trial 240, Buffer Size 50, Buffer Id 60, Seed 240, 2018-02-06 13:24:51.409695' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main_cmc.py --trial_id 240 --seed 240 --noise_type ou_0.3 --study GEP_DDPG --nb-epochs 250 --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/CMC_buffer_50_60 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait