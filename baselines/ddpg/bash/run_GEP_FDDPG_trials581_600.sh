#!/bin/sh
#SBATCH -n20
#SBATCH -p long_sirocco
#SBATCH -t 30:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_GEP_FDDPG_trials581_600.sh.err
#SBATCH -o run_GEP_FDDPG_trials581_600.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep_Fddpg Withnoise: Trial 581, Buffer Size 700, Buffer Id 1, Seed 581, 2018-02-19 14:04:40.094756';
echo '=================> Gep_Fddpg Withnoise: Trial 581, Buffer Size 700, Buffer Id 1, Seed 581, 2018-02-19 14:04:40.094756' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 581 --seed 581 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_1 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 582, Buffer Size 700, Buffer Id 2, Seed 582, 2018-02-19 14:04:40.094819';
echo '=================> Gep_Fddpg Withnoise: Trial 582, Buffer Size 700, Buffer Id 2, Seed 582, 2018-02-19 14:04:40.094819' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 582 --seed 582 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_2 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 583, Buffer Size 700, Buffer Id 3, Seed 583, 2018-02-19 14:04:40.094854';
echo '=================> Gep_Fddpg Withnoise: Trial 583, Buffer Size 700, Buffer Id 3, Seed 583, 2018-02-19 14:04:40.094854' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 583 --seed 583 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_3 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 584, Buffer Size 700, Buffer Id 4, Seed 584, 2018-02-19 14:04:40.094878';
echo '=================> Gep_Fddpg Withnoise: Trial 584, Buffer Size 700, Buffer Id 4, Seed 584, 2018-02-19 14:04:40.094878' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 584 --seed 584 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_4 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 585, Buffer Size 700, Buffer Id 5, Seed 585, 2018-02-19 14:04:40.094898';
echo '=================> Gep_Fddpg Withnoise: Trial 585, Buffer Size 700, Buffer Id 5, Seed 585, 2018-02-19 14:04:40.094898' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 585 --seed 585 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_5 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 586, Buffer Size 700, Buffer Id 6, Seed 586, 2018-02-19 14:04:40.094918';
echo '=================> Gep_Fddpg Withnoise: Trial 586, Buffer Size 700, Buffer Id 6, Seed 586, 2018-02-19 14:04:40.094918' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 586 --seed 586 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_6 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 587, Buffer Size 700, Buffer Id 7, Seed 587, 2018-02-19 14:04:40.094937';
echo '=================> Gep_Fddpg Withnoise: Trial 587, Buffer Size 700, Buffer Id 7, Seed 587, 2018-02-19 14:04:40.094937' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 587 --seed 587 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_7 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 588, Buffer Size 700, Buffer Id 8, Seed 588, 2018-02-19 14:04:40.094956';
echo '=================> Gep_Fddpg Withnoise: Trial 588, Buffer Size 700, Buffer Id 8, Seed 588, 2018-02-19 14:04:40.094956' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 588 --seed 588 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_8 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 589, Buffer Size 700, Buffer Id 9, Seed 589, 2018-02-19 14:04:40.094975';
echo '=================> Gep_Fddpg Withnoise: Trial 589, Buffer Size 700, Buffer Id 9, Seed 589, 2018-02-19 14:04:40.094975' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 589 --seed 589 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_9 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 590, Buffer Size 700, Buffer Id 10, Seed 590, 2018-02-19 14:04:40.094995';
echo '=================> Gep_Fddpg Withnoise: Trial 590, Buffer Size 700, Buffer Id 10, Seed 590, 2018-02-19 14:04:40.094995' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 590 --seed 590 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_10 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 591, Buffer Size 700, Buffer Id 11, Seed 591, 2018-02-19 14:04:40.095015';
echo '=================> Gep_Fddpg Withnoise: Trial 591, Buffer Size 700, Buffer Id 11, Seed 591, 2018-02-19 14:04:40.095015' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 591 --seed 591 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_11 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 592, Buffer Size 700, Buffer Id 12, Seed 592, 2018-02-19 14:04:40.095035';
echo '=================> Gep_Fddpg Withnoise: Trial 592, Buffer Size 700, Buffer Id 12, Seed 592, 2018-02-19 14:04:40.095035' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 592 --seed 592 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_12 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 593, Buffer Size 700, Buffer Id 13, Seed 593, 2018-02-19 14:04:40.095055';
echo '=================> Gep_Fddpg Withnoise: Trial 593, Buffer Size 700, Buffer Id 13, Seed 593, 2018-02-19 14:04:40.095055' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 593 --seed 593 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_13 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 594, Buffer Size 700, Buffer Id 14, Seed 594, 2018-02-19 14:04:40.095074';
echo '=================> Gep_Fddpg Withnoise: Trial 594, Buffer Size 700, Buffer Id 14, Seed 594, 2018-02-19 14:04:40.095074' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 594 --seed 594 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_14 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 595, Buffer Size 700, Buffer Id 15, Seed 595, 2018-02-19 14:04:40.095160';
echo '=================> Gep_Fddpg Withnoise: Trial 595, Buffer Size 700, Buffer Id 15, Seed 595, 2018-02-19 14:04:40.095160' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 595 --seed 595 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_15 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 596, Buffer Size 700, Buffer Id 16, Seed 596, 2018-02-19 14:04:40.095198';
echo '=================> Gep_Fddpg Withnoise: Trial 596, Buffer Size 700, Buffer Id 16, Seed 596, 2018-02-19 14:04:40.095198' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 596 --seed 596 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_16 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 597, Buffer Size 700, Buffer Id 17, Seed 597, 2018-02-19 14:04:40.095249';
echo '=================> Gep_Fddpg Withnoise: Trial 597, Buffer Size 700, Buffer Id 17, Seed 597, 2018-02-19 14:04:40.095249' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 597 --seed 597 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_17 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 598, Buffer Size 700, Buffer Id 18, Seed 598, 2018-02-19 14:04:40.095285';
echo '=================> Gep_Fddpg Withnoise: Trial 598, Buffer Size 700, Buffer Id 18, Seed 598, 2018-02-19 14:04:40.095285' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 598 --seed 598 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_18 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 599, Buffer Size 700, Buffer Id 19, Seed 599, 2018-02-19 14:04:40.095315';
echo '=================> Gep_Fddpg Withnoise: Trial 599, Buffer Size 700, Buffer Id 19, Seed 599, 2018-02-19 14:04:40.095315' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 599 --seed 599 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_19 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 600, Buffer Size 700, Buffer Id 20, Seed 600, 2018-02-19 14:04:40.095350';
echo '=================> Gep_Fddpg Withnoise: Trial 600, Buffer Size 700, Buffer Id 20, Seed 600, 2018-02-19 14:04:40.095350' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 600 --seed 600 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_20 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait