#!/bin/sh
#SBATCH -n20
#SBATCH -p long_sirocco
#SBATCH -t 30:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_GEP_FDDPG_trials584_589.sh.err
#SBATCH -o run_GEP_FDDPG_trials584_589.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep_Fddpg Withnoise: Trial 584, Buffer Size 700, Buffer Id 4, Seed 584, 2018-02-20 09:47:46.157364';
echo '=================> Gep_Fddpg Withnoise: Trial 584, Buffer Size 700, Buffer Id 4, Seed 584, 2018-02-20 09:47:46.157364' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 584 --seed 584 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_4 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 585, Buffer Size 700, Buffer Id 5, Seed 585, 2018-02-20 09:47:46.157394';
echo '=================> Gep_Fddpg Withnoise: Trial 585, Buffer Size 700, Buffer Id 5, Seed 585, 2018-02-20 09:47:46.157394' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 585 --seed 585 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_5 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 586, Buffer Size 700, Buffer Id 6, Seed 586, 2018-02-20 09:47:46.157406';
echo '=================> Gep_Fddpg Withnoise: Trial 586, Buffer Size 700, Buffer Id 6, Seed 586, 2018-02-20 09:47:46.157406' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 586 --seed 586 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_6 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 587, Buffer Size 700, Buffer Id 7, Seed 587, 2018-02-20 09:47:46.157416';
echo '=================> Gep_Fddpg Withnoise: Trial 587, Buffer Size 700, Buffer Id 7, Seed 587, 2018-02-20 09:47:46.157416' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 587 --seed 587 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_7 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 588, Buffer Size 700, Buffer Id 8, Seed 588, 2018-02-20 09:47:46.157425';
echo '=================> Gep_Fddpg Withnoise: Trial 588, Buffer Size 700, Buffer Id 8, Seed 588, 2018-02-20 09:47:46.157425' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 588 --seed 588 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_8 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep_Fddpg Withnoise: Trial 589, Buffer Size 700, Buffer Id 9, Seed 589, 2018-02-20 09:47:46.157435';
echo '=================> Gep_Fddpg Withnoise: Trial 589, Buffer Size 700, Buffer Id 9, Seed 589, 2018-02-20 09:47:46.157435' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 589 --seed 589 --noise_type ou_0.0 --study GEP_FDDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_700_9 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait