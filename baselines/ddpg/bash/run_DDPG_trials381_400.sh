#!/bin/sh
#SBATCH -n1
#SBATCH -p long_sirocco
#SBATCH -t 60:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_DDPG_trials381_400.sh.err
#SBATCH -o run_DDPG_trials381_400.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Ddpg Withnoise: Trial 381, Seed 381, 2018-02-07 13:50:31.025083';
echo '=================> Ddpg Withnoise: Trial 381, Seed 381, 2018-02-07 13:50:31.025083' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 381 --study DDPG --noise_type ou_0.0 --seed 381 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 382, Seed 382, 2018-02-07 13:50:31.025111';
echo '=================> Ddpg Withnoise: Trial 382, Seed 382, 2018-02-07 13:50:31.025111' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 382 --study DDPG --noise_type ou_0.0 --seed 382 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 383, Seed 383, 2018-02-07 13:50:31.025121';
echo '=================> Ddpg Withnoise: Trial 383, Seed 383, 2018-02-07 13:50:31.025121' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 383 --study DDPG --noise_type ou_0.0 --seed 383 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 384, Seed 384, 2018-02-07 13:50:31.025130';
echo '=================> Ddpg Withnoise: Trial 384, Seed 384, 2018-02-07 13:50:31.025130' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 384 --study DDPG --noise_type ou_0.0 --seed 384 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 385, Seed 385, 2018-02-07 13:50:31.025138';
echo '=================> Ddpg Withnoise: Trial 385, Seed 385, 2018-02-07 13:50:31.025138' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 385 --study DDPG --noise_type ou_0.0 --seed 385 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 386, Seed 386, 2018-02-07 13:50:31.025146';
echo '=================> Ddpg Withnoise: Trial 386, Seed 386, 2018-02-07 13:50:31.025146' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 386 --study DDPG --noise_type ou_0.0 --seed 386 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 387, Seed 387, 2018-02-07 13:50:31.025154';
echo '=================> Ddpg Withnoise: Trial 387, Seed 387, 2018-02-07 13:50:31.025154' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 387 --study DDPG --noise_type ou_0.0 --seed 387 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 388, Seed 388, 2018-02-07 13:50:31.025162';
echo '=================> Ddpg Withnoise: Trial 388, Seed 388, 2018-02-07 13:50:31.025162' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 388 --study DDPG --noise_type ou_0.0 --seed 388 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 389, Seed 389, 2018-02-07 13:50:31.025170';
echo '=================> Ddpg Withnoise: Trial 389, Seed 389, 2018-02-07 13:50:31.025170' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 389 --study DDPG --noise_type ou_0.0 --seed 389 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 390, Seed 390, 2018-02-07 13:50:31.025179';
echo '=================> Ddpg Withnoise: Trial 390, Seed 390, 2018-02-07 13:50:31.025179' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 390 --study DDPG --noise_type ou_0.0 --seed 390 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 391, Seed 391, 2018-02-07 13:50:31.025188';
echo '=================> Ddpg Withnoise: Trial 391, Seed 391, 2018-02-07 13:50:31.025188' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 391 --study DDPG --noise_type ou_0.0 --seed 391 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 392, Seed 392, 2018-02-07 13:50:31.025197';
echo '=================> Ddpg Withnoise: Trial 392, Seed 392, 2018-02-07 13:50:31.025197' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 392 --study DDPG --noise_type ou_0.0 --seed 392 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 393, Seed 393, 2018-02-07 13:50:31.025206';
echo '=================> Ddpg Withnoise: Trial 393, Seed 393, 2018-02-07 13:50:31.025206' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 393 --study DDPG --noise_type ou_0.0 --seed 393 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 394, Seed 394, 2018-02-07 13:50:31.025215';
echo '=================> Ddpg Withnoise: Trial 394, Seed 394, 2018-02-07 13:50:31.025215' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 394 --study DDPG --noise_type ou_0.0 --seed 394 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 395, Seed 395, 2018-02-07 13:50:31.025225';
echo '=================> Ddpg Withnoise: Trial 395, Seed 395, 2018-02-07 13:50:31.025225' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 395 --study DDPG --noise_type ou_0.0 --seed 395 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 396, Seed 396, 2018-02-07 13:50:31.025235';
echo '=================> Ddpg Withnoise: Trial 396, Seed 396, 2018-02-07 13:50:31.025235' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 396 --study DDPG --noise_type ou_0.0 --seed 396 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 397, Seed 397, 2018-02-07 13:50:31.025245';
echo '=================> Ddpg Withnoise: Trial 397, Seed 397, 2018-02-07 13:50:31.025245' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 397 --study DDPG --noise_type ou_0.0 --seed 397 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 398, Seed 398, 2018-02-07 13:50:31.025253';
echo '=================> Ddpg Withnoise: Trial 398, Seed 398, 2018-02-07 13:50:31.025253' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 398 --study DDPG --noise_type ou_0.0 --seed 398 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 399, Seed 399, 2018-02-07 13:50:31.025261';
echo '=================> Ddpg Withnoise: Trial 399, Seed 399, 2018-02-07 13:50:31.025261' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 399 --study DDPG --noise_type ou_0.0 --seed 399 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withnoise: Trial 400, Seed 400, 2018-02-07 13:50:31.025266';
echo '=================> Ddpg Withnoise: Trial 400, Seed 400, 2018-02-07 13:50:31.025266' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 400 --study DDPG --noise_type ou_0.0 --seed 400 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait