#!/bin/sh
#SBATCH -n1
#SBATCH -p long_sirocco
#SBATCH -t 60:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_GEP_DDPG_trials111_120.sh.err
#SBATCH -o run_GEP_DDPG_trials111_120.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withparamnoise: Trial 111, Buffer Size 500, Buffer Id 51, Seed 111, 2018-02-01 18:05:35.723792';
echo '=================> Gep-Ddpg Withparamnoise: Trial 111, Buffer Size 500, Buffer Id 51, Seed 111, 2018-02-01 18:05:35.723792' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 111 --seed 111  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_51 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 112, Buffer Size 500, Buffer Id 52, Seed 112, 2018-02-01 18:05:35.723824';
echo '=================> Gep-Ddpg Withparamnoise: Trial 112, Buffer Size 500, Buffer Id 52, Seed 112, 2018-02-01 18:05:35.723824' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 112 --seed 112  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_52 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 113, Buffer Size 500, Buffer Id 53, Seed 113, 2018-02-01 18:05:35.723837';
echo '=================> Gep-Ddpg Withparamnoise: Trial 113, Buffer Size 500, Buffer Id 53, Seed 113, 2018-02-01 18:05:35.723837' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 113 --seed 113  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_53 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 114, Buffer Size 500, Buffer Id 54, Seed 114, 2018-02-01 18:05:35.723847';
echo '=================> Gep-Ddpg Withparamnoise: Trial 114, Buffer Size 500, Buffer Id 54, Seed 114, 2018-02-01 18:05:35.723847' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 114 --seed 114  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_54 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 115, Buffer Size 500, Buffer Id 55, Seed 115, 2018-02-01 18:05:35.723856';
echo '=================> Gep-Ddpg Withparamnoise: Trial 115, Buffer Size 500, Buffer Id 55, Seed 115, 2018-02-01 18:05:35.723856' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 115 --seed 115  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_55 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 116, Buffer Size 500, Buffer Id 56, Seed 116, 2018-02-01 18:05:35.723866';
echo '=================> Gep-Ddpg Withparamnoise: Trial 116, Buffer Size 500, Buffer Id 56, Seed 116, 2018-02-01 18:05:35.723866' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 116 --seed 116  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_56 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 117, Buffer Size 500, Buffer Id 57, Seed 117, 2018-02-01 18:05:35.723875';
echo '=================> Gep-Ddpg Withparamnoise: Trial 117, Buffer Size 500, Buffer Id 57, Seed 117, 2018-02-01 18:05:35.723875' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 117 --seed 117  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_57 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 118, Buffer Size 500, Buffer Id 58, Seed 118, 2018-02-01 18:05:35.723884';
echo '=================> Gep-Ddpg Withparamnoise: Trial 118, Buffer Size 500, Buffer Id 58, Seed 118, 2018-02-01 18:05:35.723884' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 118 --seed 118  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_58 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 119, Buffer Size 500, Buffer Id 59, Seed 119, 2018-02-01 18:05:35.723893';
echo '=================> Gep-Ddpg Withparamnoise: Trial 119, Buffer Size 500, Buffer Id 59, Seed 119, 2018-02-01 18:05:35.723893' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 119 --seed 119  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_59 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withparamnoise: Trial 120, Buffer Size 500, Buffer Id 60, Seed 120, 2018-02-01 18:05:35.723902';
echo '=================> Gep-Ddpg Withparamnoise: Trial 120, Buffer Size 500, Buffer Id 60, Seed 120, 2018-02-01 18:05:35.723902' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 120 --seed 120  --noise_type adaptive-param_0.2 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_60 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait