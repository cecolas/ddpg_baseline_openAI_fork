#!/usr/bin/python
# -*- coding: utf-8 -*-

from itertools import *
import datetime

# PATH_TO_RESULTS = '/mnt/shared/cedric/DDPG/'
# PATH_TO_INTERPRETER = "/usr/bin/python3"
PATH_TO_RESULTS = '/projets/flowers/cedric/ddpg_baseline_openAI_fork/results/'

PATH_TO_INTERPRETER = "/home/ccolas/virtual_envs/py3.5/bin/python"

trial_id = list(range(191,201))
frozen = False
study = 'GEP_DDPG'
ddpg_noise = 'withParamNoise'
size_buff = 500
buffer_folder = '/projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/'

filename = 'run_GEP_DDPG_trials' + str(trial_id[0])+'_'+str(trial_id[-1])+'.sh'
with open(filename, 'w') as f:
    f.write('#!/bin/sh\n')
    f.write('#SBATCH -n1\n')
    f.write('#SBATCH -p long_sirocco\n')
    f.write('#SBATCH -t 60:00:00\n')
    f.write('#SBATCH --gres=gpu:1\n')
    f.write('#SBATCH -e ' + filename + '.err\n')
    f.write('#SBATCH -o ' + filename + '.out\n')
    f.write('rm log.txt; \n')
    f.write("export EXP_INTERP='%s' ;\n" % PATH_TO_INTERPRETER)
    f.write('ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"\n')
    f.write('agpu=0\n')
    for i,buff_id in enumerate(list(range(71,81))):
        # buffer_name = 'Cheetah_model_%s_%s_buffer' %(str(size_buff), str(buff_id))
        buffer_name = 'Cheetah_buffer_%s_%s' % (str(size_buff), str(buff_id))
        print('buffer_name :',buffer_name)
        t_id = trial_id[i]
        name = ("GEP-DDPG %s: trial %s, buffer size %s, buffer id %s, seed %s, %s" % (ddpg_noise,str(t_id),str(size_buff),str(buff_id), str(t_id), str(datetime.datetime.now()))).title()
        f.write("echo '=================> %s';\n" % name)
        f.write("echo '=================> %s' >> log.txt;\n" % name)
        f.write("export CUDA_VISIBLE_DEVICES=$agpu\n")
        f.write("agpu=$(((agpu+1)%ngpu))\n")
        if ddpg_noise == 'withNoise':
            f.write("$EXP_INTERP main.py --trial_id %i --seed %i --noise_type %s --study %s --buffer_location %s --data_path %s & \n"
                    % (t_id, t_id, 'ou_0.3', study,  buffer_folder+buffer_name, PATH_TO_RESULTS))
        elif ddpg_noise == 'withoutNoise':
            f.write("$EXP_INTERP main.py --trial_id %i --seed %i  --noise_type %s --study %s --buffer_location %s --data_path %s & \n"
                % (t_id, t_id, 'none', study, buffer_folder + buffer_name, PATH_TO_RESULTS))
        elif ddpg_noise == 'withParamNoise':
            f.write("$EXP_INTERP main.py --trial_id %i --seed %i  --noise_type %s --study %s --buffer_location %s --data_path %s & \n"
                % (t_id, t_id, 'adaptive-param_0.2', study, buffer_folder + buffer_name, PATH_TO_RESULTS))
        else:
            raise NotImplementedError
    f.write('wait')
