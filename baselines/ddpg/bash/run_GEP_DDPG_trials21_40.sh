#!/bin/sh
#SBATCH -n1
#SBATCH -p long_sirocco
#SBATCH -t 30:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_GEP_DDPG_trials21_40.sh.err
#SBATCH -o run_GEP_DDPG_trials21_40.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withnoise: Trial 21, Buffer Size 500, Buffer Id 1, Seed 21, 2018-01-25 17:21:26.618665';
echo '=================> Gep-Ddpg Withnoise: Trial 21, Buffer Size 500, Buffer Id 1, Seed 21, 2018-01-25 17:21:26.618665' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 21 --seed 21 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_1_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 22, Buffer Size 500, Buffer Id 2, Seed 22, 2018-01-25 17:21:26.618701';
echo '=================> Gep-Ddpg Withnoise: Trial 22, Buffer Size 500, Buffer Id 2, Seed 22, 2018-01-25 17:21:26.618701' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 22 --seed 22 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_2_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 23, Buffer Size 500, Buffer Id 3, Seed 23, 2018-01-25 17:21:26.618713';
echo '=================> Gep-Ddpg Withnoise: Trial 23, Buffer Size 500, Buffer Id 3, Seed 23, 2018-01-25 17:21:26.618713' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 23 --seed 23 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_3_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 24, Buffer Size 500, Buffer Id 4, Seed 24, 2018-01-25 17:21:26.618734';
echo '=================> Gep-Ddpg Withnoise: Trial 24, Buffer Size 500, Buffer Id 4, Seed 24, 2018-01-25 17:21:26.618734' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 24 --seed 24 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_4_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 25, Buffer Size 500, Buffer Id 5, Seed 25, 2018-01-25 17:21:26.618743';
echo '=================> Gep-Ddpg Withnoise: Trial 25, Buffer Size 500, Buffer Id 5, Seed 25, 2018-01-25 17:21:26.618743' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 25 --seed 25 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_5_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 26, Buffer Size 500, Buffer Id 6, Seed 26, 2018-01-25 17:21:26.618752';
echo '=================> Gep-Ddpg Withnoise: Trial 26, Buffer Size 500, Buffer Id 6, Seed 26, 2018-01-25 17:21:26.618752' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 26 --seed 26 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_6_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 27, Buffer Size 500, Buffer Id 7, Seed 27, 2018-01-25 17:21:26.618771';
echo '=================> Gep-Ddpg Withnoise: Trial 27, Buffer Size 500, Buffer Id 7, Seed 27, 2018-01-25 17:21:26.618771' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 27 --seed 27 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_7_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 28, Buffer Size 500, Buffer Id 8, Seed 28, 2018-01-25 17:21:26.618782';
echo '=================> Gep-Ddpg Withnoise: Trial 28, Buffer Size 500, Buffer Id 8, Seed 28, 2018-01-25 17:21:26.618782' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 28 --seed 28 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_8_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 29, Buffer Size 500, Buffer Id 9, Seed 29, 2018-01-25 17:21:26.618791';
echo '=================> Gep-Ddpg Withnoise: Trial 29, Buffer Size 500, Buffer Id 9, Seed 29, 2018-01-25 17:21:26.618791' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 29 --seed 29 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_9_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 30, Buffer Size 500, Buffer Id 10, Seed 30, 2018-01-25 17:21:26.618799';
echo '=================> Gep-Ddpg Withnoise: Trial 30, Buffer Size 500, Buffer Id 10, Seed 30, 2018-01-25 17:21:26.618799' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 30 --seed 30 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_10_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 31, Buffer Size 500, Buffer Id 11, Seed 31, 2018-01-25 17:21:26.618820';
echo '=================> Gep-Ddpg Withnoise: Trial 31, Buffer Size 500, Buffer Id 11, Seed 31, 2018-01-25 17:21:26.618820' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 31 --seed 31 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_11_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 32, Buffer Size 500, Buffer Id 12, Seed 32, 2018-01-25 17:21:26.618829';
echo '=================> Gep-Ddpg Withnoise: Trial 32, Buffer Size 500, Buffer Id 12, Seed 32, 2018-01-25 17:21:26.618829' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 32 --seed 32 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_12_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 33, Buffer Size 500, Buffer Id 13, Seed 33, 2018-01-25 17:21:26.618837';
echo '=================> Gep-Ddpg Withnoise: Trial 33, Buffer Size 500, Buffer Id 13, Seed 33, 2018-01-25 17:21:26.618837' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 33 --seed 33 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_13_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 34, Buffer Size 500, Buffer Id 14, Seed 34, 2018-01-25 17:21:26.618857';
echo '=================> Gep-Ddpg Withnoise: Trial 34, Buffer Size 500, Buffer Id 14, Seed 34, 2018-01-25 17:21:26.618857' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 34 --seed 34 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_14_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 35, Buffer Size 500, Buffer Id 15, Seed 35, 2018-01-25 17:21:26.618900';
echo '=================> Gep-Ddpg Withnoise: Trial 35, Buffer Size 500, Buffer Id 15, Seed 35, 2018-01-25 17:21:26.618900' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 35 --seed 35 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_15_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 36, Buffer Size 500, Buffer Id 16, Seed 36, 2018-01-25 17:21:26.618914';
echo '=================> Gep-Ddpg Withnoise: Trial 36, Buffer Size 500, Buffer Id 16, Seed 36, 2018-01-25 17:21:26.618914' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 36 --seed 36 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_16_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 37, Buffer Size 500, Buffer Id 17, Seed 37, 2018-01-25 17:21:26.618941';
echo '=================> Gep-Ddpg Withnoise: Trial 37, Buffer Size 500, Buffer Id 17, Seed 37, 2018-01-25 17:21:26.618941' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 37 --seed 37 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_17_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 38, Buffer Size 500, Buffer Id 18, Seed 38, 2018-01-25 17:21:26.618957';
echo '=================> Gep-Ddpg Withnoise: Trial 38, Buffer Size 500, Buffer Id 18, Seed 38, 2018-01-25 17:21:26.618957' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 38 --seed 38 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_18_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 39, Buffer Size 500, Buffer Id 19, Seed 39, 2018-01-25 17:21:26.618984';
echo '=================> Gep-Ddpg Withnoise: Trial 39, Buffer Size 500, Buffer Id 19, Seed 39, 2018-01-25 17:21:26.618984' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 39 --seed 39 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_19_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 40, Buffer Size 500, Buffer Id 20, Seed 40, 2018-01-25 17:21:26.618996';
echo '=================> Gep-Ddpg Withnoise: Trial 40, Buffer Size 500, Buffer Id 20, Seed 40, 2018-01-25 17:21:26.618996' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 40 --seed 40 --noise_type ou_0.3 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_model_500_20_buffer --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait