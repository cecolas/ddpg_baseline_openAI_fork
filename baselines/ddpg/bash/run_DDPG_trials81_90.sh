#!/bin/sh
#SBATCH -n1
#SBATCH -p long_sirocco
#SBATCH -t 60:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_DDPG_trials81_90.sh.err
#SBATCH -o run_DDPG_trials81_90.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Ddpg Withparamnoise: Trial 81, Seed 81, 2018-01-30 16:15:28.136008';
echo '=================> Ddpg Withparamnoise: Trial 81, Seed 81, 2018-01-30 16:15:28.136008' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 81 --study DDPG --noise_type adaptive-param_0.2 --seed 81 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 82, Seed 82, 2018-01-30 16:15:28.136063';
echo '=================> Ddpg Withparamnoise: Trial 82, Seed 82, 2018-01-30 16:15:28.136063' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 82 --study DDPG --noise_type adaptive-param_0.2 --seed 82 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 83, Seed 83, 2018-01-30 16:15:28.136079';
echo '=================> Ddpg Withparamnoise: Trial 83, Seed 83, 2018-01-30 16:15:28.136079' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 83 --study DDPG --noise_type adaptive-param_0.2 --seed 83 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 84, Seed 84, 2018-01-30 16:15:28.136090';
echo '=================> Ddpg Withparamnoise: Trial 84, Seed 84, 2018-01-30 16:15:28.136090' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 84 --study DDPG --noise_type adaptive-param_0.2 --seed 84 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 85, Seed 85, 2018-01-30 16:15:28.136101';
echo '=================> Ddpg Withparamnoise: Trial 85, Seed 85, 2018-01-30 16:15:28.136101' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 85 --study DDPG --noise_type adaptive-param_0.2 --seed 85 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 86, Seed 86, 2018-01-30 16:15:28.136113';
echo '=================> Ddpg Withparamnoise: Trial 86, Seed 86, 2018-01-30 16:15:28.136113' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 86 --study DDPG --noise_type adaptive-param_0.2 --seed 86 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 87, Seed 87, 2018-01-30 16:15:28.136131';
echo '=================> Ddpg Withparamnoise: Trial 87, Seed 87, 2018-01-30 16:15:28.136131' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 87 --study DDPG --noise_type adaptive-param_0.2 --seed 87 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 88, Seed 88, 2018-01-30 16:15:28.136146';
echo '=================> Ddpg Withparamnoise: Trial 88, Seed 88, 2018-01-30 16:15:28.136146' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 88 --study DDPG --noise_type adaptive-param_0.2 --seed 88 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 89, Seed 89, 2018-01-30 16:15:28.136158';
echo '=================> Ddpg Withparamnoise: Trial 89, Seed 89, 2018-01-30 16:15:28.136158' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 89 --study DDPG --noise_type adaptive-param_0.2 --seed 89 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Ddpg Withparamnoise: Trial 90, Seed 90, 2018-01-30 16:15:28.136170';
echo '=================> Ddpg Withparamnoise: Trial 90, Seed 90, 2018-01-30 16:15:28.136170' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 90 --study DDPG --noise_type adaptive-param_0.2 --seed 90 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait