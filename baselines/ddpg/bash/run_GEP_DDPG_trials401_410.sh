#!/bin/sh
#SBATCH -n20
#SBATCH -p long_sirocco
#SBATCH -t 30:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_GEP_DDPG_trials401_410.sh.err
#SBATCH -o run_GEP_DDPG_trials401_410.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Gep-Ddpg Withnoise: Trial 401, Buffer Size 500, Buffer Id 61, Seed 401, 2018-02-07 13:52:08.388090';
echo '=================> Gep-Ddpg Withnoise: Trial 401, Buffer Size 500, Buffer Id 61, Seed 401, 2018-02-07 13:52:08.388090' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 401 --seed 401 --noise_type ou_0.0 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_61 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 402, Buffer Size 500, Buffer Id 62, Seed 402, 2018-02-07 13:52:08.388126';
echo '=================> Gep-Ddpg Withnoise: Trial 402, Buffer Size 500, Buffer Id 62, Seed 402, 2018-02-07 13:52:08.388126' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 402 --seed 402 --noise_type ou_0.0 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_62 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 403, Buffer Size 500, Buffer Id 63, Seed 403, 2018-02-07 13:52:08.388149';
echo '=================> Gep-Ddpg Withnoise: Trial 403, Buffer Size 500, Buffer Id 63, Seed 403, 2018-02-07 13:52:08.388149' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 403 --seed 403 --noise_type ou_0.0 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_63 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 404, Buffer Size 500, Buffer Id 64, Seed 404, 2018-02-07 13:52:08.388170';
echo '=================> Gep-Ddpg Withnoise: Trial 404, Buffer Size 500, Buffer Id 64, Seed 404, 2018-02-07 13:52:08.388170' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 404 --seed 404 --noise_type ou_0.0 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_64 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 405, Buffer Size 500, Buffer Id 65, Seed 405, 2018-02-07 13:52:08.388190';
echo '=================> Gep-Ddpg Withnoise: Trial 405, Buffer Size 500, Buffer Id 65, Seed 405, 2018-02-07 13:52:08.388190' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 405 --seed 405 --noise_type ou_0.0 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_65 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 406, Buffer Size 500, Buffer Id 66, Seed 406, 2018-02-07 13:52:08.388208';
echo '=================> Gep-Ddpg Withnoise: Trial 406, Buffer Size 500, Buffer Id 66, Seed 406, 2018-02-07 13:52:08.388208' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 406 --seed 406 --noise_type ou_0.0 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_66 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 407, Buffer Size 500, Buffer Id 67, Seed 407, 2018-02-07 13:52:08.388226';
echo '=================> Gep-Ddpg Withnoise: Trial 407, Buffer Size 500, Buffer Id 67, Seed 407, 2018-02-07 13:52:08.388226' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 407 --seed 407 --noise_type ou_0.0 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_67 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 408, Buffer Size 500, Buffer Id 68, Seed 408, 2018-02-07 13:52:08.388245';
echo '=================> Gep-Ddpg Withnoise: Trial 408, Buffer Size 500, Buffer Id 68, Seed 408, 2018-02-07 13:52:08.388245' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 408 --seed 408 --noise_type ou_0.0 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_68 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 409, Buffer Size 500, Buffer Id 69, Seed 409, 2018-02-07 13:52:08.388264';
echo '=================> Gep-Ddpg Withnoise: Trial 409, Buffer Size 500, Buffer Id 69, Seed 409, 2018-02-07 13:52:08.388264' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 409 --seed 409 --noise_type ou_0.0 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_69 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
echo '=================> Gep-Ddpg Withnoise: Trial 410, Buffer Size 500, Buffer Id 70, Seed 410, 2018-02-07 13:52:08.388283';
echo '=================> Gep-Ddpg Withnoise: Trial 410, Buffer Size 500, Buffer Id 70, Seed 410, 2018-02-07 13:52:08.388283' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 410 --seed 410 --noise_type ou_0.0 --study GEP_DDPG --buffer_location /projets/flowers/cedric/ddpg_baseline_openAI_fork/buffers/Cheetah_buffer_500_70 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/results/ & 
wait