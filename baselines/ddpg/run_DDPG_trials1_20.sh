#!/bin/sh
#SBATCH -n1
#SBATCH -p long_sirocco
#SBATCH -t 20:00:00
#SBATCH --gres=gpu:1
#SBATCH -e run_DDPG_trials1_20.sh.err
#SBATCH -o run_DDPG_trials1_20.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
echo '=================> Ddpg Withnoise: Trial 1, Seed 1, 2018-01-24 20:43:09.856897';
echo '=================> Ddpg Withnoise: Trial 1, Seed 1, 2018-01-24 20:43:09.856897' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 1 --study DDPG --seed 1 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/data_study_GEP/ & 
echo '=================> Ddpg Withnoise: Trial 2, Seed 2, 2018-01-24 20:43:09.856920';
echo '=================> Ddpg Withnoise: Trial 2, Seed 2, 2018-01-24 20:43:09.856920' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 2 --study DDPG --seed 2 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/data_study_GEP/ & 
echo '=================> Ddpg Withnoise: Trial 3, Seed 3, 2018-01-24 20:43:09.856931';
echo '=================> Ddpg Withnoise: Trial 3, Seed 3, 2018-01-24 20:43:09.856931' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 3 --study DDPG --seed 3 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/data_study_GEP/ & 
echo '=================> Ddpg Withnoise: Trial 4, Seed 4, 2018-01-24 20:43:09.856941';
echo '=================> Ddpg Withnoise: Trial 4, Seed 4, 2018-01-24 20:43:09.856941' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 4 --study DDPG --seed 4 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/data_study_GEP/ & 
echo '=================> Ddpg Withnoise: Trial 5, Seed 5, 2018-01-24 20:43:09.856950';
echo '=================> Ddpg Withnoise: Trial 5, Seed 5, 2018-01-24 20:43:09.856950' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 5 --study DDPG --seed 5 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/data_study_GEP/ & 
echo '=================> Ddpg Withnoise: Trial 6, Seed 6, 2018-01-24 20:43:09.856965';
echo '=================> Ddpg Withnoise: Trial 6, Seed 6, 2018-01-24 20:43:09.856965' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 6 --study DDPG --seed 6 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/data_study_GEP/ & 
echo '=================> Ddpg Withnoise: Trial 7, Seed 7, 2018-01-24 20:43:09.856975';
echo '=================> Ddpg Withnoise: Trial 7, Seed 7, 2018-01-24 20:43:09.856975' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 7 --study DDPG --seed 7 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/data_study_GEP/ & 
echo '=================> Ddpg Withnoise: Trial 8, Seed 8, 2018-01-24 20:43:09.856990';
echo '=================> Ddpg Withnoise: Trial 8, Seed 8, 2018-01-24 20:43:09.856990' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 8 --study DDPG --seed 8 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/data_study_GEP/ & 
echo '=================> Ddpg Withnoise: Trial 9, Seed 9, 2018-01-24 20:43:09.856996';
echo '=================> Ddpg Withnoise: Trial 9, Seed 9, 2018-01-24 20:43:09.856996' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 9 --study DDPG --seed 9 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/data_study_GEP/ & 
echo '=================> Ddpg Withnoise: Trial 10, Seed 10, 2018-01-24 20:43:09.857001';
echo '=================> Ddpg Withnoise: Trial 10, Seed 10, 2018-01-24 20:43:09.857001' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 10 --study DDPG --seed 10 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/data_study_GEP/ & 
echo '=================> Ddpg Withnoise: Trial 11, Seed 11, 2018-01-24 20:43:09.857007';
echo '=================> Ddpg Withnoise: Trial 11, Seed 11, 2018-01-24 20:43:09.857007' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 11 --study DDPG --seed 11 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/data_study_GEP/ & 
echo '=================> Ddpg Withnoise: Trial 12, Seed 12, 2018-01-24 20:43:09.857012';
echo '=================> Ddpg Withnoise: Trial 12, Seed 12, 2018-01-24 20:43:09.857012' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 12 --study DDPG --seed 12 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/data_study_GEP/ & 
echo '=================> Ddpg Withnoise: Trial 13, Seed 13, 2018-01-24 20:43:09.857017';
echo '=================> Ddpg Withnoise: Trial 13, Seed 13, 2018-01-24 20:43:09.857017' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 13 --study DDPG --seed 13 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/data_study_GEP/ & 
echo '=================> Ddpg Withnoise: Trial 14, Seed 14, 2018-01-24 20:43:09.857023';
echo '=================> Ddpg Withnoise: Trial 14, Seed 14, 2018-01-24 20:43:09.857023' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 14 --study DDPG --seed 14 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/data_study_GEP/ & 
echo '=================> Ddpg Withnoise: Trial 15, Seed 15, 2018-01-24 20:43:09.857028';
echo '=================> Ddpg Withnoise: Trial 15, Seed 15, 2018-01-24 20:43:09.857028' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 15 --study DDPG --seed 15 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/data_study_GEP/ & 
echo '=================> Ddpg Withnoise: Trial 16, Seed 16, 2018-01-24 20:43:09.857033';
echo '=================> Ddpg Withnoise: Trial 16, Seed 16, 2018-01-24 20:43:09.857033' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 16 --study DDPG --seed 16 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/data_study_GEP/ & 
echo '=================> Ddpg Withnoise: Trial 17, Seed 17, 2018-01-24 20:43:09.857038';
echo '=================> Ddpg Withnoise: Trial 17, Seed 17, 2018-01-24 20:43:09.857038' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 17 --study DDPG --seed 17 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/data_study_GEP/ & 
echo '=================> Ddpg Withnoise: Trial 18, Seed 18, 2018-01-24 20:43:09.857043';
echo '=================> Ddpg Withnoise: Trial 18, Seed 18, 2018-01-24 20:43:09.857043' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 18 --study DDPG --seed 18 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/data_study_GEP/ & 
echo '=================> Ddpg Withnoise: Trial 19, Seed 19, 2018-01-24 20:43:09.857049';
echo '=================> Ddpg Withnoise: Trial 19, Seed 19, 2018-01-24 20:43:09.857049' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 19 --study DDPG --seed 19 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/data_study_GEP/ & 
echo '=================> Ddpg Withnoise: Trial 20, Seed 20, 2018-01-24 20:43:09.857054';
echo '=================> Ddpg Withnoise: Trial 20, Seed 20, 2018-01-24 20:43:09.857054' >> log.txt;
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP main.py --trial_id 20 --study DDPG --seed 20 --data_path /projets/flowers/cedric/ddpg_baseline_openAI_fork/data_study_GEP/ & 
wait