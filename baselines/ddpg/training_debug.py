from baselines.ddpg.ddpg import DDPG
import os
from os import path
import shutil
import time
import pickle
from collections import deque
from baselines import logger
import baselines.common.tf_util as U
from baselines.ddpg.memory import load_from_cedric
from baselines import logger
import numpy as np
import tensorflow as tf

def run_agent(env, nb_epochs, nb_epoch_cycles, render_eval, reward_scale, render, param_noise, actor, critic,
    normalize_returns, normalize_observations, critic_l2_reg, actor_lr, critic_lr, action_noise,
    popart, gamma, clip_norm, nb_train_steps, nb_rollout_steps, nb_eval_steps, batch_size, memory, study,
    buffer_location, trial_id, data_path, nb_eval_episodes, tau=0.01, eval_env=None, param_noise_adaption_interval=50, **kwargs):
    rank = 0

    assert (np.abs(env.action_space.low) == env.action_space.high).all()  # we assume symmetric actions.
    max_action = env.action_space.high
    logger.info('scaling actions by {} before executing in env'.format(max_action))
    agent = DDPG(actor, critic, memory, env.observation_space.shape, env.action_space.shape,
                 gamma=gamma, tau=tau, normalize_returns=normalize_returns, normalize_observations=normalize_observations,
                 batch_size=batch_size, action_noise=action_noise, param_noise=param_noise, critic_l2_reg=critic_l2_reg,
                 actor_lr=actor_lr, critic_lr=critic_lr, enable_popart=popart, clip_norm=clip_norm,
                 reward_scale=reward_scale)
    logger.info('Using agent with the following configuration:')
    logger.info(str(agent.__dict__.items()))

    # Set up logging stuff only for a single worker.
    if rank == 0:
        saver = tf.train.Saver()
    else:
        saver = None

    episode = 0
    best_score = -1000
    eval_episode_rewards_history = deque(maxlen=100)
    episode_rewards_history = deque(maxlen=100)
    saver_best = tf.train.Saver(max_to_keep=1)

    with U.single_threaded_session() as sess:
        # Prepare everything.
        agent.initialize(sess)
        sess.graph.finalize()

        # load buffer if necessary
        if study == 'GEP_DDPG' or study == 'GEP_FDDPG':
            assert buffer_location != '', 'study is ' + study + ', a buffer location should be provided.'
            # fill replay buffer
            buffer_gep = load_from_cedric(filename=buffer_location)
            size_buffer = len(buffer_gep[0])
            for i in range(size_buffer):
                agent.store_transition(np.array(buffer_gep[0][i]), np.array(buffer_gep[1][i]),
                                       buffer_gep[2][i][0], np.array(buffer_gep[3][i]), buffer_gep[4][i])
            print('Buffer of size ' + str(size_buffer) + ' has been loaded')

        agent.reset()
        obs = env.reset()
        if eval_env is not None:
            eval_obs = eval_env.reset()
        done = False
        episode_reward = 0.
        episode_step = 0
        episodes = 0
        t = 0

        epoch = 0
        start_time = time.time()

        epoch_episode_rewards = []
        epoch_episode_steps = []
        epoch_episode_eval_rewards = []
        epoch_episode_eval_steps = []
        epoch_start_time = time.time()
        epoch_actions = []
        epoch_qs = []
        epoch_episodes = 0

        for epoch in range(nb_epochs):
            for cycle in range(nb_epoch_cycles):
                # Perform rollouts.
                if study != 'GEP_FDDPG':
                    agent, env, episode_reward, episode_step, epoch_actions, epoch_qs, epoch_episode_rewards, episode_rewards_history, \
                    epoch_episode_steps, epoch_episodes, episodes, t = rollout(nb_rollout_steps, agent, obs, rank, env, max_action,
                                                                               episode_reward, episode_step, epoch_actions,
                                                                               epoch_qs,epoch_episode_rewards, episode_rewards_history,
                                                                               epoch_episode_steps, epoch_episodes, episodes, render, t
                                                                               )
                # Train.
                agent, epoch_actor_losses, epoch_critic_losses, epoch_adaptive_distances = train(nb_train_steps, memory,
                                                                                                 batch_size, t,
                                                                                                 param_noise_adaption_interval, agent
                                                                                                 )
            # Evaluate.
            agent, eval_episode_rewards, eval_qs, best_score, eval_obs, eval_env, \
            eval_episode_rewards_history = evaluate(agent, eval_obs, max_action, eval_env, render_eval, sess, t,
                                                    eval_episode_rewards_history, best_score, data_path, saver_best, nb_eval_episodes
                                                    )

            # update decreasing_ou if necessary
            try:
                action_noise.adapt()
                print(action_noise.sigma)
            except:
                pass

            # Log stats.
            epoch_train_duration = time.time() - epoch_start_time
            duration = time.time() - start_time
            stats = agent.get_stats()
            combined_stats = {}
            for key in sorted(stats.keys()):
                combined_stats[key] = np.mean(stats[key])

            # Added statistics

            # Rollout statistics.
            combined_stats['rollout/return'] = np.mean(epoch_episode_rewards)
            combined_stats['rollout/return_history'] = np.mean(np.mean(episode_rewards_history))
            combined_stats['rollout/episode_steps'] = np.mean(epoch_episode_steps)
            combined_stats['rollout/episodes'] = np.sum(epoch_episodes)
            combined_stats['rollout/actions_mean'] = np.mean(epoch_actions)
            combined_stats['rollout/actions_std'] = np.std(epoch_actions)
            combined_stats['rollout/Q_mean'] = np.mean(epoch_qs)

            # Train statistics.
            combined_stats['train/loss_actor'] = np.mean(epoch_actor_losses)
            combined_stats['train/loss_critic'] = np.mean(epoch_critic_losses)
            combined_stats['train/param_noise_distance'] = np.mean(epoch_adaptive_distances)

            # Evaluation statistics.
            if eval_env is not None:
                combined_stats['eval/return'] = np.mean(eval_episode_rewards)
                combined_stats['eval/return_history'] = np.mean(np.mean(eval_episode_rewards_history))
                combined_stats['eval/Q'] = np.mean(eval_qs)
                combined_stats['eval/episodes'] = np.mean(len(eval_episode_rewards))

            # Total statistics.
            combined_stats['total/duration'] = np.mean(duration)
            combined_stats['total/steps_per_second'] = np.mean(float(t) / float(duration))
            combined_stats['total/episodes'] = np.mean(episodes)
            combined_stats['total/epochs'] = epoch + 1
            combined_stats['total/steps'] = t

            for key in sorted(combined_stats.keys()):
                logger.record_tabular(key, combined_stats[key])
            logger.dump_tabular()
            logger.info('')
            logdir = logger.get_dir()
            if rank == 0 and logdir:
                if hasattr(env, 'get_state'):
                    with open(os.path.join(logdir, 'env_state.pkl'), 'wb') as f:
                        pickle.dump(env.get_state(), f)
                if eval_env and hasattr(eval_env, 'get_state'):
                    with open(os.path.join(logdir, 'eval_env_state.pkl'), 'wb') as f:
                        pickle.dump(eval_env.get_state(), f)

    x = 0
    while True:
        x += 1
        if (x % 1000000 == 0):
            print(x)



def rollout(nb_rollout_steps, agent, obs, rank, env, max_action, episode_reward,
            episode_step, epoch_actions, epoch_qs,epoch_episode_rewards, episode_rewards_history,
            epoch_episode_steps, epoch_episodes, episodes, render, t):

    for t_rollout in range(nb_rollout_steps):
        # Predict next action.
        action, q = agent.pi(obs, apply_noise=True, compute_Q=True)
        assert action.shape == env.action_space.shape

        # Execute next action.
        if rank == 0 and render:
            env.render()
        assert max_action.shape == action.shape
        new_obs, r, done, info = env.step(max_action * action)  # scale for execution in env (as far as DDPG is concerned, every action is in [-1, 1])
        t += 1
        if rank == 0 and render:
            env.render()
        episode_reward += r
        episode_step += 1
        #print ("rollout episode_step : ", episode_step, " t:", t)

        # Book-keeping.
        epoch_actions.append(action)
        epoch_qs.append(q)
        agent.store_transition(obs, action, r, new_obs, done)
        obs = new_obs

        if done:
            # Episode done.
            epoch_episode_rewards.append(episode_reward)
            episode_rewards_history.append(episode_reward)
            epoch_episode_steps.append(episode_step)
            episode_reward = 0.
            episode_step = 0
            epoch_episodes += 1
            episodes += 1

            agent.reset()
            obs = env.reset()

    return agent, env, episode_reward, episode_step, epoch_actions,epoch_qs,epoch_episode_rewards, episode_rewards_history, \
            epoch_episode_steps, epoch_episodes, episodes, t


def train(nb_train_steps, memory, batch_size, t, param_noise_adaption_interval, agent):

    epoch_actor_losses = []
    epoch_critic_losses = []
    epoch_adaptive_distances = []
    for t_train in range(nb_train_steps):
        # Adapt param noise, if necessary.
        if memory.nb_entries >= batch_size and t % param_noise_adaption_interval == 0:
            distance = agent.adapt_param_noise()
            epoch_adaptive_distances.append(distance)

        cl, al = agent.train()
        epoch_critic_losses.append(cl)
        epoch_actor_losses.append(al)
        agent.update_target_net()
    return agent, epoch_actor_losses, epoch_critic_losses, epoch_adaptive_distances


def evaluate(agent, eval_obs, max_action, eval_env, render_eval, sess, t,
             eval_episode_rewards_history, best_score, data_path, saver_best, nb_eval_episodes):

    eval_episode_rewards = []
    eval_qs = []
    if eval_env is not None:
        eval_episode_reward = 0.
        for i in range(nb_eval_episodes): # 5 rollouts
            while True:
                eval_action, eval_q = agent.pi(eval_obs, apply_noise=False, compute_Q=True)
                eval_obs, eval_r, eval_done, eval_info = eval_env.step(max_action * eval_action)  # scale for execution in env (as far as DDPG is concerned, every action is in [-1, 1])
                if render_eval:
                    eval_env.render()
                eval_episode_reward += eval_r
                eval_qs.append(eval_q)
               # print("evaluate eval_done : ", eval_done, " eval_episode_reward:", eval_episode_reward)
                if eval_done:
                    eval_obs = eval_env.reset()
                    eval_episode_rewards.append(eval_episode_reward)
                    eval_episode_rewards_history.append(eval_episode_reward)
                    eval_episode_reward = 0.
                    break
        if np.mean(eval_episode_rewards) > best_score:
            best_score = np.mean(eval_episode_rewards)
            if not os.path.exists(data_path+'tf_save/'):
                os.mkdir(data_path+'tf_save/')
                os.mkdir(data_path+'tf_save/'+'best1_5M/') # optional, to save GEP data at 1.5M
            for file in os.listdir(data_path+'tf_save/'):
                if path.isfile(data_path+'tf_save/'+file): # optional, to save GEP data at 1.5M
                    os.remove(data_path+'tf_save/'+file)
            saver_best.save(sess, data_path+'tf_save/'+ 'best_actor_step'+str(t)+'_score'+str(int(best_score)))
            # optional, to save GEP data at 1.5M
            if t < 1500000:
                for file in os.listdir(data_path + 'tf_save/' + 'best1_5M/'):
                    if path.isfile(data_path + 'tf_save/' + 'best1_5M/' + file):  # optional, to save GEP data at 1.5M
                        os.remove(data_path + 'tf_save/' + 'best1_5M/' + file)
                for file in os.listdir(data_path + 'tf_save/'):
                    if path.isfile(data_path + 'tf_save/'+file):  # optional, to save GEP data at 1.5M
                        shutil.copy2(data_path+'tf_save/'+file, data_path + 'tf_save/'+'best1_5M/'+file)


    return agent, eval_episode_rewards, eval_qs, best_score, eval_obs, eval_env, \
            eval_episode_rewards_history