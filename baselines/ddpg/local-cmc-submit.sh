#!/usr/bin/env bash

NB_TRIALS=20

for TRIAL in $(seq $NB_TRIALS)
do
  (
    echo "Running experiment $TRIAL"
    export TRIAL
    python cluster_cmc.py > test_${TRIAL}.out 2> test_${TRIAL}.err
  ) &
done

wait
