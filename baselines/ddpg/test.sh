#!/bin/sh
#SBATCH -n1
#SBATCH -p long_sirocco
#SBATCH -t 20:00:00
#SBATCH --gres=gpu:1
#SBATCH -e test.sh.err
#SBATCH -o test.sh.out
rm log.txt; 
export EXP_INTERP='/home/ccolas/virtual_envs/py3.5/bin/python' ;
ngpu="$(nvidia-smi -L | tee /dev/stderr | wc -l)"
agpu=0
export CUDA_VISIBLE_DEVICES=$agpu
agpu=$(((agpu+1)%ngpu))
$EXP_INTERP test.py 
