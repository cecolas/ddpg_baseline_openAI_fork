import argparse
import time
import os
import logging
import sys
sys.path.append('/home/flowers/Desktop/Scratch/ddpg_baseline_openAI_fork/')
from baselines import logger, bench
from baselines.common.misc_util import (
    set_global_seeds,
    boolean_flag,
)
import baselines.ddpg.training as training
from baselines.ddpg.models import Actor, Critic
from baselines.ddpg.memory import Memory
from baselines.ddpg.noise import *
from baselines.common import tf_util as U
import gym
import tensorflow as tf
from mpi4py import MPI

def run(env_id, seed, noise_type, layer_norm, evaluation,
        study, buffer_location, trial_id, data_path, **kwargs):
    # Configure things.
    rank = MPI.COMM_WORLD.Get_rank()
    if rank != 0: logger.set_level(logger.DISABLED)

    # Create envs.
    env = gym.make(env_id)
    env = bench.Monitor(env, logger.get_dir() and os.path.join(logger.get_dir(), "%i.monitor.json"%rank))
    gym.logger.setLevel(logging.WARN)

    if evaluation and rank==0:
        eval_env = gym.make(env_id)
        eval_env = bench.Monitor(eval_env, logger.get_dir() and os.path.join(logger.get_dir(), "%i.monitor.json"%rank))
    else:
        eval_env = None

    # Parse noise_type
    action_noise = None
    param_noise = None
    nb_actions = env.action_space.shape[-1]
    for current_noise_type in noise_type.split(','):
        current_noise_type = current_noise_type.strip()
        if current_noise_type == 'none':
            pass
        elif 'adaptive-param' in current_noise_type:
            _, stddev = current_noise_type.split('_')
            param_noise = AdaptiveParamNoiseSpec(initial_stddev=float(stddev), desired_action_stddev=float(stddev))
        elif 'normal' in current_noise_type:
            _, stddev = current_noise_type.split('_')
            action_noise = NormalActionNoise(mu=np.zeros(nb_actions), sigma=float(stddev) * np.ones(nb_actions))
        elif 'ou' in current_noise_type:
            _, stddev = current_noise_type.split('_')
            action_noise = OrnsteinUhlenbeckActionNoise(mu=np.zeros(nb_actions), sigma=float(stddev) * np.ones(nb_actions))
        else:
            raise RuntimeError('unknown noise type "{}"'.format(current_noise_type))

    # Configure components.
    memory = Memory(limit=int(2e6), action_shape=env.action_space.shape, observation_shape=env.observation_space.shape)

    activation_map = { "relu" : tf.nn.relu, "leaky_relu" : U.lrelu, "tanh" :tf.nn.tanh}

    critic = Critic(layer_norm=layer_norm, hidden_sizes=kwargs["vf_size"], activation=activation_map[kwargs["activation_vf"]])
    actor = Actor(nb_actions, layer_norm=layer_norm, hidden_sizes=kwargs["policy_size"], activation=activation_map[kwargs["activation_policy"]])

    # Seed everything to make things reproducible.
    seed = seed + 1000000 * rank
    logger.info('rank {}: seed={}, logdir={}'.format(rank, seed, logger.get_dir()))
    tf.reset_default_graph()
    set_global_seeds(seed)
    env.seed(seed)
    if eval_env is not None:
        eval_env.seed(seed)

    # Disable logging for rank != 0 to avoid noise.
    if rank == 0:
        start_time = time.time()
    training.train(env=env, eval_env=eval_env, param_noise=param_noise, action_noise=action_noise,
                   actor=actor, critic=critic, memory=memory, study = study, buffer_location = buffer_location,
                   trial_id = trial_id, data_path = data_path, **kwargs)
    env.close()
    if eval_env is not None:
        eval_env.close()
    if rank == 0:
        logger.info('total runtime: {}s'.format(time.time() - start_time))


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--env-id', type=str, default='HalfCheetah-v1')
    boolean_flag(parser, 'render-eval', default=False)
    boolean_flag(parser, 'layer-norm', default=True)
    boolean_flag(parser, 'render', default=False)
    boolean_flag(parser, 'normalize-returns', default=False)
    boolean_flag(parser, 'normalize-observations', default=True)
    parser.add_argument('--seed', help='RNG seed', type=int, default=0)
    parser.add_argument('--critic-l2-reg', type=float, default=1e-2)
    parser.add_argument('--batch-size', type=int, default=64)  # per MPI worker
    parser.add_argument('--actor-lr', type=float, default=1e-4)
    parser.add_argument('--critic-lr', type=float, default=1e-3)
    boolean_flag(parser, 'popart', default=False)
    parser.add_argument('--gamma', type=float, default=0.99)
    parser.add_argument('--reward-scale', type=float, default=1.)
    parser.add_argument('--clip-norm', type=float, default=None)
    parser.add_argument('--nb-epochs', type=int, default=10)  # with default settings, perform 1M steps total
    parser.add_argument('--nb-epoch-cycles', type=int, default=20)
    parser.add_argument('--nb-train-steps', type=int, default=5)  # per epoch cycle and MPI worker
    parser.add_argument('--nb-eval-steps', type=int, default=100)  # per epoch cycle and MPI worker
    parser.add_argument('--nb-rollout-steps', type=int, default=100)  # per epoch cycle and MPI worker
    parser.add_argument('--noise-type', type=str, default='ou_0.3')  # choices are adaptive-param_xx, ou_xx, normal_xx, none
    parser.add_argument("--policy_size", nargs="+", default=(64,64), type=int)
    parser.add_argument("--vf_size", nargs="+", default=(64,64), type=int)
    parser.add_argument("--activation_vf", type=str, default="relu")
    parser.add_argument("--activation_policy", type=str, default="relu")
    boolean_flag(parser, 'evaluation', default=True)
    parser.add_argument('--study', type=str, default='DDPG')
    parser.add_argument('--buffer_location', type=str, default ='/home/flowers/Desktop/Scratch/experiments/RandomGoal_NNController_Cheetah/Cheetah_modelbuffer_50_500k_buffer_1817')
    parser.add_argument('--data_path', type=str, default='/media/flowers/3C3C66F13C66A59C/data_save/ddpg_study_baseline/data/')
    parser.add_argument('--trial_id', type=int, default=999)
    args = parser.parse_args()

    potential_studies = ['DDPG','GEP_DDPG','GEP_FDDPG']
    assert args.study in potential_studies, "argument 'study' should be GEP_DDPG, GEP_FDDPG or DDPG"
    assert 'trial_id' in args, 'trial_id should be given as an argument'

    data_path = args.data_path + args.env_id+'/'+str(args.trial_id)+'/'
    if os.path.exists(data_path):
        i = 1
        while os.path.exists(args.data_path + args.env_id+'/'+str(args.trial_id+100*i)+'/'):
            i += 1
        args.trial_id = args.trial_id + i * 100
        print('result_path already exist, trial_id changed to: ', args.trial_id)
    data_path = args.data_path + args.env_id + '/' + str(args.trial_id) + '/'
    args.data_path = data_path
    os.mkdir(data_path)

    n_timesteps = args.nb_epochs * args.nb_epoch_cycles * args.nb_rollout_steps
    print('Running study: '+args.study+', with noise: ,'+args.noise_type+' for '+str(n_timesteps)+' timesteps.')

    dict_args = vars(args)
    return dict_args


if __name__ == '__main__':
    args = parse_args()
    if MPI.COMM_WORLD.Get_rank() == 0:
        logger.configure(dir=args['data_path'])
    # Run actual script.
    run(**args)
